<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
	<head>
		<title><?php print $head_title; ?></title>
		<meta name="description" content="Hess Equine Shop offers a full product of Horse First supplements.">
		<meta name="keywords" content="equine nutrition, horse supplements, horse first, horse first supplements, horse nutrition" />
		<meta name="msvalidate.01" content="9C10FFCAC889E99F74FCA0B1725A85E4" />
		<meta name="y_key" content="5459dc8b1a9373d2" />
		<?php print $head; ?>
		<?php print $styles; ?>
		<?php print $scripts; ?>
		<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/dzdy.required.js"></script>
	</head>
	<body class="<?php print $body_classes; ?>">
		<div id="utility-bar">
			<div id="utility-bar-inner">
				<span id="utility-bar-left">
				Featuring a complete line of Horse First products.
				</span>
				<span id="utility-bar-right">
				<ul id="utility-bar-links">
					<li class="last"><a href="/help" title="Help">Help</a></li>
					<li><a href="/cart" title="Shopping Cart">Shopping Cart</a></li>
					<li><a href="/user" title="My Account">My Account</a></li>
					<li class="first"><?php global $user; ?><?php if ($user->uid) : ?><?php print l("Logout","logout"); ?><?php else : ?><?php print l("Login","user/login", array('query' => drupal_get_destination()));?><?php endif; ?></li>
				</ul>
				</span>
			</div>
		</div>
		<div id="page">
			<div id="header">
				<div id="header-left" onclick="location.href='<?php print $base_path ?>';">
					&nbsp;
					<span id="logo-tagline" style="display:none"></span>
				</div>
				<br style="clear:both"/>
			</div>
			<?php if ($navbar): ?>
			<div id="navbar">
				<div id="navbar-inner" class="clear-block region region-navbar">
					<?php print $navbar; ?>
				</div>
			</div> 
			<?php endif; ?>
			<div id="main">
				<div id="main-inner" class="clear-block<?php if ($primary_links || $secondary_links || $navbar) { print ' with-navbar'; } ?>">
					<div id="content">
						<div id="content-inner">
							<?php if ($content_top): ?>
							<div id="content-top" class="region region-content_top">
								<?php print $content_top; ?>
							</div>
							<?php endif; ?>
							<?php if ($title || $tabs || $help || $messages): ?>
							<div id="content-header">
								<?php print $messages; ?>
								<?php if ($tabs): ?>
								<div class="tabs"><?php print $tabs; ?></div>
								<?php endif; ?>
								<?php if ($title): ?>
								<h1 class="title"><?php print $title; ?></h1>
								<?php endif; ?>
								<?php print $help; ?>
							</div>
							<?php endif; ?>
							<div id="content-area">
								<?php print $content; ?>
							</div>
							<?php if ($content_bottom): ?>
							<div id="content-bottom" class="region region-content_bottom">
								<?php print $content_bottom; ?>
							</div>
							<?php endif; ?>
						</div>
					</div>
				<?php if ($right): ?>
				<div id="sidebar-right">
					<div id="sidebar-right-inner" class="region region-right">
						<?php print $right; ?>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php if ($footer || $footer_message): ?>
    <div id="footer">
		<div id="footer-inner" class="region region-footer">
			<?php if ($footer_message): ?>
			<div id="footer-message">
				<?php print $footer_message; ?>
			</div>
			<?php endif; ?>
        <?php print $footer; ?>
		<div id="dzdy_badge" style="margin-bottom:10px;text-align:center;"></div>
		</div>
	</div>
    <?php endif; ?>
  <?php if ($closure_region): ?>
    <div id="closure-blocks" class="region region-closure"><?php print $closure_region; ?></div>
  <?php endif; ?>
  <?php print $closure; ?>
</body>
</html>
