﻿$(document).ready(function(){  

	$('#contact-btn').bt({
	  showTip: function(box){
		$(box).fadeIn(200);
	  },
	  hideTip: function(box, callback){
		$(box).animate({opacity: 0}, 100, callback);
	  },
	  fill:             "#E3F9E4",
	  strokeStyle:      "#1F8028",
	  width:            '210px',
	  positions:        ['bottom'],
	  cornerRadius:     5,
	  shadow:           true,
	  shadowOffsetX:    2,                    
      shadowOffsetY:    2,
	  shadowBlur:       3,
	  shadowColor:      "#7C7C7C",
	  overlap:          3,
	  spikeGirth:       15,            
	  spikeLength:      10,
	  cssStyles:        {
		color: '#232C1A',
		fontWeight: 'normal',
		fontSize: '0.8em'
	  },
	  hoverIntentOpts: {
		interval: 0,
		timeout: 0
	  }
	});
	
	
	$('a.breadcrumb').bt({
	  showTip: function(box){
		$(box).fadeIn(200);
	  },
	  hideTip: function(box, callback){
		$(box).animate({opacity: 0}, 100, callback);
	  },
	  fill:             "#EFEFEF",
	  strokeStyle:      "#3F3F3F",
	  width:            '100px',
	  positions:        ['right'],
	  cornerRadius:     5,
	  shadow:           true,
	  shadowOffsetX:    2,                    
      shadowOffsetY:    2,
	  shadowBlur:       3,
	  shadowColor:      "#7C7C7C",
	  overlap:          -1,
	  spikeGirth:       15,            
	  spikeLength:      10,
	  cssStyles:        {
		color: '#232C1A',
		fontWeight: 'normal',
		fontSize: '0.9em'
	  },
	  hoverIntentOpts: {
		interval: 0,
		timeout: 0
	  }
	});
	
	$('a.hire-us').bt({
	  showTip: function(box){
		$(box).fadeIn(200);
	  },
	  hideTip: function(box, callback){
		$(box).animate({opacity: 0}, 100, callback);
	  },
	  fill:             "#FCDEDE",
	  strokeStyle:      "#A31617",
	  width:            '170px',
	  positions:        ['left'],
	  cornerRadius:     5,
	  shadow:           true,
	  shadowOffsetX:    2,                    
      shadowOffsetY:    2,
	  shadowBlur:       3,
	  shadowColor:      "#7C7C7C",
	  overlap:          2,
	  spikeGirth:       15,            
	  spikeLength:      10,
	  cssStyles:        {
		color: '#232C1A',
		fontWeight: 'normal',
		fontSize: '0.9em'
	  },
	  hoverIntentOpts: {
		interval: 0,
		timeout: 0
	  }
	});
	
	// Handles adding parameter to url for contact variable.
	var title = $(this).attr('title');
	
	var link = $("#contact-btn").attr('href');
	var linkEdson = link + "?h=edson";
	var linkMart = link + "?h=martini";
	var linkHire = link + "?h=hire";	
	
	var titleEdson = "Edson Pump Out Systems | Green Marine LLC";
	var titleMart = "Martini Boats | Green Marine LLC";
	var titleHire = "Hire Us | Green Marine LLC";
	
	if(title == titleEdson) {
	    $("#contact-btn").attr('href', linkEdson);
	}
	
	if(title == titleMart) {
	    $("#contact-btn").attr('href', linkMart);
	}

	if(title == titleHire) {
	    $("#contact-btn").attr('href', linkHire);
	}

});