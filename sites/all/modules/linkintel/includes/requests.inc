<?php
// $Id: requests.inc,v 1.6 2010/11/04 23:10:25 tomdude48 Exp $

function linkintel_node_requests_page($node, $mode, $options = '') {
  linkintel_set_admin_theme(); 
  drupal_set_title('Requests');

  $a = explode(',', $options);
  $options = array();
  foreach($a AS $v) {
    $options[$v] = 1; 
  }  
  
  drupal_set_title('Requests');


  $filter = array(
    'where' => "r.nid = %d",
    'args' => array($node->nid)
  );
  
  $header = linkintel_get_requests_report_header('node');
  
  $result = linkintel_load_filtered_requests_result($filter, $options, $header);
  
  $destination = 'node/' . $node->nid . '/linkintel/requests';
  $output .= theme_linkintel_requests_report($header, $result, $destination, 'node', $node);
  
  $output .= l(t('Add request'), 'admin/content/linkintel/request/edit/', array('query' => 'destination=' . $destination . '&nid=' . $node->nid));
  return $output;
}

function linkintel_requests_page($options = '') {
  if($_GET['options']) {
    $options = $_GET['options'];
  }
  $a = explode(',', $options);
  $options = array();
  foreach($a AS $v) {
    $options[$v] = 1; 
  }
  drupal_set_title('Requests');
  $filter = linkintel_requests_build_filter_query();


  $output .= drupal_get_form('linkintel_requests_filter_form');
  
  $header = linkintel_get_requests_report_header();
  
  $result = linkintel_load_filtered_requests_result($filter, $options, $header);
  
  $destination = 'admin/content/linkintel/requests';
  $output .= theme_linkintel_requests_report($header, $result, $destination);
  
  return $output;
}

function linkintel_get_requests_report_header($mode = 'all') {
  $header = array();
  $header[] = array('data' => t('Keyword'), 'field' => 'k.keyword');
  if($mode != 'node') {
    if($mode == 'suggestions') {
      $header[] = array('data' => t('To'), 'field' => 'pid');
      $header[] = array('data' => t('nid'), 'field' => 'nid');
    }
    else {
      $header[] = array('data' => t('To path'), 'field' => 'path');
      $header[] = array('data' => t('nid'), 'field' => 'nid');
    }
  }
  $header[] = array('data' => t('Score'), 'field' => 'score', 'sort' => 'desc');
  $header[] = array('data' => t('Request'), 'field' => 'r.priority');
  $header[] = array('data' => t('Page'), 'field' => 'p.priority');
  if(module_exists('kwresearch')) {
    $header[] = array('data' => t('Keyword'), 'field' => 'keyword_priority'); 
  }  
  $header[] = array('data' => t('Mode'), 'field' => 'r.mode');
  $header[] = array('data' => t('Text'), 'field' => 'r.text');
  $header[] = array('data' => t('Variants'), 'field' => 'r.allow_variants');
  if($mode != 'suggestions') {
    $header[] = array('data' => t('Sync'), 'field' => 'r.sync');
  }
  $header[] = array('data' => t('Ops'));
  
  return $header;
}



function theme_linkintel_requests_report($header, $result, $destination, $mode = 'all', $node = NULL) {
  $modes = linkintel_get_mode_options('request');
  $priorities = linkintel_get_priority_options();  
  
  if($mode == 'node') {
    $settings = linkintel_load_page_settings($node->nid);
  }  
  $rows = array();
  $allow_variants_options = linkintel_get_allow_variants_options('report');
  while ($r = db_fetch_object($result)) {
    if($mode != 'node') {
      $settings = linkintel_load_page_settings($r->nid);
    }
    $attributes = unserialize($r->attributes);
    $options = array();
    if($r->external) {
      $options['absolute'] = TRUE;
    }
    if($mode == 'suggestions') {
      $ops = l(t('add link'), 'admin/content/linkintel/link/edit', array('query' => 'destination=' . $destination . '&rid=' . $r->rid . '&from_nid=' . $node->nid));
    }
    else {
      $ops = l(t('edit'), 'admin/content/linkintel/request/edit/' . $r->rid, array('query' => 'destination=' . $destination));
      $ops .= ' | ' . l(t('delete'), 'admin/content/linkintel/request/delete/' . $r->rid, array('query' => 'destination=' . $destination));
    }
    $row = array();
    $row[] = check_plain($r->keyword);
    if($mode != 'node') {
      $row[] = linkintel_l_report($r->path, FALSE);
      $row[] = ($r->nid) ? $r->nid : '-'; 
    }
    $row[] = number_format($r->score);
    $t = ($priorities[$r->priority]) ? $priorities[$r->priority] : $r->priority;
    $row[] = l($t, 'admin/content/linkintel/request/edit/' . $r->rid, array('query' => 'destination=' . $destination));
    
    if(!isset($r->page_priority)) {
      $r->page_priority = t('[default]');
    }
    $t = ($priorities[$r->page_priority]) ? $priorities[$r->page_priority] : $r->page_priority;
    //if($settings->enable > 1) {
      $row[] = l($t, 'admin/content/linkintel/page/edit', array('query' => 'pid=' . (($r->nid) ? $r->nid : $r->path) . '&destination=' . $destination));

    /*}
    else {
      $row[] = $t;
    }
    */
    
    if(module_exists('kwresearch')) {
      $query = 'destination=' . $destination;
      if(!isset($r->kwr_kid)) {
        $query .= '&keyword=' . $r->keyword;
        $t = t('x');
      }
      else if($r->keyword_priority == -1) {
        $t = t('-'); 
      } else {
        $t = ($priorities[$r->keyword_priority]) ? $priorities[$r->keyword_priority] : $r->keyword_priority;
      }
      $row[] = l($t, 'admin/content/kwresearch/keyword_list/edit/' . $r->kwr_kid, array('query' => $query));
    } 
    $row[] = $modes[$r->mode];
    $row[] = ($r->text) ?  $r->text : '[keyword]';
    $row[] = $allow_variants_options[($r->allow_variants) ? $r->allow_variants : 0];
    if($mode != 'suggestions') {
      $row[] = ($r->sync) ?  'Yes' : 'No';
    }
    $row[] = $ops;
    
    $rows[] = array('data' =>
      $row,
      // Attributes for tr
      'class' => "linkintel"
    );
  }

  if (!$rows) {
    $rows[] = array(array('data' => t('No link request were found.'), 'colspan' => count($header)));
  }  

  $output .= theme('table', $header, $rows, array('id' => 'linkintel-request'));
  $output .= theme('pager', NULL, 100, 0);
  
  //$output .= l(t('Add request'), 'admin/content/linkintel/request/edit/', array('query' => 'destination=' . $destination ));
  return $output;
}

function linkintel_requests_build_filter_query() {
  if (empty($_SESSION['linkintel_requests_filter'])) {
    return;
  }

  $filters = linkintel_requests_filters();

  // Build query
  $where = $args = array();

  foreach ($_SESSION['linkintel_requests_filter'] as $key => $filter) {

    if($key == 'like_match') {
      continue;
    }
    $filter_where = array();
    if ($filters[$key]['type'] == 'select') {
      foreach ($filter as $value) {
        $filter_where[] = $filters[$key]['where'];
        $args[] = $value;
      }
    }
    elseif ($filter) {      
      if ($key == 'keywords') {        
        if($_SESSION['linkintel_requests_filter']['like_match']) {
          $filter_where[] = $filters['like_match']['where'];
          $args[] = $filter;
        }
        else {
          $keywords = explode(',', $filter);
          $placeholders = implode(',', array_fill(0, count($keywords), '"%s"'));
          $filter_where[] = str_replace('"%s"', $placeholders, $filters[$key]['where']);
          foreach($keywords AS $keyword) {
            $keyword = trim(strtolower($keyword));
            $args[] = $keyword;
          }
        }
      }
      else if($key == 'from') {
        // treat as nid input
        $a = explode('/', $filter);
        if(($a[0] == 'node' && is_numeric($a[1])) || ($filter > 0)) {
          $nid = (($a[0] == 'node' && is_numeric($a[1])))? $a[1] : $filter;
          $filter_where[] = 'r.nid = %d';
          $args[] = $nid;
        }
        else {
          $filter_where[] = $filters[$key]['where'];
          $args[] = $filter;         
        }
      }
      else {
        $filter_where[] = $filters[$key]['where'];
        $args[] = $filter;
      }      
    }
    if (!empty($filter_where)) {
      $where[] = '('. implode(' OR ', $filter_where) .')';
    }
  }
  $where = !empty($where) ? implode(' AND ', $where) : '';

  return array(
    'where' => $where,
    'args' => $args,
  );
}

/**
 * Return form for site keyword list filters.
 *
 */
function linkintel_requests_filter_form() {
  $session = &$_SESSION['linkintel_requests_filter'];
  $session = is_array($session) ? $session : array();
  $filters = linkintel_requests_filters();

  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter requests'),
    '#theme' => 'dblog_filters',
    '#collapsible' => TRUE,
    '#collapsed' => empty($session),
  );
  foreach ($filters as $key => $filter) {
    if ($filter['type'] == 'textfield') {
      $form['filters']['status'][$key] = array(
        '#title' => $filter['title'],
        '#type' => 'textfield',
        '#size' => ($filter['size'])? $filter['size']:8,
      );    
    }
    elseif ($filter['type'] == 'select') {
      $form['filters']['status'][$key] = array(
        '#title' => $filter['title'],
        '#type' => 'select',
        '#multiple' => TRUE,
        '#size' => 6,
        '#options' => $filter['options'],
      );
    }
    elseif ($filter['type'] == 'checkbox') {
      $form['filters']['status'][$key] = array(
        '#title' => $filter['title'],
        '#type' => 'checkbox',
        '#return_value' => $filter['return_value'],
      );
    }
    elseif ($filter['type'] == 'markup') {
      $form['filters']['status'][$key] = array(
        '#type' => $filter['type'],
        '#value' => $filter['value'],
      );       
    }
    if (!empty($session[$key])) {
      $form['filters']['status'][$key]['#default_value'] = $session[$key];
    }
  }

  $form['filters']['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );
  if (!empty($session)) {
    $form['filters']['buttons']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset')
    );
  }

  return $form;
}

/**
 * Returns filters for site keywords report
 */
function linkintel_requests_filters() {
  $filters = array();

  $options = linkintel_get_priority_options();
  $filters['keywords'] = array(
    'title' => t('Keywords'),
    'type' => 'textfield',
    'size' => 60,
    'description' => t('Enter a comma seperated list of keywords to filter by exact match.'),
    'where' => 'k.keyword IN ("%s")',
  ); 
  $filters['like_match'] = array(
    'title' => t('Like match'),
    'type' => 'checkbox',
    'return_value' => 1,
    'where' => 'k.keyword LIKE "%%%s%%"',
  );
  $filters['from'] = array(
    'title' => t('From'),
    'type' => 'textfield',
    'size' => 60,
    'where' => 'path = "%s"',
    'description' => t('Input a path or nid.'),
  ); 
  $filters['request_priority'] = array(
    'title' => t('Request priority'),
    'type' => 'select',
    'where' => 'r.priority = %d',
    'options' => $options,
  );
  $filters['page_priority'] = array(
    'title' => t('Request priority'),
    'type' => 'select',
    'where' => 'p.priority = %d',
    'options' => $options,
  );
  $filters['keyword_priority'] = array(
    'title' => t('Request priority'),
    'type' => 'select',
    'where' => 'kwr.priority = %d',
    'options' => $options,
  );
   
  $filters['sync'] = array(
    'title' => t('Sync'),
    'type' => 'checkbox',
    'return_value' => 1,
    'where' => 'r.sync = %d',
  ); 

  return $filters;
}

/**
 * Validate result from site keyword report filter form.
 */
function linkintel_requests_filter_form_validate($form, &$form_state) {
  if ($form_state['values']['op'] == t('Filter') 
    && empty($form_state['values']['keywords'])
    && empty($form_state['values']['from']) 
    && empty($form_state['values']['request_priority'])
    && empty($form_state['values']['page_priority'])
    && empty($form_state['values']['keyword_priority'])
    && empty($form_state['values']['sync'])
  ) {
    form_set_error('type', t('You must select something to filter by.'));
  }
}

/**
 * Process result from site keywords report filter form.
 */
function linkintel_requests_filter_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  $filters = linkintel_requests_filters();
  switch ($op) {
    case t('Filter'):
      foreach ($filters as $name => $filter) {
        if (isset($form_state['values'][$name])) {
          $_SESSION['linkintel_requests_filter'][$name] = $form_state['values'][$name];
        }
      }
      break;
    case t('Reset'):
      $_SESSION['linkintel_requests_filter'] = array();
      break;
  }
  return 'admin/content/linkintel/requests';
}

/**
 * Generages page keyword edit form
 * 
 * @param int|str $pid - page id; the nid (int) or path (str) of the page
 * @param int $kid - keyword id
 */
function linkintel_request_edit_page($rid = '') {
  if($rid > 0) {
    $request_obj = linkintel_load_request($rid);
  }
  if ($request_obj->rid) {
    drupal_set_title(t('Edit request'));
  }
  else {
    drupal_set_title(t('Add request'));
    $request_obj = new stdClass();
    if($_GET['nid']) {
      $request_obj->nid = $_GET['nid'];
    }
  }  
  $output .= drupal_get_form('linkintel_request_edit_form', $request_obj);
  return $output;
}

/**
 * Returns page keyword edit form
 * 
 * @param arr $form_state
 * @param int|str $pid - page id; the nid (int) or path (str) of the page
 * @param str $keyword
 */

function linkintel_request_edit_form($form_state, $request = stdClass) {
  $form['rid'] = array(
    '#type' => 'value',
    '#value' => ($request->rid) ? $request->rid : ''
  );
  if($request->nid || $request->path) {
    if($request->nid) {
      $node = node_load($request->nid);
    }
    $link = linkintel_l_report($request);
    $form['page'] = array(
        '#title' => t('Page'),
        '#type' => 'item',
        '#value' =>  $link,
    );
    $form['nid'] = array(
      '#type' => 'value',
      '#value' => $request->nid
    );
    $form['path'] = array(
      '#type' => 'value',
      '#value' => ($request->path) ? $request->path : 'node/' . $request->nid
    );
    if($request->nid) {
      $node = node_load($request->nid);
      $report = linkintel_node_requests_page($node, 'node');
      $form['page_requests'] = array(
        '#type' => 'fieldset',      
        '#title' => t('Existing page requests'),      
        '#collapsible' => TRUE,      
        '#collapsed' => TRUE,    
      ); 
      $form['page_requests']['report'] = array(
        '#type' => 'markup',
        '#value' => $report,
      );     
    }
  }
  else {
    $form['path'] = array(
      '#type' => 'textfield',
      '#title' => t('URL Path'),
      '#required' => TRUE,
      '#description' => t('Enter the url path you want to associate the request. This can be an internal Drupal path such as node/add or an external URL such as http://drupal.org. Enter &lt;front&gt; to link to the front page.'),
    );   
  }
  if ($request->kid) {
    $form['kid'] = array(
      '#type' => 'value',
      '#value' => $request->kid
    );
    $form['keyword_item'] = array(
      '#type' => 'item',
      '#title' => t('Keyword phrase'), 
      '#value' => $request->keyword,
    );    
  }
  else {
    $form['kid'] = array(
      '#type' => 'value',
      '#value' => 0,
    );
    $form['keyword'] = array(
      '#type' => 'textfield',
      '#title' => t('Keyword phrase'), 
      '#required' => TRUE,
    );  
  }
  
  $options = linkintel_get_allow_variants_options();
  $form['allow_variants'] = array(
    '#type' => 'select',
    '#title' => t('Match keyword variants'), 
    '#options' => $options,
    '#default_value' => ($request->allow_variants) ? $request->allow_variants:LINKINTEL_ALLOW_VARIANTS_DEFAULT,
    '#description' => t('Set if you want to allow a request match on keyword variants. E.g. the term "brakes" would match "brake", "braking", "brakes". Disable for exact match only.'),
  );
   
  $options = linkintel_get_priority_options();
  $form['priority'] = array(
    '#type' => 'select',
    '#title' => t('Priority'), 
    '#options' => $options,
    '#default_value' => ($request->priority) ? $request->priority:LINKINTEL_PRIORITY_DEFAULT,
    '#description' => t('Use to set how important the keyword phrase is for the page. If unsure, just set to "Standard".'),
  );
  $options = linkintel_get_mode_options('request');
  $form['mode'] = array(
    '#type' => 'select',
    '#title' => t('Mode'), 
    '#options' => $options,
    '#default_value' => ($request->mode) ? $request->mode:LINKINTEL_REQUEST_MODE_DEFAULT,
    '#description' => t('Determines the desired style of link: 
      <ul>
        <li><em>Either</em> - Either keyword or sidebar.</li>
        <li><em>Keyword</em> - Inline link wraped around the keyword phrase.</li>
        <li><em>Sidebar</em> - Sidebar links.</li>
      </ul>
    '),
  );
  $form['text'] = array(
    '#type' => 'textfield',
    '#title' => t('Sidebar anchor text'),
    '#default_value' => $request->text,
    '#description' => t('Used as the anchor text for sidebar links. Leave blank to use the keyword phrase. Use [title] to use the node title.'),
  );
  $form['sync'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sync'), 
    '#return_value' => 1,
    '#default_value' => $request->sync,
    '#description' => t('Check if you want this request to be automatically synced with page keywords.'),
  );

  $form['attributes'] = array(
    '#type' => 'fieldset',      
    '#title' => t('Attributes'),      
    '#collapsible' => TRUE,      
    '#collapsed' => TRUE,    
  );
  $attributes = linkintel_get_link_attributes();

  foreach($attributes AS $attr => $lable) {
    $form['attributes'][$attr] = array(
      '#type' => 'textfield',
      '#title' => $lable,
      '#default_value' => $request->attributes[$attr],
    ); 
  }
  $form['attributes']['rel']['#type'] = 'checkbox';
  $form['attributes']['rel']['#return_value'] = 'nofollow';
    
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  
  return $form;
}

/**
 * Validates the page keyword edit form
 * 
 * @param $form
 * @param $form_state
 */
function linkintel_request_edit_form_validate($form, &$form_state) {

  $link = $form_state['values']['path'];
  linkintel_parse_link_path($link, $msgs, 'form');


}

/**
 * Processes the page keyword edit form
 * 
 * @param $form
 * @param $form_state
 */
function linkintel_request_edit_form_submit($form, &$form_state) {

  $request = new stdClass();
  $request->mode = $form_state['values']['mode'];
  $request->rid = $form_state['values']['rid'];
  $request->nid = $form_state['values']['nid'];
  $request->path = $form_state['values']['path'];
  $request->options = $form_state['values']['options'];
  $request->kid = $form_state['values']['kid'];
  $request->keyword = $form_state['values']['keyword'];
  $request->allow_variants = $form_state['values']['allow_variants'];
  $request->priority = $form_state['values']['priority'];
  $request->mode = $form_state['values']['mode'];
  $request->text = $form_state['values']['text'];
  $request->sync = $form_state['values']['sync'];  
  
  $request->attributes = array();
  $attributes = linkintel_get_link_attributes();
  foreach($attributes AS $attr => $lable) {
    if($form_state['values'][$attr]) {
        $request->attributes[$attr] = $form_state['values'][$attr];
    }
  }

  // embed in node body
  $rid = linkintel_save_request($request);

  drupal_set_message(t('A link request for the keyword %keyword to the page !link was saved.', 
    array(      
      '%keyword' => $request->keyword,
      '!link' => linkintel_l_report($request),
    )),
    'status'
  );
}

function linkintel_request_delete_page($rid) {
  drupal_set_title(t('Delete link request'));
  $request = linkintel_load_request($rid);
  if(!$request->rid) {
    drupal_set_message(t('A link request with ID @id was not found.', array('@id' => $request->rid)), 'error');
    return '';
  }
  $output .= drupal_get_form('linkintel_request_delete_form', $request);
  return $output;
}

/**
 * Generages page keyword edit form
 * 
 * @param int|str $pid - page id; the nid (int) or path (str) of the page
 * @param int $kid - keyword id
 */
function linkintel_request_delete_form($form_state, $request) {
  $form['message'] = array(
  '#type' => 'markup',
  '#value' => t('Are you sure you want to delete the link request for the keyword "%keyword" for page !link?',
      array(
        '%keyword' => $request->keyword,
        '!link' => linkintel_l_report($request),
      )
    ),
  );
  
  $form['rid'] = array(
    '#type' => 'value',
    '#value' => $request->rid,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  $form['cancel'] = array(
      '#type' => 'markup',
      '#value' => l(t('Cancel'), $_GET['destination']),
  );
  
  return $form;
}

function linkintel_request_delete_form_submit($form, &$form_state) {

  $request = linkintel_load_request($form_state['values']['rid']);

  linkintel_delete_request($request);
  
  if($request->sync) {
    drupal_set_message(t('The link request for the keyword "%keyword" to the page !link was deactivated. Note this request is synced and synced requests must be deactivated by setting the priority to none rather than deleting.', 
      array(      
        '%keyword' => $request->keyword,
        '!link' => linkintel_l_report($request),
      )),
      'status'
    ); 
  }
  else {
    drupal_set_message(t('The link request for the keyword "%keyword" to the page !link was deleted.', 
      array(      
        '%keyword' => $request->keyword,
        '!link' => linkintel_l_report($request),
      )),
      'status'
    );
  }
}