<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/dzdy.required.js"></script>
</head>
<body class="<?php print $classes; ?>">

  <?php if ($primary_links): ?>
    <div id="skip-link"><a href="#main-menu"><?php print t('Jump to Navigation'); ?></a></div>
  <?php endif; ?>

	<div id="header-wrapper">
		<div id="header">
    		<div class="section clearfix">
    			<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"></a>
    			<div id="shopPromote">For the finest in custom blended supplements</div>
    			<?php print $header; ?>
    		</div>
		</div><!-- /.section, /#header -->
		<?php if ($primary_links || $navigation): ?>
			<div id="navigation">
			<div class="section clearfix">

			  <?php print $navigation; ?>

			</div>
			</div><!-- /.section, /#navigation -->
		<?php endif; ?>
	</div>
	
	<?php if ($pre_content): ?>
	<div id="pre-content-wrapper">
		<div id="pre-content"><div class="section clearfix">
			<?php print $pre_content; ?>
		</div></div><!-- /.section, /#pre-content" -->
	</div>
	<?php endif; ?>
  
  <div id="page-wrapper"><div id="page">

    <div id="main-wrapper"><div id="main" class="clearfix">

	
      <div id="content" class="column"><div class="section">

        <?php print $highlight; ?>

        <?php print $breadcrumb; ?>
        <?php if ($title): ?>
          <h1 class="title"><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print $messages; ?>
        <?php if ($tabs): ?>
          <div class="tabs"><?php print $tabs; ?></div>
        <?php endif; ?>
        <?php print $help; ?>

        <?php print $content_top; ?>

        <div id="content-area">
          <?php print $content; ?>
        </div>

        <?php print $content_bottom; ?>

        <?php if ($feed_icons): ?>
          <div class="feed-icons"><?php print $feed_icons; ?></div>
        <?php endif; ?>

      </div></div><!-- /.section, /#content -->

      <?php print $sidebar_first; ?>

      <?php print $sidebar_second; ?>

    </div></div><!-- /#main, /#main-wrapper -->

    <?php if ($footer || $footer_message || $secondary_links): ?>
      <div id="footer"><div class="section">

        <?php print theme(array('links__system_secondary_menu', 'links'), $secondary_links,
          array(
            'id' => 'secondary-menu',
            'class' => 'links clearfix',
          ),
          array(
            'text' => t('Secondary menu'),
            'level' => 'h2',
            'class' => 'element-invisible',
          ));
        ?>

        <?php if ($footer_message): ?>
          <div id="footer-message"><?php print $footer_message; ?></div>
        <?php endif; ?>
		
		<div id="footer-boxes">
			<div>
				<span class="logo-colorize"></span>
			</div>
			<div class="call-box">
				<span style="float:left">
					Call Hess Equine<br/>
					<span>(215) 837-4678</span>
				</span>
				<span class="phone-bubble"></span>
			</div>
			<div class="email-box">
				<span style="float:left">
					"Tiocfaidh Ar La"<br/>
					<span><a href="mailto:info@hessequine.com">info@hessequine.com</a></span>
				</span>
				<span class="hess-symbol"></span>
			</div>
		</div>

        <?php print $footer; ?>

      </div></div><!-- /.section, /#footer -->
    <?php endif; ?>

  </div></div><!-- /#page, /#page-wrapper -->

  <?php print $page_closure; ?>

  <?php print $closure; ?>
  
  <div id="dzdy_badge" style="text-align:center"></div>

</body>
</html>
