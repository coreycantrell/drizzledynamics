<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
<head>
	<title><?php print $head_title; ?></title>
	<?php print $head; ?>
	<?php print $styles; ?>
	<link href="https://s3.amazonaws.com/scripts.drizzledynamics.com/formalize/formalize.css" rel="stylesheet" type="text/css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700|Droid+Serif' rel='stylesheet' type='text/css'>  	
	<?php print $scripts; ?>
	<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/dzdy.required.js"></script>
	<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/formalize/jquery.formalize.js"></script>
</head>
<body class="<?php print $classes; ?>">
<div id="page-wrapper">
	<div id="page">
		<div id="header">
			<div id="logo-container" class="section clearfix">
			<?php if ($logo): ?>
				<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
			<?php endif; ?>
			<div class="slogan"><span class="slogan-curl-left"></span> Exclusive Equestrian Home Decor and Exquisite Gift Baskets <span class="slogan-curl-right"></span></div>
			<?php print $header; ?>
			</div>
		</div> <!-- /.section, /#header -->
		<div id="main-wrapper">
			<div id="utility-bar">
				<?php if ($search_box): ?>
					<div id="utility-search-box"><span></span><?php print $search_box; ?></div>
				<?php endif; ?>
					<div id="utility-links">
						<ul id="utility-links-list">
							<li class="last"><a href="/help" title="Help">Help</a></li>
							<li><a href="/cart" title="Shopping Cart">Shopping Cart</a></li>
							<li><a href="/user" title="My Account">My Account</a></li>
							<li class="first"><?php global $user; ?><?php if ($user->uid) : ?><?php print l("Logout","logout"); ?><?php else : ?><?php print l("Login","user/login", array('query' => drupal_get_destination()));?><?php endif; ?></li>
						</ul>
					</div>
			</div>
			<div id="main" class="clearfix<?php if ($navigation) { print ' with-navigation'; } ?>">
				<div id="content" class="column">
					<div class="section">
						<?php print $highlight; ?>
						<?php print $breadcrumb; ?>
						<?php if ($title): ?>
							<h1 class="title"><?php print $title; ?></h1>
						<?php endif; ?>
						<?php print $messages; ?>
						<?php if ($tabs): ?>
							<div class="tabs"><?php print $tabs; ?></div>
						<?php endif; ?>
						<?php print $help; ?>
						<?php print $content_top; ?>
						
						<div id="content-area">
							<?php print $content; ?>
						</div>
						<?php print $content_bottom; ?>
						<?php //if ($feed_icons): ?>
							<div class="feed-icons"><?php //print $feed_icons; ?></div>
						<?php //endif; ?>
					</div>
				</div> <!-- /.section, /#content -->
				<?php if ($navigation): ?>
					<div id="navigation">
						<div class="section clearfix">
							<?php print $navigation; ?>
						</div>
					</div> <!-- /.section, /#navigation -->
				<?php endif; ?>
				<?php print $sidebar_first; ?>
				<?php print $sidebar_second; ?>
			</div>
		</div> <!-- /#main, /#main-wrapper -->

		<?php if ($footer || $footer_message || $secondary_links): ?>
			<div id="footer">
				<div class="section">
					<?php print theme(array('links__system_secondary_menu', 'links'), $secondary_links,
					  array(
						'id' => 'secondary-menu',
						'class' => 'links clearfix',
					  ),
					  array(
						'text' => t('Secondary menu'),
						'level' => 'h2',
						'class' => 'element-invisible',
					  ));
					?>
					<?php if ($footer_message): ?>
						<div id="footer-message"><?php print $footer_message; ?></div>
					<?php endif; ?>
					<?php print $footer; ?>
				</div>
			</div> <!-- /.section, /#footer -->
		<?php endif; ?>
	</div>
</div> <!-- /#page, /#page-wrapper -->
<?php print $page_closure; ?>
<?php print $closure; ?>
</body>
</html>
