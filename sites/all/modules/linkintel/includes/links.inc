<?php
function linkintel_node_links_page($node, $mode, $dir = 'out') {
  linkintel_set_admin_theme();  
  $filter = array();
  $filter['args'] = array($node->nid);
  if($dir == 'back') {
    drupal_set_title('Backlinks');
    $filter['where'] = 'nid = %d';    
  }
  else {
    drupal_set_title('Links');
    $filter['where'] = 'from_nid = %d';  
  }
  
  $header = linkintel_get_links_report_header('node', $dir);
  
  $result = linkintel_load_filtered_links_result($filter, $options, $header);

  $destination = 'node/' . $node->nid . '/linkintel/' . (($dir == 'back') ? 'backlinks' : 'links');
  $output = theme_linkintel_links_report($header, $result, $destination, 'node', $dir, $node);

  $qa = '';
  $qa = (($dir == 'back') ? '&nid=' : '&from_nid=') . $node->nid;
  $output .= l(t('Add link'), 'admin/content/linkintel/link/edit/', array('query' => 'destination=' . $destination . $qa));  
  
  return $output;
}

/**
 * Generates table of keywords associated with a pages
 * 
 * @param int|str|nodeobj $pid - page id; the nid (int) or path (str) of the page 
 */
function linkintel_links_page() {

  $filter = linkintel_links_build_filter_query();

  $output .= drupal_get_form('linkintel_links_filter_form');

  $header = linkintel_get_links_report_header();
  
  $result = linkintel_load_filtered_links_result($filter, $options, $header);

  $destination = 'admin/content/linkintel/links';
  $output .= theme_linkintel_links_report($header, $result, $destination);
  
  return $output;
}

function linkintel_get_links_report_header($mode = 'all', $dir = 'out') {
  $header = array();
  $header[] = array('data' => t('Text'), 'field' => 'atext');    
  if($dir == 'out') { 
    $header[] = array('data' => t('To path'), 'field' => 'l.path');
    $header[] = array('data' => t('nid'), 'field' => 'l.nid'); 
  }
  if(($mode == 'all') || ($dir == 'back')) {    
    $header[] = array('data' => t('From path'), 'field' => 'from_pid');
    $header[] = array('data' => t('nid'), 'field' => 'from_nid'); 
  }
  $header[] = array('data' => t('Mode'), 'field' => 'l.mode');
  $header[] = array('data' => t('Autogen'), 'field' => 'l.autogen');
  $header[] = array('data' => t('Nofollow'), 'field' => 'l.nofollow');
  $header[] = array('data' => t('Request'), 'field' => 'l.rid');
  $header[] = array('data' => t('Ops'));  
  return $header;
}

function theme_linkintel_links_report($header, $result, $destination, $mode = 'all', $dir = 'out', $node = NULL) {
  $linkmodes = linkintel_get_mode_options('report');
  
  $rows = array();
  $autogen_options = linkintel_get_autogen_options();
  while ($r = db_fetch_object($result)) {
    //$attributes = unserialize($r->attributes);
    $row = array();
    if($r->mode && ($r->mode < 10)) {
      $ops = l(t('edit'), 'admin/content/linkintel/link/edit/' . $r->lid, array('query' => 'destination=' . $destination));
      $ops .= ' | ' . l(t('delete'), 'admin/content/linkintel/link/delete/' . $r->lid, array('query' => 'destination=' . $destination));
    } else if($r->from_nid) {
      $ops = l(t('edit node'), 'node/' . $r->from_nid . '/edit', array('query' => 'destination=' . $destination));
    }
    $to_options = array();
    $from_options = array();
    if($r->to_external) {
      $to_options['absolute'] = TRUE;
    }
    if($r->external) {
      $from_options['absolute'] = TRUE;
    }
    $row[] = ($r->atext) ? check_plain($r->atext) : t('[default]');
 
    if($dir == 'out') { 
      $row[] = linkintel_l_report($r->path, FALSE); 
      $row[] = ($r->nid) ? $r->nid : '-'; 
    }
    if(($mode == 'all') || ($dir == 'back')) {    
      $row[] = linkintel_l_report($r->from_path, FALSE);
      $row[] = ($r->from_nid) ? $r->from_nid : '-';  
    }    
    
    $row[] = $linkmodes[$r->mode];
    $row[] = $autogen_options[$r->autogen];
    $row[] = ($r->nofollow) ?  t('Y') : t('N'); 
    $row[] = ($r->rid) ?  l($r->rid, 'admin/content/linkintel/request/edit/' . $r->rid, array('query' => 'destination=' . $destination)) : '-';    
    $row[] = $ops;
    
    $rows[] = array('data' =>
      $row,
      // Attributes for tr
      'class' => "linkintel"
    );
  }

  if (!$rows) {
    $rows[] = array(array('data' => t('No links were found.'), 'colspan' => count($header)));
  }  

  $output .= theme('table', $header, $rows, array('id' => 'linkintel-request'));
  $output .= theme('pager', NULL, 100, 0);

  return $output;  
}

function linkintel_links_build_filter_query() {
  if (empty($_SESSION['linkintel_links_filter'])) {
    return;
  }

  $filters = linkintel_links_filters();

  // Build query
  $where = $args = array();
//dsm($_SESSION['kwresearch_overview_filter']);
  foreach ($_SESSION['linkintel_links_filter'] as $key => $filter) {
//dsm("$key = $filter " . $filters[$key]['type']);
    if($key == 'like_match') {
      continue;
    }
    $filter_where = array();

    if ($filters[$key]['type'] == 'select') {
      if(($key == 'mode') && ($filter == 0)) {
        continue;
      }
      $where[] = $filters[$key]['where'];
      // fix options index bug
      if($key == 'mode') {        
        $args[] = $filter-1;
      }
      else {
        $args[] = $filter;
      }
    }
    elseif ($filter) {      
      if ($key == 'keywords') {        
        if($_SESSION['linkintel_links_filter']['like_match']) {
          $filter_where[] = $filters['like_match']['where'];
          // there are two of them
          $args[] = $filter;
          $args[] = $filter;
        }
        else {
          $keywords = explode(',', $filter);
          $placeholders = implode(',', array_fill(0, count($keywords), '"%s"'));
          $filter_where[] = str_replace('"%s"', $placeholders, $filters[$key]['where']);
          // got to double the inserts
          foreach($keywords AS $keyword) {
            $keyword = trim(strtolower($keyword));
            $args[] = $keyword;
          }
          foreach($keywords AS $keyword) {
            $keyword = trim(strtolower($keyword));
            $args[] = $keyword;
          }
        }
      }
      else if(($key == 'from') || ($key == 'to')) {
        // treat as nid input
        $a = explode('/', $filter);
        if(($a[0] == 'node' && is_numeric($a[1])) || ($filter > 0)) {
          $nid = (($a[0] == 'node' && is_numeric($a[1])))? $a[1] : $filter;
          $filter_where[] = ($key == 'to') ? 'to_nid = %d' : 'nid = %d';
          $args[] = $nid;
        }
        else {
          $filter_where[] = $filters[$key]['where'];
          $args[] = $filter;         
        }
      }
      else {
        $filter_where[] = $filters[$key]['where'];
        $args[] = $filter;
      }      
    }
    if (!empty($filter_where)) {
      $where[] = '('. implode(' OR ', $filter_where) .')';
    }
  }
  $where = !empty($where) ? implode(' AND ', $where) : '';

  return array(
    'where' => $where,
    'args' => $args,
  );
}

/**
 * Return form for site keyword list filters.
 *
 */
function linkintel_links_filter_form() {
  $session = &$_SESSION['linkintel_links_filter'];
  $session = is_array($session) ? $session : array();
  $filters = linkintel_links_filters();

  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter links'),
    '#theme' => 'dblog_filters',
    '#collapsible' => TRUE,
    '#collapsed' => empty($session),
  );
  foreach ($filters as $key => $filter) {
    if ($filter['type'] == 'textfield') {
      $form['filters']['status'][$key] = array(
        '#title' => $filter['title'],
        '#type' => 'textfield',
        '#size' => ($filter['size'])? $filter['size']:8,
      );    
    }
    elseif ($filter['type'] == 'select') {
      $form['filters']['status'][$key] = array(
        '#title' => $filter['title'],
        '#type' => 'select',
        '#options' => $filter['options'],
      );
    }
    elseif ($filter['type'] == 'checkbox') {
      $form['filters']['status'][$key] = array(
        '#title' => $filter['title'],
        '#type' => 'checkbox',
        '#return_value' => $filter['return_value'],
      );
    }
    elseif ($filter['type'] == 'markup') {
      $form['filters']['status'][$key] = array(
        '#type' => $filter['type'],
        '#value' => $filter['value'],
      );       
    }
    if (!empty($session[$key])) {
      $form['filters']['status'][$key]['#default_value'] = $session[$key];
    }
  }

  $form['filters']['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );
  if (!empty($session)) {
    $form['filters']['buttons']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset')
    );
  }

  return $form;
}

/**
 * Returns filters for site keywords report
 */
function linkintel_links_filters() {
  $filters = array();

  $options = linkintel_get_mode_options();
  $filters['keywords'] = array(
    'title' => t('Keywords'),
    'type' => 'textfield',
    'size' => 60,
    'description' => t('Enter a comma seperated list of keywords to filter by exact match.'),
    'where' => 'k.keyword IN ("%s") OR l.text IN ("%s")',
  ); 
  $filters['like_match'] = array(
    'title' => t('Like match'),
    'type' => 'checkbox',
    'return_value' => 1,
    'where' => 'k.keyword LIKE "%%%s%%" OR l.text LIKE "%%%s%%"',
  );
  $filters['from'] = array(
    'title' => t('From'),
    'type' => 'textfield',
    'size' => 60,
    'where' => 'path = "%s"',
    'description' => t('Input a path or nid.'),
  );  
  $filters['to'] = array(
    'title' => t('To'),
    'type' => 'textfield',
    'size' => 60,
    'where' => 'href = "%s"',
    'description' => t('Input a path or nid.'),
  );
  $filters['external'] = array(
    'title' => t('From external'),
    'type' => 'checkbox',
    'return_value' => 1,
    'where' => 'l.external = %d',
  ); 
  $filters['to_external'] = array(
    'title' => t('To external'),
    'type' => 'checkbox',
    'return_value' => 1,
    'where' => 'l.to_external = %d',
  ); 

  $options = array(0 => t('All'));
  $options = array_merge($options, linkintel_get_mode_options());
  //foreach(linkintel_get_mode_options() AS $k => $v) {
  //  $options[$k] = $v;
  //}
  $filters['mode'] = array(
    'title' => t('Mode'),
    'type' => 'select',
    'options' => $options,
    'where' => 'l.mode = %d',
  ); 
  $filters['autogen'] = array(
    'title' => t('Auto Generated'),
    'type' => 'checkbox',
    'return_value' => 1,
    'where' => 'l.autogen = %d',
  ); 
  $filters['nofollow'] = array(
    'title' => t('Nofollow'),
    'type' => 'checkbox',
    'return_value' => 1,
    'where' => 'l.nofollow = %d',
  );
  return $filters;
}

/**
 * Validate result from site keyword report filter form.
 */
function linkintel_links_filter_form_validate($form, &$form_state) {
  if ($form_state['values']['op'] == t('Filter') 
    && empty($form_state['values']['keywords'])
    && empty($form_state['values']['from']) 
    && empty($form_state['values']['to'])
    && empty($form_state['values']['external'])
    && empty($form_state['values']['to_external'])
    && ($form_state['values']['mode'] == 0)
    && empty($form_state['values']['autogen'])
    && empty($form_state['values']['nofollow'])
  ) {
    form_set_error('type', t('You must select something to filter by.'));
  }
}

/**
 * Process result from site keywords report filter form.
 */
function linkintel_links_filter_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  $filters = linkintel_links_filters();
  switch ($op) {
    case t('Filter'):
      foreach ($filters as $name => $filter) {
        if (isset($form_state['values'][$name])) {
          $_SESSION['linkintel_links_filter'][$name] = $form_state['values'][$name];
        }
      }
      break;
    case t('Reset'):
      $_SESSION['linkintel_links_filter'] = array();
      break;
  }
  return 'admin/content/linkintel/links';
}

/**
 * Generages page keyword edit form
 * 
 * @param int|str $pid - page id; the nid (int) or path (str) of the page
 * @param int $kid - keyword id
 */
function linkintel_link_edit_page($lid = '') {
  if($lid > 0) {
    $link = linkintel_load_link($lid);
  }
  if ($link->lid) {
    drupal_set_title(t('Edit link'));
  }
  else {
    drupal_set_title(t('Add link'));
    $link = new stdClass();
    if($_GET['from_nid']) {
      $link->from_nid = $_GET['from_nid'];
      $link->from_path = 'node/' . $_GET['from_nid'];
    }
    if($_GET['from_path']) {
      $link->from_path = $_GET['from_path'];
    }
    if($_GET['nid']) {
      $link->nid = $_GET['nid'];
      $link->path = 'node/' . $_GET['nid'];
    }
    if($_GET['path']) {
      $link->path = $_GET['path'];
    }
    if($_GET['rid']) {
      $request = linkintel_load_request($_GET['rid']);
      //$link->path = $request->path;
      $link->text = $request->keyword;
      $link->keyword = $request->keyword;
      $link->kid = $request->kid;
      $link->path = $request->path;
      $link->nid = $request->nid;
      $link->mode = ($link->keyword) ? 1 : 2;
      if(is_array($link->attributes)) {
        foreach($link->attributes AS $attr => $v) {
          $link->attributes[$attr] = $v;
        }
      }
    }
  }  
  $output .= drupal_get_form('linkintel_link_edit_form', $link);
  return $output;
}

/**
 * Returns page keyword edit form
 * 
 * @param arr $form_state
 * @param int|str $pid - page id; the nid (int) or path (str) of the page
 * @param str $keyword
 */

function linkintel_link_edit_form($form_state, $link = stdClass) {
//dsm($link);  
  $form['lid'] = array(
    '#type' => 'value',
    '#value' => ($link->lid) ? $link->lid : ''
  );
//dsm($link);
  if($link->from_path) {
    $from_link = linkintel_l_report($link->from_path);
    $form['from_page'] = array(
        '#title' => t('From page'),
        '#type' => 'item',
        '#value' =>  $from_link,
    );
    $form['from_nid'] = array(
      '#type' => 'value',
      '#value' => $link->from_nid
    );
    $form['from_path'] = array(
      '#type' => 'value',
      '#value' => $link->from_path
    );
    if($link->from_nid) {
      $node = node_load($link->from_nid);
      $report = linkintel_node_links_page($node, 'node', 'out');
      $form['page_links'] = array(
        '#type' => 'fieldset',      
        '#title' => t('Existing page links'),      
        '#collapsible' => TRUE,      
        '#collapsed' => TRUE,    
      ); 
      $form['page_links']['report'] = array(
        '#type' => 'markup',
        '#value' => $report,
      );     
    }
  }
  else {
    $form['from_path'] = array(
      '#type' => 'textfield',
      '#title' => t('From page'),
      '#required' => TRUE,
      '#description' => t('Enter the page this link should be added.'),
    );    
  }

  $options = linkintel_get_mode_options();
  $form['mode'] = array(
    '#type' => 'select',
    '#title' => t('Mode'), 
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => ($link->mode) ? $link->mode:2,
    '#description' => t('Determines where the link will be displayed: 
      <ul>
        <li><em>Coded</em> - Adds markup to content transforming the first occurance of the keyword phrase into a hyperlink.</li>
        <li><em>In content</em> - Transforms first occurance of the keyword phrase into a hyperlink on page load.</li>
        <li><em>Sidebar</em> - Included in the sidebar links.</li>
      </ul>
    '),
  );
  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('URL path'),
    '#default_value' => $link->path,
    '#required' => TRUE,
    '#description' => t('The url you want to link to. This can be an internal Drupal path such as node/add or an external URL such as http://drupal.org. Enter &lt;front&gt; to link to the front page.'),
  );  
  $form['text'] = array(
    '#type' => 'textfield',
    '#title' => t('Anchor text'), 
    '#default_value' => $link->text,
    '#description' => t('Anchor text is the clickable text of a hyperlink. For <em>coded</em> or <em>keyword</em> mode link, this is the keyword that get converted to a link therefor it must exist in the node body. For sidebar links, this is the clickable text. For sidebar links that link to another node, this can be left blank and the linked to node title will be used.'),
  ); 
  $form['kid'] = array(
    '#type' => 'value',
    '#value' => $link->kid,
  );
  $form['keyword'] = array(
    '#type' => 'value',
    '#value' => $link->keyword,
  );
  if(($link->mode == 1) || ($link->mode == 2)) {
    $form['autogen'] = array(
      '#type' => 'checkbox',
      '#title' => t('Autogen'), 
      '#return_value' => 1,
      '#default_value' => $link->autogen,
      '#description' => t('Uncheck if you want this link to be permanent, e.g. it can only be modifed by a user. Check if you want Link Intelligence to automatically manage this link, e.g. Link Intelligence might delete this link on future link autogens. For content links, it is recommened to make a link permanent by changing its mode to code rather than unsetting autogen.'),
    ); 
  }
  else {
    $form['autogen'] = array(
      '#type' => 'value',
      '#value' => $link->autogen,
    );    
  }
  
  $form['attributes'] = array(
    '#type' => 'fieldset',      
    '#title' => t('Attributes'),      
    '#collapsible' => TRUE,      
    '#collapsed' => TRUE,    
  );
  $attributes = linkintel_get_link_attributes();
//dsm($link);
  foreach($attributes AS $attr => $lable) {
    $form['attributes'][$attr] = array(
      '#type' => 'textfield',
      '#title' => $lable,
      '#default_value' => $link->attributes[$attr],
    ); 
  }
  $form['attributes']['rel']['#type'] = 'checkbox';
  $form['attributes']['rel']['#return_value'] = 'nofollow';
  
    
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  
  return $form;
}

/**
 * Validates the page keyword edit form
 * 
 * @param $form
 * @param $form_state
 */
function linkintel_link_edit_form_validate($form, &$form_state) {
  // validate from page
  $link = $form_state['values']['from_path'];
  linkintel_parse_link_path($link, $msgs, 'form', 'from_path');
  if($link->nid) {
    $form_state['values']['from_nid'] = $link->nid;
  }

  if($link->external) {
    form_set_error('path', t('You can only add links from pages that are in your Drupal site.'));
  } 
  
  if(!$link->nid) {
    if($form_state['values']['mode'] == 0) {
      form_set_error('path', t('You can only add coded links to nodes. Please select a valid node from page or select the Sidebar mode.'));
    }
    else if($form_state['values']['mode'] == 1) {
      form_set_error('path', t('You can only add content links to nodes. Please select a valid node from page or select the Sidebar mode.'));
    }
  } 
  else {
    if($form_state['values']['mode'] < 2) {
      $node = node_load($link->nid);
      $link->text = $form_state['values']['text'];
      $regex = linkintel_get_regex_for_link($link);
      preg_match($regex, $node->body, $found);
      if(!count($found)) {
        form_set_error('text', t('The phrase "%phrase" was not found in the body of node !node. Please select a text that exists in the node body or select Sidebar link mode.',
          array(
            '%phrase' => $form_state['values']['text'],
            '!node' => l('node/' . $node->nid, 'node/' . $node->nid),
          )
        ));
      }
    }
  }
  // validate link to path
  $link = new stdClass();
  $link = $form_state['values']['path'];
  linkintel_parse_link_path($link, $msgs2, 'form');
  if(!trim($form_state['values']['text'])) {
    if(!$link->nid) {
      form_set_error('text', t('You can only leave the anchor text blank when linking to a node. Either add achnor text or link to a valid node.'));
    }
  }
}

/**
 * Processes the page keyword edit form
 * 
 * @param $form
 * @param $form_state
 */
function linkintel_link_edit_form_submit($form, &$form_state) {
//dsm($form_state['values']);
  $link = new stdClass();
  $link->lid = $form_state['values']['lid'];
  $link->from_path = $form_state['values']['from_path'];
  $link->from_nid = $form_state['values']['from_nid'];
  $link->uid = $_GLOBALS['user']->uid;
  $link->mode = $form_state['values']['mode'];
  if($link->mode == 0 || $link->mode == 1) {
    $link->keyword = $form_state['values']['text'];
  }
  $link->text = $form_state['values']['text'];
  $link->path = $form_state['values']['path'];
  $link->nid = $form_state['values']['nid'];
  
  $link->autogen = $form_state['values']['autogen'];  
  
  $link->attributes = array();
  $attributes = linkintel_get_link_attributes();
  foreach($attributes AS $attr => $lable) {
    if($form_state['values'][$attr]) {
        $link->attributes[$attr] = $form_state['values'][$attr];
    }
  }
  
  if($link->mode == 0) {
    $node = node_load($link->from_nid);
//dsm($form_state['values']);
//dsm($link);
    $link->keyword = ($form_state['values']['keyword']) ? $form_state['values']['keyword'] : $link->text;
    $node->body = linkintel_link_keywords($node->body, array($link), 'node/' . $link->nid, TRUE);
    $node->teaser = node_teaser($node->body);
    $node->linkintel_action = 'save_coded_link';
//dsm($node);
    node_save($node);    
    $action = t('embeded into');
  }
  else {
    linkintel_save_link($link);
    $action =  t('saved from');
  }

  $from_link = new stdClass();
  $from_link->path = $link->from_path;
  $options = linkintel_get_mode_options();
  $mode = strtolower($options[$link->mode]);
  drupal_set_message(t('A %mode link was @action page !from_link to !to_link.', 
    array(
      '%mode' => $mode,
      '@action' => $action,
      '!to_link' => linkintel_l_report($link),
      '!from_link' => linkintel_l_report($from_link),
      '%keyword' => $link->keyword
    )),
    'status'
  );
}

function linkintel_link_delete_page($lid) {
  drupal_set_title(t('Delete link'));
  $link = linkintel_load_link($lid);
  if(!$link->lid) {
    drupal_set_message(t('A %mode link with ID @id was not found.', array('@id' => $link->lid)), 'error');
    return '';
  }
  if($link->mode == 0) {
    drupal_set_message(t('The link you are trying to delete is coded into content. You must remove coded links directly from the content. !link.', 
      array(
        '!link' => l(t('Edit content'), 'node/' . $link->nid),
      )), 
      'error');
    return '';
  }
  $output .= drupal_get_form('linkintel_link_delete_form', $link);
  return $output;
}

/**
 * Generages page keyword edit form
 * 
 * @param int|str $pid - page id; the nid (int) or path (str) of the page
 * @param int $kid - keyword id
 */
function linkintel_link_delete_form($form_state, $link) {
//dsm($link);
  $form['message'] = array(
  '#type' => 'markup',
  '#value' => t('Are you sure you want to delete the link <em>!link</em> on page <em>!page</em>?',
      array(
        '!link' => l($link->text, $link->href),
        '!page' => l($link->path, $link->path),
      )
    ),
  );
  
  $form['lid'] = array(
    '#type' => 'value',
    '#value' => $link->lid,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  $form['cancel'] = array(
      '#type' => 'markup',
      '#value' => l(t('Cancel'), $_GET['destination']),
  );
  
  return $form;
}

function linkintel_link_delete_form_submit($form, &$form_state) {

  $link = linkintel_load_link($form_state['values']['lid']);

  linkintel_delete_link($link);
  
  drupal_set_message(t('The link <em>!link</em> was deleted on page <em>!page</em>?',
    array(
      '!link' => l($link->text, $link->href),
      '!page' => l($link->path, $link->path),
     )),
    'status'
  );
}
