$(document).ready(function() {
var hideClientFields = $.cookie('hideClientFields');
if(hideClientFields == 'true'){
	$("div.views-field-field-client-projects-value").hide();
	$("div.views-field-field-client-repo-value").hide();
	$("div.views-field-field-client-user-uid").hide();
	$("div.views-field-field-client-email-email").hide();
	$("div.views-field-field-client-phone-value").hide();
	$("div.views-field-field-client-address-value").hide();
	$(".toggle-client").text("expand");
} else {
	$("div.views-field-field-client-projects-value").show();
	$("div.views-field-field-client-repo-value").show();
	$("div.views-field-field-client-user-uid").show();
	$("div.views-field-field-client-email-email").show();
	$("div.views-field-field-client-phone-value").show();
	$("div.views-field-field-client-address-value").show();
	$(".toggle-client").text("collapse");
}

$("#hidePanel").click(function() {
    $("ul.primary").css("display", "none");
});

if (status == 'Outstanding') { 
    $(".field-field-invoice-status").addClass("status-outstanding");
    $("#status-icon-paid").css("display", "none");
    $("#status-icon-outstanding").css("display", "inline");
}
    else if (status == 'Payment Received') {
    $(".field-field-invoice-status").addClass("status-paid");
    $("img#status-icon-outstanding").css("display", "none");
    $("#status-icon-paid").css("display", "inline");
}

$("#dhtml_menu-186").bt({
        positions: ['right', 'top', 'left', 'bottom'],
        fill: '#FFFFFF',
        padding: '21px',
        strokeWidth: 1,
        strokeStyle: "#474747",
        cornerRadius: 5,
        spikeGirth: 33,
        spikeLength: 15,
        overlap: -1,
        cssStyles: {fontWeight: 'bold', fontSize: '14pt'}
});

$("#dhtml_menu-198").bt({
        positions: ['right', 'top', 'left', 'bottom'],
        fill: '#FFFFFF',
        padding: '21px',
        strokeWidth: 1,
        strokeStyle: "#474747",
        cornerRadius: 5,
        spikeGirth: 33,
        overlap: -1,
        cssStyles: {fontWeight: 'bold', fontSize: '14pt'}
});

$("#dhtml_menu-389").bt({
        positions: ['right', 'top', 'left', 'bottom'],
        fill: '#FFFFFF',
        padding: '21px',
        strokeWidth: 1,
        strokeStyle: "#474747",
        cornerRadius: 5,
        spikeGirth: 33,
        overlap: -1,
        cssStyles: {fontWeight: 'bold', fontSize: '14pt'}
});

$("#dhtml_menu-892").bt({
        positions: ['right', 'top', 'left', 'bottom'],
        fill: '#FFFFFF',
        padding: '21px',
        strokeWidth: 1,
        strokeStyle: "#474747",
        cornerRadius: 5,
        spikeGirth: 33,
        overlap: -1,
        cssStyles: {fontWeight: 'bold', fontSize: '14pt'}
});

$("#dhtml_menu-929").bt({
        positions: ['right', 'top', 'left', 'bottom'],
        fill: '#FFFFFF',
        padding: '21px',
        strokeWidth: 1,
        strokeStyle: "#474747",
        cornerRadius: 5,
        spikeGirth: 33,
        overlap: -1,
        cssStyles: {fontWeight: 'bold', fontSize: '14pt'}
});

$("#dhtml_menu-3048").bt({
        positions: ['right', 'top', 'left', 'bottom'],
        fill: '#FFFFFF',
        padding: '21px',
        strokeWidth: 1,
        strokeStyle: "#474747",
        cornerRadius: 5,
        spikeGirth: 33,
        overlap: -1,
        cssStyles: {fontWeight: 'bold', fontSize: '14pt'}
});

$(".views-field-field-invoice-status-value span.invoice_paid").bt({
        positions: ['right', 'top', 'left', 'bottom'],
        fill: '#D1EFD5',
        padding: '10px',
        strokeWidth: 1,
        strokeStyle: "#6F6F6F",
        cornerRadius: 5,
        spikeGirth: 20,
        spikeLength: 10,
        overlap: -5,
        width: 210
});

$(".views-field-field-invoice-status-value span.invoice_outstanding").bt({
        positions: ['right', 'top', 'left', 'bottom'],
        fill: '#FFDFDF',
        padding: '10px',
        strokeWidth: 1,
        strokeStyle: "#6F6F6F",
        cornerRadius: 5,
        spikeGirth: 20,
        spikeLength: 10,
        overlap: -5,
        width: 265
});

$("td.views-field-view-node .front-view").bt({
        positions: ['right', 'top', 'left', 'bottom'],
        fill: '#FFFFFF',
        padding: '10px',
        strokeWidth: 1,
        strokeStyle: "#6F6F6F",
        cornerRadius: 5,
        spikeGirth: 33,
        overlap: 10,
        width: 130
});

$("td.views-field-view-node .front-download").bt({
        positions: ['right', 'top', 'left', 'bottom'],
        fill: '#FFFFFF',
        padding: '10px',
        strokeWidth: 1,
        strokeStyle: "#6F6F6F",
        cornerRadius: 5,
        spikeGirth: 33,
        overlap: 10,
        width: 170
});

$("td.views-field-view-node .front-print").bt({
        positions: ['right', 'top', 'left', 'bottom'],
        fill: '#FFFFFF',
        padding: '10px',
        strokeWidth: 1,
        strokeStyle: "#6F6F6F",
        cornerRadius: 5,
        spikeGirth: 20,
        spikeLength: 10,
        overlap: 20,
        width: 145
});

$("#user-bar-block .user-logout").bt({
        positions: ['bottom'],
        fill: '#6F6F6F',
        padding: '21px',
        strokeWidth: 1,
        strokeStyle: "#474747",
        cornerRadius: 5,
        spikeGirth: 33,
        overlap: 0,
        width: 175,
        cssStyles: {fontWeight: 'bold'}
});

$("a#invoice-download-link").bt({
        positions: ['right', 'top', 'left', 'bottom'],
        fill: '#FFFFFF',
        padding: '10px',
        strokeWidth: 1,
        strokeStyle: "#6F6F6F",
        cornerRadius: 5,
        spikeGirth: 33,
        overlap: 0,
        width: 170
});

$("span.toggle-client").bt({
        positions: ['bottom', 'top', 'left', 'right'],
        fill: '#FFFFFF',
        padding: '10px',
        strokeWidth: 1,
        strokeStyle: "#6F6F6F",
        cornerRadius: 5,
        spikeGirth: 33,
        overlap: 0,
        width: 190
});

$(".toggle-client").click(function() {
	var options = {path: '/', expires: 7};
	if ($("div.views-field-field-client-projects-value").is(":visible")) {
		$(this).text("expand");
		$("div.views-field-field-client-projects-value").hide();
		$("div.views-field-field-client-repo-value").hide();
		$("div.views-field-field-client-user-uid").hide();
		$("div.views-field-field-client-email-email").hide();
		$("div.views-field-field-client-phone-value").hide();
		$("div.views-field-field-client-address-value").hide();
		$.cookie("hideClientFields", "true", "options");
	} else {
		$(this).text("collapse");
		$("div.views-field-field-client-projects-value").show();
		$("div.views-field-field-client-repo-value").show();
		$("div.views-field-field-client-user-uid").show();
		$("div.views-field-field-client-email-email").show();
		$("div.views-field-field-client-phone-value").show();
		$("div.views-field-field-client-address-value").show();
		$.cookie("hideClientFields", "false", "options");
	}
});

});

