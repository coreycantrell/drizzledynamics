Module: Ajax Attribute Calculations
Author: Chris Yu
Sponsor: Classic Graphics (http://www.classicgraphics.com)

This module works in tandem with the Ubercart module
(http://drupal.org/project/ubercart), adding the ability to dynamically
update an Ubercart customer's product view page when price affecting
attributes are selected and changed.

This module is available for both Drupal 5 and Drupal 6 versions of Ubercart.

Special thanks to deekayen and tecto for taking some time to code review and provide 'drupal way' insight. 

BUGS
----
http://drupal.org/project/issues/uc_aac