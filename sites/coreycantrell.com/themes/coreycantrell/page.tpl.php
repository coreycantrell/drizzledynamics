﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
<title><?php print $head_title; ?></title>
<meta name="msvalidate.01" content="9C10FFCAC889E99F74FCA0B1725A85E4" />
<meta name="google-site-verification" content="jmdJG2ia4IK3naMlbrM9nFjUT0dX8GFnZ4b8pWAT2iw" />
<m name="y_key" content="19bfae464e4ffabf" />
<meta name="Description" content="A blog from the mind of Corey Cantrell covering technology, apps, life, and anything else.">
<meta name="keywords" content="web, graphic, design, development, blog, portfolio, cory, cantrel" />
<meta property="og:title" content="<?php print $node->title ?>"/>
<meta property="og:site_name" content="CoreyCantrell.com"/>
<meta property="og:type" content="website"/>
<meta property="fb:admins" content="coreycantrell"/>
<meta property="og:url" content="http://www.coreycantrell.com/<?php print $node->path ?>"/>
<?php
function limit_text($text, $limit) {
      if (strlen($text) > $limit) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
          $textraw = substr($text, 0, $pos[$limit]) . '...';
          $text = strip_tags($textraw);
      }
      return $text;
    }
?>
<meta property="og:description" content="<?php echo limit_text($node->content['body']['#value'], 25); ?>"/>
<meta property="og:image" content="http://www.coreycantrell.com/<?php print $node->field_blog_image[0]['filepath'] ?>"/>
<?php print $head; ?>
<?php print $styles; ?>
<link href="https://s3.amazonaws.com/scripts.drizzledynamics.com/formalize/formalize.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Karla:400,700|Enriqueta:400,700' rel='stylesheet' type='text/css'>
<?php print $scripts; ?>
<script type="text/javascript" src="http://platform.twitter.com/anywhere.js?id=5pr0MsZv2lo7pHF8BhYxA&v=1"></script>
<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/dzdy.required.js"></script>
<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/jquery.jcarousel.min.js"></script>
<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/formalize/jquery.formalize.js"></script>
</head>

<body class="<?php print $body_classes; ?>">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
<script type="text/javascript">
(function() {
    var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
    s.type = 'text/javascript';
    s.async = true;
    s.src = 'http://widgets.digg.com/buttons.js';
    s1.parentNode.insertBefore(s, s1);
})();
</script>
<!--[if IE]>
  <script type="text/javascript">
  $(document).ready(function(){
	var theframes = document.getElementsByTagName('iframe');
	for(var i = 0; i < theframes.length; i++){
		theframes[i].setAttribute("allowTransparency","true");
	}
  });
  </script>
<![endif]-->

<a id="top"></a>
    <div id="header">
        <div id="header-design">
			<div id="header-inner">
				<?php if ($primary_links): ?>
					<div id="header-links"><?php print theme('links', $primary_links); ?></div>
				<?php endif; ?>
				<div id="header-logo" onclick="location.href='/';">
					&nbsp;
				</div>
			</div>
        </div>
    </div>

  <div id="page"><div id="page-inner">

    <div id="main"><div id="main-inner" class="clear-block<?php if ($search_box || $primary_links || $secondary_links || $navbar) { print ' with-navbar'; } ?>">

      <div id="content"><div id="content-inner">

        <?php if ($content_top): ?>
          <div id="content-top" class="region region-content_top">
            <?php print $content_top; ?>
          </div>
        <?php endif; ?>
        <?php if ($tabs || $help || $messages || $title): ?>
            <?php print $messages; ?>
            <?php if ($tabs): ?>
              <div class="tabs"><?php print $tabs; ?></div>
            <?php endif; ?>
            <?php print $help; ?>
          </div>
        <?php endif; ?>

        <div id="content-area">
          <?php print $content; ?>
        </div>

        <?php if ($content_bottom): ?>
          <div id="content-bottom" class="region region-content_bottom">
            <?php print $content_bottom; ?>
          </div>
        <?php endif; ?>

      </div></div>

      <?php if ($secondary_links || $navbar): ?>
        <div id="navbar"><div id="navbar-inner" class="clear-block region region-navbar">

          <a name="navigation" id="navigation"></a>

          <?php if ($secondary_links): ?>
            <div id="secondary">
              <?php print theme('links', $secondary_links); ?>
            </div>
          <?php endif; ?>

          <?php print $navbar; ?>

        </div></div>
      <?php endif; ?>

    </div></div>
  <div id="toTop" style="display:none"><a href="#top">Top</a></div>
  </div></div>

    <div id="footer">
        <div id="footer-inner" class="region region-footer">
        <?php print $footer; ?>
        </div>
    </div>
    
    <div id="copyright">
        <div id="copyright-inner">
            Copyright ©2012. Corey Cantrell. All Rights Reserved.<br/>
        </div>
    </div>
  
  <?php if ($closure_region): ?>
    <div id="closure-blocks" class="region region-closure"><?php print $closure_region; ?></div>
  <?php endif; ?>

  <?php print $closure; ?>

</body>
</html>
