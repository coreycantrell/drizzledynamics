﻿$(document).ready(function(){  

	$('.page-jobs td.views-field-title a').bt({
	  showTip: function(box){
		$(box).fadeIn(200);
	  },
	  hideTip: function(box, callback){
		$(box).animate({opacity: 0}, 100, callback);
	  },
	  fill:             "#FFFFFF",
	  strokeStyle:      "#232C1A",
	  width:            '400px',
	  positions:        ['right'],
	  cornerRadius:     5,
	  shadow:           false,
	  overlap:          -10,
	  cssStyles:        {
		color: '#232C1A',
		fontWeight: 'normal'
	  },
	  hoverIntentOpts: {
		interval: 0,
		timeout: 0
	  }
	});
	
	$('.view-id-Front_Slideshow .view-content').cycle({ 
        fx:     'fade',
        speed:   400, 
        timeout: 5000, 
        next:   '#s3', 
        pause:   1
    });

});

