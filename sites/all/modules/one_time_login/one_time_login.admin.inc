<?php
// $Id: one_time_login.admin.inc,v 1.1.2.1 2008/04/07 01:25:13 mfb Exp $

/**
 * Output the CSV.
 */
function one_time_login_csv() {
  drupal_set_header('Content-Type: text/csv; charset=utf-8');
  drupal_set_header('Content-Disposition: inline; filename="one_time_login.csv"');
  print t('UID') .','. t('Name') .','. t('Mail') .','. t('URL') .','. t('Expiry') ."\r\n";
  $users = db_query('SELECT * FROM {users} WHERE uid > 0');
  $date = format_date(time() + 86400, 'small');
  while ($user = db_fetch_object($users)) {
    print $user->uid .',"'. $user->name .'","'. $user->mail .'","'. user_pass_reset_url($user) .'","'. ($user->login ? $date : '') ."\"\r\n";
  }
}

