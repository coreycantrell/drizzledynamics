<?php
// $Id: pages.inc,v 1.1.2.4 2011/01/05 20:50:30 tomdude48 Exp $

function linkintel_pages_page($options = '') {
  linkintel_input_get_as_filters();
  
  if($_GET['options']) {
    $options = $_GET['options'];
  }
  $a = explode(',', $options);
  $options = array();
  foreach($a AS $v) {
    $options[$v] = 1; 
  }
  drupal_set_title('Pages');

  $filter = linkintel_pages_build_filter_query();

  $output .= drupal_get_form('linkintel_pages_filter_form');
  
  $header = linkintel_get_pages_report_header();
  $options['link_counts'] = TRUE;
  $result = linkintel_load_filtered_pages_result($filter, $options, $header);
  
  $destination = 'admin/content/linkintel/pages';
  $output .= theme_linkintel_pages_report($header, $result, $destination);
  
  return $output;
}

function linkintel_get_pages_report_header($mode = 'all') {
  $header = array();
  $header[] = array('data' => t('Page'), 'field' => 'p.path');
  $header[] = array('data' => t('nid'), 'field' => 'p.nid');
  $header[] = array('data' => t('Page priority'), 'field' => 'p.priority');
  $header[] = array('data' => t('Eff. priority'), 'field' => 'effective_priority', 'sort' => 'desc');
  $header[] = array('data' => t('Backlinks'), 'field' => 'backlink_count');
  //$header[] = array('data' => t('Links'), 'field' => 'link_count');  
  //$header[] = array('data' => t('Ops'));
  return $header;
}

function linkintel_input_get_as_filters() {
//dsm($_GET['filters']);
  if($_GET['filters']) {
    $_SESSION['kwresearch_overview_filter'] = json_decode($_GET['filters'], TRUE);
  }
}



function theme_linkintel_pages_report($header, $result, $destination, $mode = 'all', $node = NULL) {
  $priorities = linkintel_get_priority_options();  
  
  $rows = array();
  while ($r = db_fetch_object($result)) {
    $row = array();
    $row[] = linkintel_l_report($r->path, FALSE);
    $row[] = ($r->nid) ? $r->nid : '-';
    if(!isset($r->priority)) {
      $r->priority = t('[default]');
    }
    $t = ($priorities[$r->priority]) ? $priorities[$r->priority] : $r->priority;
    $row[] = l($t, 'admin/content/linkintel/page/edit', array('query' => 'pid=' . (($r->nid) ? $r->nid : $r->path) . '&destination=' . $destination));
    $row[] = ($priorities[$r->effective_priority]) ? $priorities[$r->effective_priority] : $r->effective_priority;
    $t = ($r->backlink_count) ? number_format($r->backlink_count) : '-';
    if($r->nid) {
      $row[] = l($t, 'node/' . $r->nid . '/linkintel/backlinks');
    }
    else {
      $row[] = $t;
    }
    //$row[] = ($r->link_count) ? number_format($r->link_count) : '-';
    //$row[] = $ops;
    
    $rows[] = array('data' =>
      $row,
      // Attributes for tr
      'class' => "linkintel"
    );
  }

  if (!$rows) {
    $rows[] = array(array('data' => t('No pages were found.'), 'colspan' => count($header)));
  }  

  $output .= theme('table', $header, $rows, array('id' => 'linkintel-request'));
  $output .= theme('pager', NULL, 100, 0);
  
  //$output .= l(t('Add request'), 'admin/content/linkintel/request/edit/', array('query' => 'destination=' . $destination ));
  return $output;
}

function linkintel_pages_build_filter_query() {
  if (empty($_SESSION['linkintel_pages_filter'])) {
    return;
  }

  $filters = linkintel_pages_filters();

  // Build query
  $where = $args = array();
  foreach ($_SESSION['linkintel_pages_filter'] as $key => $filter) {
    if($key == 'like_match') {
      continue;
    }
    $filter_where = array();
    if ($filters[$key]['type'] == 'select') {
      if(!is_array($filter)) {
        $filter = array($filter);
      }
      foreach ($filter as $value) {
        $filter_where[] = $filters[$key]['where'];
        $args[] = $value;
      }
    }
    elseif ($filter) {      
      if ($key == 'keywords') {        
        if($_SESSION['linkintel_pages_filter']['like_match']) {
          $filter_where[] = $filters['like_match']['where'];
          $args[] = $filter;
        }
        else {
          $keywords = explode(',', $filter);
          $placeholders = implode(',', array_fill(0, count($keywords), '"%s"'));
          $filter_where[] = str_replace('"%s"', $placeholders, $filters[$key]['where']);
          foreach($keywords AS $keyword) {
            $keyword = trim(strtolower($keyword));
            $args[] = $keyword;
          }
        }
      }
      else if($key == 'from') {
        // treat as nid input
        $a = explode('/', $filter);
        if(($a[0] == 'node' && is_numeric($a[1])) || ($filter > 0)) {
          $nid = (($a[0] == 'node' && is_numeric($a[1])))? $a[1] : $filter;
          $filter_where[] = 'r.nid = %d';
          $args[] = $nid;
        }
        else {
          $filter_where[] = $filters[$key]['where'];
          $args[] = $filter;         
        }
      }
      else {
        $filter_where[] = $filters[$key]['where'];
        $args[] = $filter;
      }      
    }
    if (!empty($filter_where)) {
      $where[] = '('. implode(' OR ', $filter_where) .')';
    }
  }
  $where = !empty($where) ? implode(' AND ', $where) : '';

  return array(
    'where' => $where,
    'args' => $args,
  );
}

/**
 * Return form for site keyword list filters.
 *
 */
function linkintel_pages_filter_form() {
  $session = &$_SESSION['linkintel_pages_filter'];
  $session = is_array($session) ? $session : array();
  $filters = linkintel_pages_filters();

  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter requests'),
    '#theme' => 'dblog_filters',
    '#collapsible' => TRUE,
    '#collapsed' => empty($session),
  );
  foreach ($filters as $key => $filter) {
    if ($filter['type'] == 'textfield') {
      $form['filters']['status'][$key] = array(
        '#title' => $filter['title'],
        '#type' => 'textfield',
        '#size' => ($filter['size'])? $filter['size']:8,
      );    
    }
    elseif ($filter['type'] == 'select') {
      $form['filters']['status'][$key] = array(
        '#title' => $filter['title'],
        '#type' => 'select',
        '#multiple' => TRUE,
        '#size' => 6,
        '#options' => $filter['options'],
      );
    }
    elseif ($filter['type'] == 'checkbox') {
      $form['filters']['status'][$key] = array(
        '#title' => $filter['title'],
        '#type' => 'checkbox',
        '#return_value' => $filter['return_value'],
      );
    }
    elseif ($filter['type'] == 'markup') {
      $form['filters']['status'][$key] = array(
        '#type' => $filter['type'],
        '#value' => $filter['value'],
      );       
    }
    if (!empty($session[$key])) {
      $form['filters']['status'][$key]['#default_value'] = $session[$key];
    }
  }

  $form['filters']['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );
  if (!empty($session)) {
    $form['filters']['buttons']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset')
    );
  }

  return $form;
}

/**
 * Returns filters for site keywords report
 */
function linkintel_pages_filters() {
  $filters = array();

  $options = linkintel_get_priority_options();
  $filters['page_priority'] = array(
    'title' => t('Page priority'),
    'type' => 'select',
    'where' => 'p.priority = %d',
    'options' => $options,
  );

  return $filters;
}

/**
 * Validate result from site keyword report filter form.
 */
function linkintel_pages_filter_form_validate($form, &$form_state) {
  if ($form_state['values']['op'] == t('Filter') 
    && empty($form_state['values']['page_priority'])

  ) {
    form_set_error('type', t('You must select something to filter by.'));
  }
}

/**
 * Process result from site keywords report filter form.
 */
function linkintel_pages_filter_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  $filters = linkintel_pages_filters();
  switch ($op) {
    case t('Filter'):
      foreach ($filters as $name => $filter) {
        if (isset($form_state['values'][$name])) {
          $_SESSION['linkintel_pages_filter'][$name] = $form_state['values'][$name];
        }
      }
      break;
    case t('Reset'):
      $_SESSION['linkintel_pages_filter'] = array();
      break;
  }
  return 'admin/content/linkintel/pages';
}

// $obj can be a node object, nid or path
function linkintel_node_edit_fieldset($obj, $context = 'page') {
  if(is_object($obj)) {
    $node = $obj;
    $pid = $node->nid;
  }
  else {
    $pid = $obj;
  }
  if($pid) {
    $settings = linkintel_load_page_settings($pid);
  }
  else if ($node->type) {
    $settings = linkintel_load_page_settings(NULL, $node->type);
  }
  else {
    return array();
  }
  $form['linkintel'] = array(
    '#type' => 'fieldset',
    '#title' => t('Link Intelligence'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#attributes' => array('class' => 'linkintel'),
  );
  $options = linkintel_get_priority_options();
  $form['linkintel']['linkintel_priority'] = array(
    '#type' => 'select',
    '#title' => ($node) ? t('Node priority') : t('Page priority'), 
    '#options' => $options,
    '#default_value' => (isset($settings->priority)) ? $settings->priority : variable_get('linkintel_type_'. $node->type .'_priority', variable_get('linkintel_priority', LINKINTEL_PRIORITY_DEFAULT)),
    '#description' => t('Use to set how nodes of this content type are. The higher the priority, the more likely they will be backlinked to. If unsure, just set to "Standard".'),
  );
  
  $form['linkintel']['autogen'] = array(
    '#type' => 'fieldset',
    '#title' => t('Link autogen settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#attributes' => array('class' => 'linkintel-autogen'),
  ); 
  $form['linkintel']['autogen'] = array_merge($form['linkintel']['autogen'], linkintel_get_autogen_subform($settings, $context)); 

  $form['linkintel']['sync'] = array(
    '#type' => 'fieldset',
    '#title' => t('Request sync settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#attributes' => array('class' => 'linkintel-sync'),
  ); 
  $form['linkintel']['sync'] = array_merge($form['linkintel']['sync'], linkintel_get_request_sync_subform($settings, $context));

  return $form;
}

function linkintel_page_settings_edit_page() {
  drupal_set_title(t('Edit page settings'));
  $pn = linkintel_construct_pathnid_obj($_GET['pid']);
  $output .= drupal_get_form('linkintel_page_settings_edit_form', $pn);
  return $output;
}

function linkintel_page_settings_edit_form($form_state, $pn) {
  if($pn->nid || $pn->path) {
    if($pn->nid) {
      $node = node_load($pn->nid);
    }
    $link = linkintel_l_report($pn);
    $form['page'] = array(
        '#title' => t('Page'),
        '#type' => 'item',
        '#value' =>  $link,
    );
    $form['nid'] = array(
      '#type' => 'value',
      '#value' => $pn->nid
    );
    $form['path'] = array(
      '#type' => 'value',
      '#value' => ($pn->path) ? $pn->path : 'node/' . $pn->nid
    );
  }
  else {
    $form['path'] = array(
      '#type' => 'textfield',
      '#title' => t('URL Path'),
      '#required' => TRUE,
      '#description' => t('Enter the url path of the page. This can be an internal Drupal path such as node/add or an external URL such as http://drupal.org. Enter &lt;front&gt; to link to the front page.'),
    );   
  }
  $fieldset = linkintel_node_edit_fieldset(($node) ? $node : $pn->path);
  $form = array_merge($form, $fieldset['linkintel']);
  $form['autogen']['#collapsed'] = FALSE;
  $form['sync']['#collapsed'] = FALSE;
  
  
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  
  return $form;
}

/**
 * Validates the page keyword edit form
 * 
 * @param $form
 * @param $form_state
 */
function linkintel_page_settings_edit_form_validate($form, &$form_state) {
  $link = $form_state['values']['path'];  
  linkintel_parse_link_path($link, $msgs, 'form');
}

function linkintel_page_settings_edit_form_submit($form, &$form_state) {
//dsm($form_state);
  $settings = new stdClass();
  $settings->nid = $form_state['values']['nid'];
  $settings->path = $form_state['values']['path'];
  $settings->priority = $form_state['values']['linkintel_priority'];
  $settings->content_autogen = $form_state['values']['linkintel_content_autogen'];
  $settings->content_target_count = $form_state['values']['linkintel_content_target_count'];
  $settings->sidebar_autogen = $form_state['values']['linkintel_sidebar_autogen'];
  $settings->sidebar_target_count = $form_state['values']['linkintel_sidebar_target_count'];
  $settings->autogen_exclusivity = $form_state['values']['autogen_exclusivity'];
  $settings->request_sync_title = $form_state['values']['linkintel_request_sync_title'];
  $settings->request_sync_tax = $form_state['values']['linkintel_request_sync_tax'];    
  linkintel_save_page_settings($settings);
}