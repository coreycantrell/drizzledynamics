<?php 
$status =($row->{$field->field_alias});

if ( $status == "" ) {
	echo "&nbsp;";
};

if ( $status == "Outstanding" ) {
	echo "<span class='invoice_outstanding' title='This invoice has an outstanding balance'>&nbsp;</span>";
};

if ( $status == "Payment Received" ) {
	echo "<span class='invoice_paid' title='This invoice has been paid in full'>&nbsp;</span>";
};

?>