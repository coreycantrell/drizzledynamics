<?php
/**
 * @file
 * Include file for Event Bookings functionality 
 */

/**
 * Return all bookable resources
 *
 * @return array
 */
function eventbookings_resources() {
  return bookingsapi_resource_list(0, 'ORDER BY nid');
}

/**
 * Return the set of default values for the eventbookings resource table
 *
 * @param object $node
 * @param boolean $multiple_bookings
 * @return array $values
 */
function eventbookings_default_values($node, $multiple_bookings, $is_availability) {
  $values = array();
  if (isset($node->nid)) {
    $bookings = eventbookings_bookings_by_nid($node->nid);
    while ($booking = db_fetch_array($bookings)) {
      if (!$multiple_bookings) {
        $values['record_id'] = $booking['record_id'];
        $values['booked'] = $booking['resource_id'];
        $values['select'] = $booking['resource_id'];
        $values['status'] = ($is_availability) ? $booking['type'] : $booking['status'];
      } 
      else {
        $values[$booking['resource_id']]['record_id'] = $booking['record_id'];
        $values[$booking['resource_id']]['select'] = $booking['resource_id'];
        $values[$booking['resource_id']]['status'] = ($is_availability) ? $booking['type'] : $booking['status'];
      }
    }
  }
  return $values;
}

/**
 * Check for conflicts
 *
 * @param object $node
 * @param array $record_id
 * @param int $resource_id
 * @param int $status
 * @param date $start_date
 * @param date $end_date
 * @param array $rrule
 * @param array $conflict_rid_array
 * @param array $conflict_array
 * @return boolean
 */
function eventbookings_has_conflicts($node, $record_id, $resource_id, $status, $start_date, $end_date, $rrule, &$conflict_rid_array = array(), &$conflict_array = array()) {
  $success = TRUE;
  $is_availability = variable_get(EVENTBOOKINGS_IS_AVAILABILITY . '_' . $node->type, FALSE);
  if ($is_availability) {
    return $success;  // don't validate an availability
  }
  
  $record = array(   
    'record_id' => (int)$record_id,
    'resource_id' => (int)$resource_id,
    'start' => $start_date,
    'end' => $end_date,
    'type' => ($is_availability) ? $status : BOOKINGSAPI_BOOKING,
    'status' => ($is_availability) ? BOOKINGSAPI_STATUS_FINALIZED : $status,
    'rrule' => $rrule,
    'rrule_until' => ($rrule == NULL) ? '' : bookingsapi_rrule_until_as_dt($rrule),
  );
  $pending_conflict = variable_get(EVENTBOOKINGS_PENDING_AS_CONFLICT . '_' . $node->type, TRUE);
  $status = bookingsapi_conflict_check($record, $pending_conflict, $conflict_rid_array);
  
  // If the conflict is actually this node, then ignore the conflict
  if ($status != BOOKINGSAPI_CONFLICTS_NO) {
    $conflict_array = eventbookings_nids_from_rids($conflict_rid_array);
    $success = FALSE;
  }
  return $success;
}

/**
 * Return the nid associated with a booking
 *
 * @param array $records
 */
function eventbookings_nids_from_rids($records) {
  $nids = array();
  if (count($records) > 0) {
    $rid_set = array();
    foreach ($records as $record) {
      if (isset($record['record_id']) && $record['record_id'] != NULL) {
        $rid_set[] = $record['record_id'];
      }
    }
    if (count($rid_set) > 0) {
      $rids = implode(', ', $rid_set);
      $results = db_query(   
        'SELECT ' .
          'enr.nid ' .
        'FROM ' .
          '{eventbookings_node_record} enr '.
        'WHERE ' .
          'enr.record_id in (%s)', $rids);
      while ($nid = db_fetch_array($results)) {
        $nids[] = $nid['nid'];
      }
    }
  }
  return $nids;
}

/**
 * Book a resource for an event
 *
 * @param object $node
 * @param int $record_id
 * @param array $resource_id
 * @param int $status
 * @param date $start_date
 * @param date $end_date
 * @param array $rrule
 * @return NULL|boolean
 */
function eventbookings_book_resource($node, $record_id, $resource_id, $status, $start_date, $end_date, $rrule) {
  $success = FALSE;
  $result = NULL;
  $is_availability = variable_get(EVENTBOOKINGS_IS_AVAILABILITY . '_' . $node->type, FALSE);
  if ($record_id) {
    $record = bookingsapi_record_load($record_id);
    if (!$record) {
      return $result;
    }
    unset($record['updated']);
  } 
  else {
    $record = array(     
      'resource_id' => (int)$resource_id,
      'record_id' => NULL,
   );
  }

  $record['record_id'] = (int)$record_id;
  $record['type'] = ($is_availability) ? $status : BOOKINGSAPI_BOOKING;
  $record['status'] =  ($is_availability) ? BOOKINGSAPI_STATUS_FINALIZED : $status;
  $record['start'] = $start_date;
  $record['end'] = $end_date;
  $record['rrule'] = $rrule;
  $record['name'] = $node->title;
  $record['rrule_until'] = bookingsapi_rrule_until_as_dt($rrule);
  $record['no_loop'] = TRUE;

  $response = bookingsapi_record_save($record);
  $rc = $response[0];
  $reason = $response[1];
  
  // If this is a new booking and it was successfully created, add an eventbookings_node_record
  if ($rc) {
    if (($reason == SAVED_NEW || $is_availability) && $record_id == NULL && isset($record['created'])) {
      $eventbookings_node_record = array(     
        'nid' => $node->nid,
        'record_id' => $record['record_id']
     );
      $result = drupal_write_record('eventbookings_node_record', $eventbookings_node_record, array());
      if ($result == SAVED_NEW) {
        $success = TRUE;
      } 
      else {
        bookingsapi_record_delete($record); // rollback
      }
    } 
    // If this is an existing booking and it was successfully updated, set success
    elseif (($reason == SAVED_UPDATED || $is_availability) && $record_id != NULL && isset($record['modified'])) {
      $success = TRUE;
    }
  }
    
  // TODO : Bug in bookingsapi where changing the rrule on an existing record causes a conflict
  return $success;
}

/**
 * Delete a booking record and the mapping record between event and booking
 *
 * @param int $nid
 * @param int $record_id
 * @return array
 */
function eventbookings_delete_booking($nid, $record_id) {
  $result = db_query(   
    'DELETE FROM '.
      '{eventbookings_node_record} ' .
    'WHERE ' .
      'nid=%d and record_id=%d', $nid, $record_id);
  if ($result) {
    $record = array('record_id' => $record_id, 'no_loop' => TRUE);
    $booking = bookingsapi_record_delete($record);
  }
  return $result;
}

/**
 * Determines if this is a bookable Event node
 *
 * @param object $node
 * @return boolean
 */
function eventbookings_is_bookable($form, $node) {
  $is_bookable = FALSE;
  $date_field = variable_get(EVENTBOOKINGS_DATE_FIELD . '_' . $node->type, '_undefined_');
  if ($date_field != '_undefined_' && $date_field != EVENTBOOKINGS_EVENT_DISABLED && eventbookings_access_date_field($date_field, $node) && isset($form[$date_field])) {
    $is_bookable = TRUE;
  }
  return $is_bookable;
}

/**
 * Return whether the User has access to the date field
 *
 * @param object $node
 * @return boolean
 */
function eventbookings_access_date_field($date_field, $node) {
  $field = content_fields($date_field, $node->type);
  return content_access('update', $field, NULL, $node);
}

/**
 * Return all bookings for an Event Node
 *
 * @param int $nid
 * @return A
 */
function eventbookings_bookings_by_nid($nid) {
  $result = db_query(
    'SELECT ' .
      'br.record_id, br.resource_id, br.type, br.status ' .
    'FROM ' .
      '{eventbookings_node_record} enr INNER JOIN '.
      '{bookings_records} br ON br.record_id=enr.record_id '.
    'WHERE ' .
      'enr.nid=%d', $nid);
  return $result;
}

/**
 * Convert gmt date to local Timezone
 *
 * @param string $date
 * @return date
 */
function eventbookings_gmt_to_local($date) {
  return eventbookings_convert_timezone($date, 'UTC', date_default_timezone_name(FALSE));
}

/**
 * Convert local date to gmt Timezone
 *
 * @param string $date
 * @return date
 */
function eventbookings_local_to_gmt($date) {
  return eventbookings_convert_timezone($date, date_default_timezone_name(FALSE), 'UTC', 'Y-m-d\TH:i:s');
}

/**
 * Convert between timezones
 *
 * @param string $date
 * @param string $from
 * @param string $to
 * @return date
 */
function eventbookings_convert_timezone($date, $from, $to) {
  if (isset($date)) {
    if (!is_object($date)) {
      $date = date_make_date($date, $from, (is_numeric($date) ? DATE_UNIX : DATE_DATETIME));
    }
    date_timezone_set($date, timezone_open($to));
  }
  return $date;
}

/**
 * Convert to local timezone based off the offset value
 *
 * @param unknown_type $date
 * @param unknown_type $offset
 * @param unknown_type $format
 * @return string
 */
function eventbookings_normalize_timezone($date, $offset, $format = 'Y-m-d H:i:s') {
  $gmt_tz = new DateTimeZone(date_default_timezone_name());
  $gmt_date = new DateTime($date, $gmt_tz);
  return date($format, $gmt_date->format('U') + $offset);
}

/**
 * Theme the eventbookings element into a table
 *
 * @param array $element
 * @return string - rendered table
 */
function theme_eventbookings_resource_table($element) {
  $is_availability = $element['#is_availability'];
  $resources = $element['#resources'];
  $output = $element['heading']['#value'];
  $multiple_bookings = $element['#multiple_bookings'];
  
  // Theme hidden variables
  $output .= theme('hidden', $element['saved_selection']);
  if (!$multiple_bookings) {
    $output .= theme('hidden', $element['record_id']);
    $output .= theme('hidden', $element['booked']);
  } 
  else {
    foreach ($resources as $resource) {
      if (isset($element[$resource->nid]['record_id'])) {
        $output .= theme('hidden', $element[$resource->nid]['record_id']);
      }
    }
  }
  
  // If we have resources, process...
  if (count($resources) > 0) {
    // Fetch the resource calendar view
    $view = ($is_availability) ? views_get_view(EVENTBOOKINGS_AVAILABILITIES) : views_get_view(EVENTBOOKINGS_RESOURCE_SCHEDULE);
    $view_path = (isset($view)) ? $view->display['calendar_1']->display_options['path'] : 'resource_schedule';
    $view_path = $view_path . '/%d';

    // Table structure
    $rows = array();
    $headers = ($is_availability) ?
      array(
        t('Resource'),
        t('Availability'),
        t('Links'),
     ) :
      array(
        t('Resource'),
        t('Status'),
        t('Booking Conflicts'),
        t('Links'),
     );
     
    // Iterate thru the list of resources, only allow selection for
    // resources that are available
    // TODO : Make this table paganationable(is this a word?)
    foreach ($resources as $resource) {
      $booking = '';
      if ($element[$resource->nid]['#conflict']) {
        $conflict_nid_array = $element[$resource->nid]['#conflict_nids'];
        $conflict_rid_array = $element[$resource->nid]['#conflict_rids'];
        if (count($conflict_nid_array) > 0) {
          foreach ($conflict_nid_array as $conflict_nid) {
            $conflicting_event = node_load($conflict_nid);
            if ($conflicting_event != NULL && node_access('view', $conflicting_event)) {
              if ($booking != '') {
                $booking .= ', ';
              }
              $booking .= l($conflicting_event->title, 'node/'. $conflicting_event->nid, array('attributes' => array('target' => '_blank ', 'class' => 'eventbookings-conflict-event')));
            }
          }
          if ($booking == '') {
            $booking = t('Unavailable');
          }
        } 
        else {
          foreach ($conflict_rid_array as $conflict_rid) {
            $conflicting_event = bookingsapi_record_load($conflict_rid['record_id']);
            if ($conflicting_event != NULL) {
              if ($booking != "") {
                $booking .= ", ";
              }
             $booking .= '<span class="eventbookings-conflict-event">' . check_plain($conflicting_event['name']) . '</span>';
            }
          }
        }

        // If there were no specific conflicts, just write 'Unavailable'
        if ($booking == '') {
          $booking = '<span class="eventbookings-conflict-event">' . t('Unavailable') . '</span>';
        }
      } 
      else {
         $booking = '<div class="eventbookings-no-conflict">' . t('No Conflicts') . '</div>';
      }
      
      // Build the table row.
      $rows[] = ($is_availability) ?
      array(
        'data' => array(
          array('data' => theme($element[$resource->nid]['select']['#type'], $element[$resource->nid]['select']), 'class' => 'eventbookings-resource'),
          array('data' => theme($element[$resource->nid]['status']['#type'], $element[$resource->nid]['status']), 'class' => 'eventbookings-status-column'),
          array('data' => l('schedule', sprintf($view_path, $resource->nid), array('attributes' => array('target' => '_blank'))), 'class' => 'eventbookings-resource-schedule-link'),
        ),
      ) : 
       array(
        'data' => array(
          array('data' => theme($element[$resource->nid]['select']['#type'], $element[$resource->nid]['select']), 'class' => 'eventbookings-resource'),
          array('data' => theme($element[$resource->nid]['status']['#type'], $element[$resource->nid]['status']), 'class' => 'eventbookings-status-column'),
          array('data' => $booking, 'class' => 'eventbookings-conflict'),
          array('data' => l('schedule', sprintf($view_path, $resource->nid), array('attributes' => array('target' => '_blank'))), 'class' => 'eventbookings-resource-schedule-link'),
        ),
      );
    }
    $output .= theme('table', $headers, $rows, array('id' => 'eventbookings-resource-bookings'));
  }
  else {
    $output .= '<div class="messages warning">'. t('No bookable resources have been defined.') . '</div>';
  }
  return $output;
}

/**
 * Handle ajax callback when the User refreshes available resource
 *
 * This code was stolen almost entirely from http://drupal.org/node/331941
 *
 */
function _eventbookings_resource_availability_form_js() {
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form['#programmed'] = $form['#redirect'] = FALSE;
  
  $node = $form['#node'];
  $date_field = variable_get(EVENTBOOKINGS_DATE_FIELD . '_' . $node->type, '');
  
  // Add ahah flag
  $form_state['#is_ahah'] = $form_state['submitted'] = TRUE;
  
  // Unset an validation errors so that date field validates
  if (isset($form[$date_field][0])) {
    array_splice($form[$date_field][0]['#element_validate'],0,0,array('_eventbookings_reset_errors'));
  }
  else {
    array_splice($form[$date_field]['#element_validate'],0,0,array('_eventbookings_reset_errors'));
  } 
   
  // Process the form like it was submitted
  drupal_process_form($form_id, $form, $form_state);

  // Clear ahah and submitted flag - rebuild the form from the form_state...
  unset($form_state['submitted']);
  unset($form_state['#is_ahah']);
  
  // Rebuild....
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  
  // Extract out just the Booking form elements, render and return...
  unset($form['eventbookings']['#prefix']);
  unset($form['eventbookings']['#suffix']);
  
  $output = drupal_render($form['eventbookings']);
  drupal_json(array('status' => TRUE, 'data' => $output));

  // Reset errors
  form_set_error(NULL, '', TRUE);
  $_SESSION['messages'] = array();
  
  // Prevent the ahah callback from overwriting the form callback
  $form = form_get_cache($form_build_id, $form_state);
  unset($form['#action']);
  form_set_cache($form_build_id, $form, $form_state);
}

/**
 * Internal function to iterate over the posted form and perform a validation/update function
 *
 * @param array $form
 * @param array $form_state
 * @param string $user_func
 * @param array $records
 * @param boolean $fail_on_first
 * @return string
 */
function _eventbookings_post_handler($form, &$form_state, $user_func, &$records = array(), $fail_on_first = FALSE) {
  global $user;
  $node = $form['#node'];
  
  // Retrieve dates - bookingsapi always works in local timezone - so let's
  // convert from gmt
  $all_day = FALSE;
  $date_field = variable_get(EVENTBOOKINGS_DATE_FIELD . '_' . $node->type, '');
  $date_value = (isset($form_state['values'][$date_field][0]) ? $form_state['values'][$date_field][0] : $form_state['values'][$date_field]);
      
  // Parse date value
  $valid_range = _eventbookings_parse_date_value($node, $date_value, $start_date, $end_date, $rrule, $user_tz, $all_day);   
  
  if (!$valid_range) {
    form_set_error('eventbookings', t('The date range isn\'t valid'));
    return FALSE;
  }
    
  $bookings = array();
  $map_record_to_booking = array();

  if (isset($node->nid)) {
    $bookings = eventbookings_bookings_by_nid($node->nid);
    // build maps
    while ($booking = db_fetch_array($bookings)) {
      $record_id = $booking['record_id'];
      $nid = $booking['resource_id'];
      $map_record_to_booking[$nid] = $record_id;
      if (array_search($record_id, $records) == FALSE) {
        $records[] = $record_id;
      }
    }
  }

  // Loop thru requested resources and create/update booking
  $success = TRUE;
  $multiple_bookings = variable_get(EVENTBOOKINGS_MULTIPLE_BOOKINGS . '_' . $node->type, FALSE);
  $selections = ($multiple_bookings) ? $form_state['values']['eventbookings']['resources'] : (isset($form_state['values']['eventbookings']['resources']['select']) ? array($form_state['values']['eventbookings']['resources']['select'] => $form_state['values']['eventbookings']['resources']['select']) : array());
  foreach ($selections as $rid => $selection) {
    if (is_numeric($rid) && (!$multiple_bookings || $rid == $selection['select'])) {
      $status = ($multiple_bookings) ? $form_state['values']['eventbookings']['resources'][$rid]['status'] : $form_state['values']['eventbookings']['resources']['status'];
      $status = ($status == NULL) ? variable_get(EVENTBOOKINGS_DEFAULT_STATUS . '_' . $node->type, BOOKINGSAPI_STATUS_FINALIZED) : $status;
      if (isset($map_record_to_booking[$rid]) && $map_record_to_booking[$rid] > 0) {
        $record_id = $map_record_to_booking[$rid];
      } 
      else {
        $record_id = NULL;
      }
      if (!call_user_func($user_func, $node, $record_id, $rid, $status, $start_date, $end_date, $rrule)) {
        $success = FALSE;
        $resource = node_load($rid);
        if ($resource) {
          form_set_error('eventbookings-error', t('%name is unavailable for booking', array('%name' => $resource->title)));
        } 
        else {
          form_set_error('eventbookings-error', t('Sorry, unable to find the resource with id=%id', array('%id' => $rid)));
        }
        if ($fail_on_first) {
          break;
        }
      } 
      else {
        // All good, remove the record from existing bookings - this way we won't delete it
        $records = array_diff($records, array($record_id));
      }
    }
  }
    
  return $success;
}

/**
 * Build the id for checkbox
 *
 * @param int $index
 * @return string
 */
function _eventbookings_checkbox_id($node, $index) {
  return EVENTBOOKINGS_RESOURCE . '[' . $index . ']';
}

/**
 * Return the id of the status field
 *
 * @param unknown_type $node
 * @param unknown_type $index
 * @return string
 */
function _eventbookings_status_id($node, $index) {
  return EVENTBOOKINGS_RESOURCE . '_status[' . $index . ']';
}

/**
 * Fetch the status from the submitted data.  If the user doesn't have access
 * use default value
 *
 * @param unknown_type $multiple_bookings
 * @param unknown_type $index
 * @return int
 */
function _eventbookings_get_status($node, $multiple_bookings, $index) {
  return (user_access(PERM_CHANGE_BOOKING_STATUS)) ? 
          (($multiple_bookings) ? $_POST[EVENTBOOKINGS_RESOURCE . '_status'][$index] : $_POST[EVENTBOOKINGS_RESOURCE . '_status'])  :
          variable_get(EVENTBOOKINGS_DEFAULT_STATUS . '_' . $node->type, BOOKINGSAPI_STATUS_FINALIZED);
}

/**
 * Parse out start_date, end_date, and repeat rule.  The date will be converted
 * to the local(server) timezone - this is because publicbookings uses local - so our
 * dates will be compatible.  However, this is really a bug with publicbookings.  It
 * should be using gmt.  The reason is that if you store as local tz, then change your site tz,
 * the dates in the db are wrong.  If it is stored as gmt, it is immune to the timezone change.
 * 
 * If the user has a configured timezone, user_tz will be returned as true.
 *
 * @param array $date_value
 * @param string $start_date
 * @param string $end_date
 * @param array $rrule
 * @param boolean $user_tz
 */
function _eventbookings_parse_date_value($node, $date_value, &$start_date, &$end_date, &$rrule, &$user_tz, &$all_day) {
  global $user;
  
  $valid_range = FALSE;
  $start_date =  (isset($date_value['value'])) ? $date_value['value'] : NULL;
  $end_date = (isset($date_value['value2'])) ? $date_value['value2'] : NULL;
  $rrule = (isset($date_value['rrule'])) ? $date_value['rrule'] : NULL;
  
  // User configurable tz?
  $user_tz = variable_get('configurable_timezones', 1) && !empty($user->timezone_name);
  
  if (is_array($start_date)) {
    $start_date = implode(' ', $start_date);
  }
  
  if (is_array($end_date)) {
    $end_date = implode(' ', $end_date);
  }
  
  // Convert input into a DateTime object based off the sytem default timezone (bookingspi uses default timezone)
  if (isset($date_value['offset'])) {
    $start_date = eventbookings_convert_timezone($start_date, 'UTC', date_default_timezone_name(FALSE));
    $end_date = eventbookings_convert_timezone($end_date, 'UTC', date_default_timezone_name(FALSE));
  }
  else { 
    $start_date = date_make_date($start_date, NULL, (is_numeric($start_date) ? DATE_UNIX : DATE_DATETIME));
    $end_date = date_make_date($end_date, NULL, (is_numeric($end_date) ? DATE_UNIX : DATE_DATETIME));
    // Convert to the local timezone
    if ($user_tz) {
      $start_date = eventbookings_convert_timezone($start_date, $user->timezone, date_default_timezone_name(FALSE));
      $end_date = eventbookings_convert_timezone($end_date, $user->timezone, date_default_timezone_name(FALSE));
    }
  } 
  
  // Valid range?
  if ($start_date != NULL && $start_date != FALSE && $end_date != NULL && $end_date != FALSE && $end_date >= $start_date) {
    
    // Stringify rrule
    if (is_array($rrule)) {
      if (!empty($rrule['UNTIL']['datetime']) && is_array($rrule['UNTIL']['datetime'])) {
        $rrule['UNTIL']['datetime'] = (isset($rrule['UNTIL']['datetime']['date'])) ? $rrule['UNTIL']['datetime']['date'] : '';
      }
      if (isset($rrule['advanced'])) {
        $rrule = array_merge($rrule, $rrule['advanced']);
        $rrule['BYDAY'][''] = '';
        $rrule['BYMONTH'][''] = '';
        $rrule['BYMONTHDAY'][''] = '';
      }
      if (isset($rrule['exceptions'])) {
        foreach ($rrule['exceptions']['EXDATE'] as &$exception) {
          if (!empty($exception['datetime']) && is_array($exception['datetime'])) {
            $exception['datetime'] = (isset($exception['datetime']['date'])) ? $exception['datetime']['date'] : '';
          }
        }
        $rrule = array_merge($rrule, $rrule['exceptions']);
      }
      if (isset($rrule['additions'])) {
        foreach ($rrule['additions']['RDATE'] as &$addition) {
          if (!empty($addition['datetime']) && is_array($addition['datetime'])) {
            $addition['datetime'] = (isset($addition['datetime']['date'])) ? $addition['datetime']['date'] : '';
          }
        }
        $rrule = array_merge($rrule, $rrule['additions']);
      } 
      $rrule = ($rrule == NULL) ? '' : date_api_ical_build_rrule($rrule);
    }
    
    // all day support
    $start_time =  date_format_date($start_date, 'custom', 'H:i');
    $end_time =  date_format_date($end_date, 'custom', 'H:i');
    $all_day_enabled = variable_get(EVENTBOOKINGS_AD_ENABLE . '_' . $node->type, TRUE);
    $all_day = isset($date_value['all_day']) ? ($date_value['all_day'] == 1) : ($start_time == $end_time);
      
    if ($all_day && $all_day_enabled) {
      $valid_range = TRUE;
      $all_day_start = date_create(variable_get(EVENTBOOKINGS_AD_START . '_' . $node->type, '1/1/1970 08:00:00'));
      $all_day_end = date_create(variable_get(EVENTBOOKINGS_AD_END . '_' . $node->type, '1/1/1970 17:00:00'));
      $date = date_format_date($start_date, 'custom', 'Y-m-d');
      $start_date = $date . ' ' .date_format_date($all_day_start, 'custom', 'H:i');
      $end_date = $date . ' ' . date_format_date($all_day_end, 'custom', 'H:i');
    }
    else {
      $valid_range = TRUE;
      $start_date = date_format_date($start_date, 'custom', DATE_FORMAT_DATETIME);
      $end_date = date_format_date($end_date, 'custom', DATE_FORMAT_DATETIME);
    }
  }
  return $valid_range;
}

/**
 * Add eventbookings javascripts
 *
 * @param boolean $multiple_bookings
 */
function _eventbookings_add_javascript($multiple_bookings) {
  drupal_add_js(drupal_get_path('module', 'eventbookings') . '/js/bookings.js');
  drupal_add_js(array('eventbookings' => 
    array(
      'date_field' => 'eventbookings-datefield',
      'clear_button' => 'eventbookings-bookings-clear',
      'checkbox_type' => ($multiple_bookings) ? 'checkbox' : 'radio',
      'eventbookings' => 'eventbookings-wrapper',
      'saved_selection' => 'eventbookings-bookings-saved-selection',
      'status' => 'eventbookings-status'
     )
  ), 'setting');
}

/**
 * Set the name and id for an input form element
 *
 * @param array $input
 * @param array $parents
 * @param string $name
 */
function _eventbookings_set_input_id_name(&$input, $parents, $name) {
  $parents_for_id = array_merge($parents, array($name));
  $input['#id'] = form_clean_id('edit-'. implode('-', $parents_for_id));
  $input['#parents'] = $parents_for_id;
  $input['#name'] = array_shift($parents_for_id) . '['. implode('][', $parents_for_id) .']';
}

/**
 * Reset the errors during the validation phase to validate the date field
 *
 * @param array $element
 * @param array $form_state
 */
function _eventbookings_reset_errors($element, &$form_state) {
  form_set_error(NULL, '', TRUE);
  $_SESSION['messages'] = array();
}