<div class="profile">
	<div class="userWidget">
		<?php print $profile['user_picture']; ?>
		<div class="information">
			<div class="name">
				<?php print $account->profile_name; ?> <?php global $user; if ($user->uid): ?>(<?php print $account->name; ?>)<?php endif ;?>
				<?php global $user; if ($user->uid): ?>
				<div class="socialIcons">
					<?php if (!empty($account->profile_facebook)): ?><a href="http://facebook.com/<?php print $account->profile_facebook; ?>"><span aria-hidden="true" class="icon-facebook"></span></a><?php endif; ?>
					<?php if (!empty($account->profile_twitter)): ?><a href="http://twitter.com/<?php print $account->profile_twitter; ?>"><span aria-hidden="true" class="icon-twitter"></span></a><?php endif; ?>
					<?php if (!empty($account->profile_website)): ?><a href="<?php print $account->profile_website; ?>"><span aria-hidden="true" class="icon-web"></span></a><?php endif; ?>
				</div>
				<?php endif ;?>
			</div>
			<div class="personal">
				<?php if (!empty($account->profile_birth)): ?><span><?php $birthDate = $account->profile_birth; $birthDate = explode("/", $birthDate); $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y")-$birthDate[2])-1):(date("Y")-$birthDate[2])); echo "Age:".$age; ?></span><?php endif ;?>
				<?php if (!empty($account->profile_level)): ?><span>Level: <?php print $account->profile_level; ?></span><?php endif ;?>
				<?php if (!empty($account->profile_state)): ?><span>State: <?php print $account->profile_state; ?></span><?php endif ;?>
				<?php
				global $user;
				if (arg(0) == 'user' && $user->uid == arg(1) && arg(2) == '' ):
				?>
				<span>
					<?php 
					$smart_ip_session = smart_ip_session_get('smart_ip'); 
					$user_allowed = smart_ip_session_get('device_geolocation');
					if ($user_allowed != null ) {
						echo "Location: ";
					} else {
						echo "Approximate Location: ";
					}
					echo $smart_ip_session['location']['city'] . ', ';
					if (!empty($smart_ip_session['location']['region_code'])) { 
						echo $smart_ip_session['location']['region_code']; 
					} else { 
						echo $smart_ip_session['location']['administrative_area_level_1'];
					};
					?>
				</span>
				<?php endif ;?>
			</div>
			<div><?php print $account->profile_bio; ?></div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
