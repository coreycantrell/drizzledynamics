<?php
/**
 * Implementation of HOOK_theme().
 */
function equinemadeeasy_theme(&$existing, $type, $theme, $path) {
  $hooks = zen_theme($existing, $type, $theme, $path);
  return $hooks;  
}

/**
 * Custom search box.
 */
function equinemadeeasy_preprocess_search_theme_form(&$vars, $hook) {
 
  // Modify elements of the search form
  $vars['form']['search_theme_form']['#title'] = t('Search mysite.com');
  
  // Add a custom class to the search box
  $vars['form']['search_theme_form']['#attributes'] = array('class' => t('cleardefault'));
 
  // Change the text on the submit button
  $vars['form']['submit']['#value'] = t('GO');
 
  // Rebuild the rendered version (search form only, rest remains unchanged)
  unset($vars['form']['search_theme_form']['#printed']);
  $vars['search']['search_theme_form'] = drupal_render($vars['form']['search_theme_form']);
 
  // Rebuild the rendered version (submit button, rest remains unchanged)
  unset($vars['form']['submit']['#printed']);
  $vars['search']['submit'] = drupal_render($vars['form']['submit']);
 
  // Collect all form elements to make it easier to print the whole form.
  $vars['search_form'] = implode($vars['search']);
}

/**
 * Content for custom search box.
 */
function equinemadeeasy_box($title, $content, $region = 'main') {
  if ($title == 'Your search yielded no results')
  {
    $title = 'Your search did not match any documents';
    $content = '<ul id="search-recommends">';
    $content .= '<li>Check if your spelling is correct.</li>';
    $content .= '<li>Remove quotes around phrases to match each word individually: <em>"horse saddle"</em> will match less than <em>horse saddle</em>.</li>';
    $content .= '<li>Consider loosening your query with <em>OR</em>: <em>horse saddle</em> will match less than <em>horse OR saddle</em>.</li>';
    $content .= '</ul>';
  } elseif ($title == 'Search results')
  {
	global $pager_total_items;
	$result_count = $pager_total_items;
	$title = 'Search Results ('. $result_count[0] .')';
	
  }
  $output = '<h2 class="title">'. $title .'</h2><div>'. $content .'</div>';
  return $output;
}

/**
 * Hides Category label for Catalog.
 */
function equinemadeeasy_uc_catalog_browse($tid = 0) {
  drupal_add_css(drupal_get_path('module', 'uc_catalog') .'/uc_catalog.css');

  $output = '';
  $catalog = uc_catalog_get_page((int)$tid);
  drupal_set_title(check_plain($catalog->name));
  drupal_set_breadcrumb(uc_catalog_set_breadcrumb($catalog->tid));
  $types = uc_product_types();
  $links = array();
  $child_list = array();
  foreach ($catalog->children as $child) {
    if (!empty($child->nodes)) {
      $links[] = array('title' => $child->name . (variable_get('uc_catalog_breadcrumb_nodecount', FALSE) ? ' ('. $child->nodes .')' : ''), 'href' => uc_catalog_path($child),
        'attributes' => array('rel' => 'tag'),
      );
    }
    if (!empty($child->image)) {
      $image = '<div>';
      if (module_exists('imagecache')) {
        $image .= l(theme('imagecache', 'uc_category', $child->image['filepath']), uc_catalog_path($child), array('html' => TRUE));
      }
      else {
        $image .= l(theme('image', $child->image['filepath']), uc_catalog_path($child), array('html' => TRUE));
      }
      $image .= '</div>';
    }
    else {
      $image = '<div></div>';
    }
    $grandchildren = array();
    $j = 0;
    $max_gc_display = 3;
    foreach ($child->children as $i => $grandchild) {
      if ($j > $max_gc_display) {
        break;
      }
      $g_child_nodes = 0;
      foreach ($types as $type) {
        $g_child_nodes += taxonomy_term_count_nodes($grandchild->tid, $type);
      }
      if ($g_child_nodes) {
        $grandchildren[$i] = l($grandchild->name, uc_catalog_path($grandchild), array('class' => 'subcategory'));
        $j++;
      }
    }
    //$grandchildren = array_slice($grandchildren, 0, intval(count($grandchildren) / 2) + 1, TRUE);
    if ($j > $max_gc_display) {
      array_push($grandchildren, l(t('More...'), uc_catalog_path($child), array('class' => 'subcategory')));
    }
    if ($child->nodes) {
      $cell_link = $image .'<strong>'. l($child->name, uc_catalog_path($child)) .'</strong>';
      if (variable_get('uc_catalog_show_subcategories', TRUE)) {
        $cell_link .= "<br/><span>". implode(', ', $grandchildren) ."</span>\n";
      }
      $child_list[] = $cell_link;
    }
  }
  if (!empty($catalog->image)) {
    if (module_exists('imagecache')) {
      $output .= theme('imagecache', 'uc_thumbnail', $catalog->image['filepath'], $catalog->name, $catalog->name, array('class' => 'category'));
    }
    else {
      $output .= theme('image', $catalog->image['filepath'], $catalog->name, $catalog->name, array('class' => 'category'));
    }
  }

  // Build an ORDER BY clause for the SELECT query based on table sort info.
  if (empty($_REQUEST['order'])) {
    $order = 'ORDER BY p.ordering, n.title, n.nid';
  }
  else {
    $order = tapirsort_sql(uc_product_table_header());
  }

  $sql = "SELECT DISTINCT(n.nid), n.sticky, n.title, n.created, p.model, p.sell_price, p.ordering
    FROM {node} n
      INNER JOIN {term_node} tn ON n.vid = tn.vid
      INNER JOIN {uc_products} AS p ON n.vid = p.vid
    WHERE tn.tid = %d AND n.status = 1
      AND n.type IN (". db_placeholders($types, 'varchar') .") ". $order;

  $sql_count = "SELECT COUNT(DISTINCT(n.nid))
    FROM {node} n
      INNER JOIN {term_node} tn ON n.vid = tn.vid
      INNER JOIN {uc_products} AS p ON n.vid = p.vid
    WHERE tn.tid = %d
      AND n.status = 1
      AND n.type IN (". db_placeholders($types, 'varchar') .")";

  $sql = db_rewrite_sql($sql);
  $sql_count = db_rewrite_sql($sql_count);
  $sql_args = array($catalog->tid);
  foreach ($types as $type) {
    $sql_args[] = $type;
  }
  $catalog->products = array();
  $result = pager_query($sql, variable_get('uc_product_nodes_per_page', 12), 0, $sql_count, $sql_args);
  while ($node = db_fetch_object($result)) {
    $catalog->products[] = $node->nid;
  }
  if (count($catalog->products)) {
    if (count($links)) {
      $output .= theme('links', $links, array('class' => 'links inline uc-categories')) ."<br />\n";
    }
    $output .= theme('uc_catalog_products', $catalog->products);
    $output .= theme('pager');
  }
  else {
    // Display table of child categories similar to an osCommerce site's front page.
    $columns = variable_get('uc_catalog_category_columns', 3);
    $cat_rows = array();
    $row = array();
    $i = 1;
    foreach ($child_list as $cell) {
      $row[] = array('data' => $cell, 'class' => 'category');
      if ($i % $columns == 0) {
        $cat_rows[] = $row;
        $row = array();
      }
      $i++;
    }
    if (count($row) > 0 && count($row) < $columns) {
      if (count($cat_rows) >= 1) {
        $row = array_merge($row, array_fill(count($row), $columns - count($row), array('data' => '&nbsp;', 'class' => 'category')));
      }
      $cat_rows[] = $row;
    }
    $output .= theme('table', array(), $cat_rows, array('class' => 'category'));
  }

  return $output;
}

/**
 * Changes variable contents for products with $0.00 to Call For Price.
 */
function equinemadeeasy_uc_product_price($price, $context, $options = array()) {
	$priceformat = uc_currency_format($price);
	if ($priceformat == '$0.00') {
		$priceformat = 'Call For Price';
	}
	
	$output = '<div class="product-info price">';
	$output .= $priceformat;
	$output .= '</div>';

	return $output;
}

/**
 * Hides Add to Cart button for products with price of $0.00.
 */
function equinemadeeasy_uc_product_add_to_cart($node, $teaser = 0, $page = 0) {
  $output = '<div class="add_to_cart">';
  if (uc_currency_format($node->sell_price) == "$0.00") {
    $output .= t('&nbsp;');
  }elseif ($node->nid) {
    $output .= drupal_get_form('uc_product_add_to_cart_form_'. $node->nid, $node);
  }
  else {
    $output .= drupal_get_form('uc_product_add_to_cart_form', $node);
  }
  $output .= '</div>';
  return $output;
}

/**
 * Customize Catalog category grid view.
 */
function equinemadeeasy_uc_catalog_product_grid($products) {
  $product_table = '<div class="category-grid-products"><table>';
  $count = 0;
  foreach ($products as $nid) {
    $product = node_load($nid);

    if ($count == 0) {
      $product_table .= "<tr>";
    }
    elseif ($count % variable_get('uc_catalog_grid_display_width', 3) == 0) {
      $product_table .= "</tr><tr>";
    }

    $titlelink = l($product->title, "node/$nid", array('html' => TRUE));
	$price = ($product->sell_price);
    if (module_exists('imagecache') && 
          ($field = variable_get('uc_image_'. $product->type, '')) && 
          isset($product->$field) && 
          file_exists($product->{$field}[0]['filepath'])) {
      $imagelink = l(theme('imagecache', 'product_list', 
                             $product->{$field}[0]['filepath'], 
                             $product->title, $product->title), 
                             "node/$nid", array('html' => TRUE));
    }
    else {
      $imagelink = '';
    }

    $product_table .= '<td>';
    if (variable_get('uc_catalog_grid_display_title', TRUE)) {
      $product_table .= '<h2 class="catalog-grid-title">'. $titlelink .'</h2>';
    }
    if (variable_get('uc_catalog_grid_display_model', TRUE)) {
      $product_table .= '<span class="catalog-grid-ref">'. $product->model .'</span>';
    }
	$product_table .= '<div class="catalog-grid-picprice">';
    $product_table .= '<span class="catalog-grid-image">'. $imagelink .'</span>';
	$product_table .= '<br/>';
	if (variable_get('uc_catalog_grid_display_sell_price', TRUE)) {
		if (uc_currency_format($price) == '$0.00') {
		$product_table .= '<span class="catalog-grid-sell-price">Call For Price</span>';
		} else {
		$product_table .= '<span class="catalog-grid-sell-price">'. uc_currency_format($price) .'</span>';
		}
	}
	$product_table .= '</div>';
    $product_table .= '<span class="catalog-grid-desc">'. node_teaser($product->body) .'</span>';
   	
    if (variable_get('uc_catalog_grid_display_add_to_cart', TRUE)) {
      if (variable_get('uc_catalog_grid_display_attributes', TRUE)) {
        $product_table .= theme('uc_product_add_to_cart', $product);
      }
      else {
        $product_table .= drupal_get_form('uc_catalog_buy_it_now_form_'. 
                                     $product->nid, $product);
      }
    }
    $product_table .= '</td>';

    $count++;
  }
  $product_table .= "</tr></table></div>";
  return $product_table;
}
