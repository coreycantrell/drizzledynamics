﻿$(document).ready(function(){  
	
	// Adds sold status to div on horse sale
	var a = $(".view-content .views-row");
	$.each( a , function(i, l){
		if ($(this).children("div").find('.sold').length != 0) {
			$(this).addClass("status-is-sold");
		} else {}
	});	
	
	$('.views-field-field-news-related-horse-nid').each(function() {
		var seen = {};
		$(this).find('div.field-item').each(function() {
			var txt = $(this).find('a').text();
			if (seen[txt])
					$(this).remove();
			else
					seen[txt] = true;
		});
    });
	
	var b = $(".views-field-field-news-related-horse-nid .field-content div:not(:last-child)");
	$.each( b , function(){
		$(this).append(",&nbsp;");
	});
	
	$(".view-photo-manager tr").hover(function () {
		$(this).children("td:last-child").children().css('visibility','visible');
	}, function () {
		$(this).children("td:last-child").children().css('visibility','hidden');
	});
	
	$(".view-content-manager tr").hover(function () {
		$(this).find("td:last-child").children().css('visibility','visible');
	}, function () {
		$(this).find("td:last-child").children().css('visibility','hidden');
	});
	
	$(".horse-usef a").click(function() { 
		$( "#usef-gateway" ).dialog({
			modal: true,
			title: 'You&#39;re leaving DevinRyan.com',
			width: 520,
			closeText: ''
		});
	});
	
	$(".close-usef-gateway").click(function() { 
		$( "#usef-gateway" ).dialog("close");
	});

	$(".page-photo-gallery .content-inner img").lazyload({  
		placeholder: "/sites/devinryan.com/themes/devinryan/images/placeholder.png",	
		effect: "fadeIn",
		threshold : 50
	});	
	
	$(".page-staff-photo-manager .content-inner img").lazyload({  
		placeholder: "/sites/devinryan.com/themes/devinryan/images/placeholder.png",	
		effect: "fadeIn",
		threshold : 50
	});

	$('.view-id-services_slideshow .view-content').after('<div class="slide-nav">').cycle({ 
        fx:     'fade',
        speed:   700, 
        timeout: 3000, 
        next:   '#s3', 
        pause:   1,
		pager:  '.slide-nav'
    });

	$('.view-id-facilities_slideshow .view-content').after('<div class="slide-nav">').cycle({ 
        fx:     'fade',
        speed:   700, 
        timeout: 3000, 
        next:   '#s3', 
        pause:   1,
		pager:  '.slide-nav'
    });
	
	$("#services-accordion").accordion({
		active: false, 
		collapsible: true,
		autoHeight: false,
		fillSpace: false
	});

	$('.priceCats').bt($(".priceCatsList").html(),{
	  showTip: function(box){
		$(box).fadeIn(300);
	  },
	  hideTip: function(box, callback){
		$(box).animate({opacity: 0}, 300, callback);
	  },
	  fill:             "#FFFFFF",
	  strokeStyle:      "#4C7CAF",
	  width:            '200px',
	  shadowOverlap:   true,
	  shadow: true,
      shadowColor: 'rgba(0,0,0,.5)',
      shadowBlur: 8,
      shadowOffsetX: 4,
      shadowOffsetY: 4,
	  cssStyles:        {
		color: '#4C7CAF'
	  },
	  hoverIntentOpts: {
		interval: 0,
		timeout: 0
	  }
	});
	
    $('img').bt({
	  showTip: function(box){
		$(box).fadeIn(300);
	  },
	  hideTip: function(box, callback){
		$(box).animate({opacity: 0}, 300, callback);
	  },
	  fill:             "#FFFFFF",
	  strokeStyle:      "#232C1A",
	  width:            '210px',
	  shadowOverlap:    true,
	  killTitle:        true,
	  shadow: true,
      shadowColor: 'rgba(0,0,0,.5)',
      shadowBlur: 8,
      shadowOffsetX: 4,
      shadowOffsetY: 4,
	  cssStyles:        {
		color: '#232C1A',
		fontWeight: 'bold'
	  },
	  hoverIntentOpts: {
		interval: 0,
		timeout: 0
	  }
	});
	
	$('a.thickbox').removeAttr('title');
	
	$('#define-dressage').bt({
	  showTip: function(box){
		$(box).fadeIn(300);
	  },
	  hideTip: function(box, callback){
		$(box).animate({opacity: 0}, 300, callback);
	  },
	  fill:             "#FFFFFF",
	  strokeStyle:      "#232C1A",
	  width:            '300px',
	  shadowOverlap:   true,
	  shadow: true,
      shadowColor: 'rgba(0,0,0,.5)',
      shadowBlur: 8,
      shadowOffsetX: 4,
      shadowOffsetY: 4,
	  cssStyles:        {
		color: '#232C1A',
		fontWeight: 'bold'
	  },
	  hoverIntentOpts: {
		interval: 0,
		timeout: 0
	  }
	});
	
	$('.section-schedule #content-area').prepend('<a href="javascript:void(0)" id="printPage"><img src="/sites/devinryan.com/themes/devinryan/images/printer.png" alt="print"/> Print</a>');

	$('#printPage').click(function() {
		window.print();
		return false;
	});	
	
	$('.horse-print').click(function() {
		window.print();
		return false;
	});
	
});

