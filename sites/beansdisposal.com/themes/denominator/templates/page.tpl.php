<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
	<title><?php print $head_title; ?></title>
	<?php print $head; ?>
	<?php print $styles; ?>
	<link href='http://fonts.googleapis.com/css?family=Vollkorn|Asap' rel='stylesheet' type='text/css'>
	<link href="https://s3.amazonaws.com/scripts.drizzledynamics.com/formalize/formalize.css" rel="stylesheet" type="text/css">
	<?php print $scripts; ?>
    <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/dzdy.required.js"></script>
    <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/formalize/jquery.formalize.js"></script>
	   <script src="//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
        <script>
            WebFont.load({
                google: {
                    families: ['Lato', 'Arvo']
                }
            });
        </script>
  </head>
<body class="<?php print $classes; ?>">
	<div id="header-wrapper">
		<div id="header">
  		<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"></a>
  		<?php print $header; ?>
		</div><!-- /.section, /#header -->
	</div>
  <?php if ($primary_links): ?>
    <?php print $navigation; ?>
  <?php endif; ?>
	<?php if ($pre_content): ?>
	<div id="pre-content-wrapper">
		<div id="pre-content"><div class="section clearfix">
			<?php print $pre_content; ?>
		</div></div><!-- /.section, /#pre-content" -->
	</div>
	<?php endif; ?>
	
	<div id="page-wrapper"><div id="page">

    <div id="main-wrapper"><div id="main" class="clearfix">

	
      <div id="content" class="column"><div class="section">

        <?php print $highlight; ?>

        <?php print $breadcrumb; ?>
        <?php if ($title): ?>
          <h1 class="title"><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print $messages; ?>
        <?php if ($tabs): ?>
          <div class="tabs"><?php print $tabs; ?></div>
        <?php endif; ?>
        <?php print $help; ?>

        <?php print $content_top; ?>

        <div id="content-area">
          <?php print $content; ?>
        </div>

        <?php print $content_bottom; ?>

        <?php if ($feed_icons): ?>
          <div class="feed-icons"><?php print $feed_icons; ?></div>
        <?php endif; ?>

      </div></div><!-- /.section, /#content -->

      <?php print $sidebar_first; ?>

      <?php print $sidebar_second; ?>

    </div></div><!-- /#main, /#main-wrapper -->

    <?php if ($footer || $footer_message || $secondary_links): ?>
      <div id="footer"><div class="section">

        <?php print theme(array('links__system_secondary_menu', 'links'), $secondary_links,
          array(
            'id' => 'secondary-menu',
            'class' => 'links clearfix',
          ),
          array(
            'text' => t('Secondary menu'),
            'level' => 'h2',
            'class' => 'element-invisible',
          ));
        ?>

        <?php if ($footer_message): ?>
          <div id="footer-message"><?php print $footer_message; ?></div>
        <?php endif; ?>
		
        <?php print $footer; ?>

      </div></div><!-- /.section, /#footer -->
    <?php endif; ?>

  </div></div><!-- /#page, /#page-wrapper -->

  <?php print $page_closure; ?>

  <?php print $closure; ?>
  
  <div id="dzdy_badge" style="text-align:center"></div>
	
</body>
</html>
