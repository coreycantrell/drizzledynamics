<?php
// $Id: maintenance-page.tpl.php,v 1.17 2009/11/04 20:49:23 johnalbin Exp $

/**
 * @file maintenance-page.tpl.php
 *
 * Theme implementation to display a single Drupal page while off-line.
 *
 * All the available variables are mirrored in page.tpl.php. Some may be left
 * blank but they are provided for consistency.
 *
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>The Equestrian Fox</title>
	<link rel="shortcut icon" href="/sites/theequestrianfox.com/themes/theequestrianfox/favicon.ico" type="image/x-icon" />
	<link type="text/css" rel="stylesheet" href="/sites/theequestrianfox.com/themes/theequestrianfox/css/maintenance-style.css" />
</head>
<body>
	<div id="container">
		<div id="logo-container">
			<img alt="Coming Soon" width="426" height="250" src="/sites/theequestrianfox.com/themes/theequestrianfox/comingsoon.png"/>
		</div>
		<div id="social-container">
			<div id="social-twitter">
				<a href="http://twitter.com/equinefox" title="Twitter (equinefox)"><span id="twitterLogo" class="social-icon"></span></a>
			</div>
			<div id="social-fb">
				<a href="http://www.facebook.com/pages/The-Equestrian-Fox/142161142490580" title="Twitter (equinefox)"><span id="fbLogo" class="social-icon"></span></a>
			</div>
			<div id="social-email">
				<a href="mailto:daun@theequestrianfox.com"><span id="emailIcon" class="social-icon"></span></a>
			</div>
		</div>
	</div>
</body>
</html>
