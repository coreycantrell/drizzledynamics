<?php global $user; ?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix">
<?php if ($teaser): ?>
	<?php if (!$page && $title): ?>
		<h2 class="title"><a href="<?php print $node_url; ?>"><?php print $title; ?><?php if ($unpublished): ?> <?php print t('(Unpublished)'); ?></div><?php endif; ?></a></h2>
	<?php endif; ?>

	<div class="content">
		<?php print $node->field_resource_photo[0]['view'] ?>
		<?php print $node->content['body']['#value']; ?>
	</div>
	
	<div class="clearfix"></div>
	
	<?php /*
	<div class="comment-links">
		<div class="blog-comment">
			<?php if ($node->comment): ?><span aria-hidden="true" class="icon-comments" title="This resource has <?php print $node->comment_count ?> comments."/></span> <?php print $node->comment_count; ?><?php endif; ?>
			<?php 
			$vid = 2;
			$terms = taxonomy_node_get_terms_by_vocabulary($node, $vid);
			if ($terms) {
				foreach ($terms as $term) {
					if ($term->name == "Guide Tips") {
						print '<span aria-hidden="true" class="icon-tips"/></span>&nbsp; ';
					} elseif ($term->name == "Hiking") {
						print '<span aria-hidden="true" class="icon-hiking"/></span>&nbsp; ';
					} elseif ($term->name == "Safety") {
						print '<span aria-hidden="true" class="icon-safety"/></span>&nbsp; ';
					} elseif ($term->name == "Survival") {
						print '<span aria-hidden="true" class="icon-survival"/></span>&nbsp; ';
					} 
					else {
						print '<span aria-hidden="true" class="icon-resource"/></span>&nbsp; ';
					}
					print $term->name;
				}
			}
			?>
		</div>
			
		<?php if ($node->field_resource_has_source[0]['safe'] == 1): ?>
		<div class="blog-comment source">
			<span aria-hidden="true" class="icon-source"/></span> Includes a source.
		</div>
		<?php endif; ?>
		<?php if ($node->readmore ): ?>
		<div class="blog-read-more">
			<a href="<?php print $node->path; ?>">Read More</a>
		</div>
		<?php endif; ?>
		<div class="clearfix"></div>
	</div>
	*/ ?>
	
<?php else: ?>

	<?php if ($unpublished): ?>
		<div class="unpublished"><?php print t('Unpublished'); ?></div>
	<?php endif; ?>
	
	<div class="content">
		<?php print $node->field_resource_photo[0]['view'] ?>
		<?php print $node->content['body']['#value']; ?>
	</div>
	
	<div class="clearfix"></div>
	
	<?php if ($node->field_resource_has_source[0]['safe'] == 1): ?>
		<div class="sourceContainer">
			<div class="header"><img src="/sites/trailstack.com/themes/denominator/images/source.png" height="16" width="16"/> Included Source</div>
			<?php print $node->field_source_name[0]['value']; ?><br/>
			<a href="<?php print $node->field_source_url[0]['url']; ?>"><?php print $node->field_source_url[0]['url']; ?></a>
		</div>
		<div class="sourceContainer">
			Category: 
			<?php 
				$node = node_load(arg(1));
				$vid = 2;
				$terms = taxonomy_node_get_terms_by_vocabulary($node, $vid);
				if ($terms) {
					foreach ($terms as $term) {
						print l($term->name, 'taxonomy/term/'.$term->tid);
					}
				}
			?>
			<br/>
			Tags:
			<?php 
				$node = node_load(arg(1));
				$vid = 4;
				$terms = taxonomy_node_get_terms_by_vocabulary($node, $vid);
				if ($terms) {
					foreach ($terms as $term) {
						print l($term->name, 'taxonomy/term/'.$term->tid) .' &nbsp;';
					}
				}
			?>
		</div>
	<?php endif; ?>
<?php endif; ?>
</div>
