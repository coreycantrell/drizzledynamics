<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
	<title><?php print $head_title; ?></title>
	<?php print $head; ?>
	<?php print $styles; ?>
	<?php print $scripts; ?>
	<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/jquery.bt.js"></script>
	<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/dzdy.required.js"></script>
</head>
<body class="<?php print $classes; ?>">
	<div id="attic-tile"></div>
	<div id="page-header-wrapper">
		<div id="page-header">
		<a id="contact-btn" title="Use our online form to request more information about one of our products." href="/request-more-information">Request More Information</a>
		<div id="header"><div class="section clearfix">
			<?php if ($logo): ?>
			<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
			<?php endif; ?>
			<div id="header-list">
				<strong>Full Service Product Representation Firm</strong><br/>
				<span class="green-check"></span> Authorized Sales and Distributor<br/>
				<span class="green-check"></span> State Grant Writing and Application<br/>
				<span class="green-check"></span> System Consulting and Design<br/>
			</div>
			<div id="header-contact">
				<strong>Contact Us Today!</strong><br/>
				<span class="phone-icon"></span> (845) 248-6357<br/>
				<span class="mail-icon"></span> <a href="mailto:info@greenmarineco.com">info@greenmarineco.com</a>
			</div>
			<?php print $header; ?>
		</div></div><!-- /.section, /#header -->
		</div>
	</div>
	<div id="page-wrapper"><div id="page">
		<?php if ($adminUI): ?>
			<div id="adminUI-wrapper">
				<?php print $adminUI; ?>
			</div>
		<?php endif; ?>
		<div id="main-wrapper"><div id="main" class="clearfix">
			<a href="<?php print $front_page; ?>" class="breadcrumb" title="Go back Home">&#60; Home</a>
			<div id="content" class="column">
			<div class="section">
				<?php print $highlight; ?>
					<?php print $messages; ?>
					<?php if ($tabs): ?>
					<div class="tabs"><?php print $tabs; ?></div>
					<?php endif; ?>
					<?php if ($title): ?>
					<h1 class="title"><?php print $title; ?></h1>
					<?php endif; ?>
					<?php print $help; ?>
					<?php print $content_top; ?>
					<div id="content-area">
						<?php print $content; ?>
					</div>
				<?php print $content_bottom; ?>
			</div></div><!-- /.section, /#content -->
		</div></div><!-- /#main, /#main-wrapper -->
		<?php if ($content_extend): ?>
		<div id="edc-br"></div>
		<div id="content-extend-wrapper">
			<?php print $content_extend; ?>
		</div>
		<?php endif; ?>
		<div id="footer"><div class="section">
			<?php if ($footer_message): ?>
			<div id="footer-message"><?php print $footer_message; ?></div>
			<?php endif; ?>
			<?php print $footer; ?>
		</div></div><!-- /.section, /#footer -->
	</div></div><!-- /#page, /#page-wrapper -->
	<?php print $page_closure; ?>
	<?php print $closure; ?>
	<div id="dzdy_badge" style="margin-bottom:10px;text-align:center;position:relative;z-index:9"></div>
	<div id="basement-tile"></div>
</body>
</html>
