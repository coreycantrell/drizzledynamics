<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix">
    
    <div class="web-right">
        <div class="web-title">
            <?php print check_plain($node->title) ?>
        </div>
        
        <div class="field field-type-text field-field-web-status">
          <div class="field-items">
              <div class="field-item">
                  <?php
                  $d=($node->field_web_status[0]['view']);
                  if ($d=="Inactive")
                    print "<span class='inactive'><img src='/sites/default/themes/dzdy/images/inactive.png' class='inactive' title='Site offline' alt='inactive'/> &nbsp; " . $node->field_web_url[0]['url'] . "</span>";
                  elseif ($d=="Active")
                    print "<span class='active'><img src='/sites/default/themes/dzdy/images/active.png' class='active' title='Site online' alt='active'/> &nbsp; <a href='" . $node->field_web_url[0]['url'] . "'>". $node->field_web_url[0]['url'] . "</a></span>";
                  elseif ($d=="Development")
                    print "<span class='dev'><img src='/sites/default/themes/dzdy/images/dev.png' class='dev' title='In Development' alt='dev'/> &nbsp; ". $node->field_web_url[0]['url'] . "</span>";
                  else
                    echo "&nbsp;";
                  ?>
              </div>
          </div>
        </div>

        <?php
        if (isset($node->field_web_twitter[0]['value'])) {
            print '<div class="field field-type-text field-field-web-twitter"><div class="field-items"><div class="field-item"> <img width="27" height="16" src="/sites/default/themes/dzdy/images/twitter-bird.png">&nbsp;@' . $node->field_web_twitter[0]['view'] . '</div></div></div>';
        }
        ?>
        
        <div class="clearfix"></div>
        
        <div class="field field-type-text field-field-web-body">
          <div class="field-items">
              <div class="field-item"><?php print $node->content['body']['#value'] ?></div>
          </div>
        </div>

        <div class="field field-type-text field-field-web-part-played">
          <div class="field-label">Part Played</div>
          <div class="field-items">
              <div class="field-item"><?php print $node->field_web_part_played[0]['view'] ?></div>
          </div>
        </div>
    </div>

    <div class="web-left">
        <?php print theme('imagecache', 'Web-Image', $node->field_web_image[0]['filepath'], $alt = '', ''); ?>
    </div>

  <?php print $links; ?>
</div>
