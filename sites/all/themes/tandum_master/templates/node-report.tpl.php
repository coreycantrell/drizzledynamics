<div class="file-right">
    <iframe src="https://docs.google.com/viewer?url=https://tandum.drizzledynamics.com/sites/tandum.drizzledynamics.com/files/file-cabinet/<?php print$node->field_file_file[0]['filename']?>&embedded=true" width="100%" height="780" style="border:none;"></iframe>
</div>
<div class="file-left">
    <div class="field-item">
        <div class="field-label">Client</div> <div class="field-value"><img src="/sites/all/themes/tandum_master/images/user.png"> <?php print $node->field_file_client[0]['view'] ?></div>
    </div>
    <div class="field-item">
        <div class="field-label">Created</div> <div class="field-value"><img src="/sites/all/themes/tandum_master/images/calendar_add.png"> <?php echo format_date($node->created, 'custom', "m/d/y - g:ia") ?></div>
    </div>
    <div class="field-item">
        <div class="field-label">File</div> <div class="field-value"><?php print $node->field_file_file[0]['view'] ?></div>
    </div>
    <div class="field-item">
        <div class="field-label">Trackable URL</div> 
        <div class="field-value">
            <input type="text" value="https://tandum.drizzledynamics.com/sites/tandum.drizzledynamics.com/modules/pubdlcnt/report.php?file=https://tandum.drizzledynamics.com/sites/tandum.drizzledynamics.com/files/file-cabinet/<?php print$node->field_file_file[0]['filename']?>&nid=<?php print $node->nid ?>"/>
        </div>
    </div>
    <div class="field-item">
        <?php foreach ((array)$node->field_file_tag as $item) { ?>
        <div class="field-item-tag"><img src="/sites/all/themes/tandum_master/images/tag_blue.png"> <?php print $item['view'] ?></div>
        <?php } ?>
    </div>
    <div class="field-item">
        <div class="field-label">Email Report</div> 
        <div class="field-value">
            <button onClick="window.location.href='/pet/traffic_report&nid=<?php print $node->nid ?>'">Send</button>
        </div>
    </div>

</div>