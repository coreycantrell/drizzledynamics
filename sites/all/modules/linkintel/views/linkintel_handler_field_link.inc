<?php

class linkintel_handler_field_link extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = 'nid';
    $this->additional_fields['lid'] = 'lid';
    $this->additional_fields['path'] = 'path';
    $this->additional_fields['options'] = 'options';
    $this->additional_fields['external'] = 'external';
    $this->additional_fields['uid'] = 'uid';
    $this->additional_fields['autogen'] = 'autogen';
    $this->additional_fields['mode'] = 'mode';
    $this->additional_fields['rid'] = 'rid';
    $this->additional_fields['kid'] = 'kid';
    $this->additional_fields['text'] = 'text';
    $this->additional_fields['nofollow'] = 'nofollow';
    $this->additional_fields['from_nid'] = 'from_nid';
    $this->additional_fields['from_path'] = 'from_path';
    $this->additional_fields['from_options'] = 'from_options';
    $this->additional_fields['from_external'] = 'from_external';
    $this->additional_fields['attributes'] = 'attributes';
  }  
  
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
//dsm($values);
    $link = new stdClass();
    foreach($values AS $k => $v) {
      if(substr($k, 0, 15) == 'linkintel_link_') {
        $ks = substr($k, 15);
        $link->$ks = $v;
      }
    }
    $link->attributes = unserialize($link->attributes);

    if(!$link->text && $values->node_title) {
      $link->text = $values->node_title;
    }
//dsm($link);
    $output = linkintel_l($link);
    return $output;    
  }
}