<div class="client-page-left">
<div class="field field-type-text field-field-client-tag">
    <div class="field-items">
        <div class="field-item"><?php print $node->field_client_tag[0]['view'] ?></div>
    </div>
</div>

<div class="field field-type-email field-field-client-email1">
    <h3 class="field-label">Primary Email</h3>
    <div class="field-items">
        <div class="field-item"><img src="/sites/all/themes/tandum_master/images/email.png"/> <?php print $node->field_client_email1[0]['view'] ?></div>
    </div>
</div>

<?php if (isset($node->field_client_email2[0]['email'])) { print '
    <div class="field field-type-email field-field-client-email2"> 
        <h3 class="field-label">Secondary Email</h3> 
        <div class="field-items">
            <div class="field-item"><img src="/sites/all/themes/tandum_master/images/email.png"/>&nbsp;' . $node->field_client_email2[0]["view"] . '</div>
        </div>
    </div>
';} ?>

<div class="field field-type-text field-field-client-server" <?php if (empty($node->field_client_server[0]['view'])) { print 'style="display:none"'; } ?>>
    <h3 class="field-label">Web Server</h3>
    <div class="field-items">
        <div class="field-item">
            <?php 
            $server_name = $node->field_client_server[0]['view'];
            if ( $server_name == "VPS" ) {
            	print '<img src="/sites/all/themes/tandum_master/images/vps-server.png"/> VPS';
            } elseif ( $server_name == "Achird" ) {
                    print '<img src="/sites/all/themes/tandum_master/images/achird-server.png"/> Achird';
            };
            ?>
        </div>
    </div>
</div>

<div class="field field-type-text field-field-client-mysql" <?php if (empty($node->field_client_mysql[0]['view'])) { print 'style="display:none"'; } ?>>
    <h3 class="field-label">MySQL Server</h3>
    <div class="field-items">
        <div class="field-item">
            <?php 
            $server_name2 = $node->field_client_mysql[0]['view'];
            if ( $server_name2 == "VPSOLD" ) {
            	print '<img src="/sites/all/themes/tandum_master/images/vps-server.png"/> VPS';
            } elseif ( $server_name2 == "AchirdOLD" ) {
                print '<img src="/sites/all/themes/tandum_master/images/achird-server.png"/> Achird';
            } else {
                print '<img src="/sites/all/themes/tandum_master/images/vps-server.png"/> VPS';
            };
            ?>
        </div>
    </div>
</div>

<div class="field field-type-text field-field-client-backups" <?php if (empty($node->field_client_backups[0]['view'])) { print 'style="display:none"'; } ?>>
    <h3 class="field-label">Backups</h3>
    <div class="field-items">
        <div class="field-item">
            <?php
            $active = $node->field_client_backups[0]['view'];
            if ( $active == "Active" ) {
            	print '<img src="/sites/all/themes/tandum_master/images/active.png"/> Active';
            } elseif ( $active == "Inactive" ) {
                    print '<img src="/sites/all/themes/tandum_master/images/inactive.png"/> Inactive';
            };
            ?>
        </div>
    </div>
</div>

<div class="field field-type-date field-field-client-bill" <?php if (empty($node->field_client_sub_date[0]['view'])) { print 'style="display:none"'; } ?>>
    <h3 class="field-label">Subscription</h3>
    <div class="field-items">
      <div class="field-item"><img src="/sites/all/themes/tandum_master/images/calendar.png"/> <?php print $node->field_client_sub_date[0]['view'] ?>, $<?php print $node->field_client_sub_price[0]['value'] ?></div>
    </div>
</div>

<div <?php if (empty($node->field_client_username[0]['view'])) { print 'style="display:none"'; } ?>>
<div class="header-section field-label">Admin Credentials</div>

<div class="field field-type-email field-field-client-username">
    <h3 class="field-label">Username</h3>
    <div class="field-items">
        <div class="field-item"><?php print $node->field_client_username[0]['view'] ?></div>
    </div>
</div>

<div class="field field-type-email field-field-client-password">
    <h3 class="field-label">Password</h3>
    <div class="field-items">
        <div class="field-item"><?php print $node->field_client_password[0]['view'] ?></div>
    </div>
</div>
</div>

<div <?php if (empty($node->field_client_email_username[0]['view'])) { print 'style="display:none"'; } ?>>
<div class="header-section field-label">Email Credentials</div>

<div class="field field-type-email field-field-client-username">
    <h3 class="field-label">Username</h3>
    <div class="field-items">
        <div class="field-item"><?php print $node->field_client_email_username[0]['view'] ?></div>
    </div>
</div>

<div class="field field-type-email field-field-client-password" <?php if (empty($node->field_client_email_password[0]['view'])) { print 'style="display:none"'; } ?>>
    <h3 class="field-label">Password</h3>
    <div class="field-items">
        <div class="field-item"><?php print $node->field_client_email_password[0]['view'] ?></div>
    </div>
</div>
</div>

<div <?php if (empty($node->field_client_server[0]['view'])) { print 'style="display:none"'; } ?>>
<div class="header-section field-label" style="margin-bottom:15px">Project Links</div>

    <div class="field field-type-link field-field-client-url">
      <div class="field-items">
          <div class="field-item"><img src="/sites/all/themes/tandum_master/images/world_link.png"/> <a href="<?php print $node->field_client_url[0]['display_url'] ?>">Visit Website</a></div>
      </div>
    </div>

    <div class="field field-type-link field-field-client-log">
      <div class="field-items">
          <div class="field-item"><img src="/sites/all/themes/tandum_master/images/report.png"/> <a href="<?php print $node->field_client_log[0]['display_url'] ?>">View Log</a></div>
      </div>
    </div>

    <div class="field field-type-link field-field-client-repo">
      <div class="field-items">
          <div class="field-item"><img src="/sites/all/themes/tandum_master/images/repo.png"/> <a href="<?php print $node->field_client_repo[0]['display_url'] ?>">View Repository</a></div>
      </div>
    </div>
</div>

<br style="clear:both"/>

<div style="display:none;">
<div style="display:none;" <?php if (empty($node->field_client_email_template[0]['filepath'])) { print 'style="display:none"'; } ?>>
    <div class="header-section field-label" style="margin-bottom:15px">Email Template</div>
    <div class="email-templates field-items">
		<?php if (isset($node->field_client_email_template[0]['filepath'])) { print '
            <div class="field-item">
                <img src="/sites/all/modules/filefield/icons/text-html.png" alt="text/html icon" class="filefield-icon field-icon-text-html"> <a href="/' . $node->field_client_email_template[0]["filepath"] . '" target="_block">' . $node->field_client_email_template[0]["data"]["description"] . '</a>
            </div>
        '; } ?>
        <?php if (isset($node->field_client_email_template[1]['filepath'])) { print '
            <div class="field-item">
                <img src="/sites/all/modules/filefield/icons/text-html.png" alt="text/html icon" class="filefield-icon field-icon-text-html"> <a href="/' . $node->field_client_email_template[1]["filepath"] . '" target="_block">' . $node->field_client_email_template[1]["data"]["description"] . '</a>
            </div>
        '; } ?>
    </div>
</div>
</div>
</div>

<div class="client-page-right">
<div <?php if (empty($node->field_backref_6c5bba149ade0c7d99[0]['view'])) { print 'style="display:none"'; } ?>>
	<div class="header-section field-label">Invoices</div>
	<?php print $node->field_backref_6c5bba149ade0c7d99[0]['view'] ?>
	<br/>
</div>
<div <?php if (empty($node->field_backref_da2def5c34f48b3928[0]['view'])) { print 'style="display:none"'; } ?>>
	<div class="header-section field-label">Reports</div>
	<?php print $node->field_backref_da2def5c34f48b3928[0]['view'] ?>
</div>
<div <?php if (empty($node->field_backref_e316372b85708bd20b[0]['view'])) { print 'style="display:none"'; } ?>>
	<div class="header-section field-label">Tickets</div>
	<?php print $node->field_backref_e316372b85708bd20b[0]['view'] ?>
</div>
</div>
