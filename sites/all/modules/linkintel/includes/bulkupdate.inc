<?php
// $Id: bulkupdate.inc,v 1.3.2.9 2011/01/05 20:50:30 tomdude48 Exp $
function linkintel_bulkupdate_page($count = NULL, $clear = FALSE) {
  if($_GET['clear']) {
    $clear = $_GET['clear'];
  }
  if(!$count) {
    $count = variable_get('linkintel_bulkupdate_requests_sync_limit', 1);
  }
  $nids = linkintel_bulkupdate($count, $clear);
//dsm($nids);
  $output .= t('<h3>Requests synced</h3>');
  $output .= theme_linkintel_buildupdate_requests_updated($nids['requests']);
  
  $output .= t('<h3>Links auto generated</h3>');
  $output .= theme_linkintel_buildupdate_links_updated($nids['links']);
  return $output;
}

function linkintel_bulkupdate($limit_override = NULL, $clear = FALSE) {
  $nids = array(
    'requests' => array(),
    'links' => array(),
  );
  $nids['requests'] = linkintel_bulkupdate_requests($limit_override, $clear);
  // check if request need to be or have been bulk updated before link autogen
  if(!variable_get('linkintel_bulkupdate_all_requests_before_links', 1) 
    || (count($nids['requests']) == 0) 
    || (count($nids['requests']) < variable_get('linkintel_bulkupdate_requests_sync_limit', 100))) {
    $nids['links'] = linkintel_bulkupdate_links($limit_override, $clear);
  }
  return $nids;
}

function linkintel_bulkupdate_clear_requests_page($pid = NULL) {
  linkintel_clear_page_sync_update($pid);
  drupal_set_message(t('Requests cache has been cleared.'));
  drupal_goto('admin/settings/linkintel');
  return '';
}

function linkintel_bulkupdate_requests_page($count = NULL) {
  if(!$count) {
    $count = variable_get('linkintel_bulkupdate_requests_sync_limit', 1);
  }
  $nids = linkintel_bulkupdate_requests($count);
//dsm($nids);
  return theme_linkintel_buildupdate_requests_updated($nids);
}

function theme_linkintel_buildupdate_requests_updated($nids) {
  $node_items = array();
  if(!is_array($nids) || empty($nids)) {
    $node_items[] = t('No nodes were updated.');
  }
  else {
    if (is_array($nids)) {
      foreach ($nids AS $nid => $requests) {
        $node_items[] .= t('!link was updated.',
          array(
            '!link' => linkintel_l_report('node/' . $nid),
          )
        );
        $req_items = array();
        if (is_array($requests)) {
          foreach ($requests AS $request) {
            $req_items[] .= t('Request for keyword phrase %keyword was synced.',
              array(
                '%keyword' => $request->keyword
              )
            );    
          } 
        }
        $node_items[] = theme_item_list($req_items);   
      }
    }
  }
  $output = theme_item_list($node_items);
  return $output;  
}

function linkintel_bulkupdate_requests($limit_override = 0, $clear) {
  if(!$limit_override && !variable_get('linkintel_bulkupdate_enable', 1)) {
    return FALSE;
  }
  if($clear) {
    linkintel_clear_page_sync_update();
    drupal_set_message(t('Requests cache has been cleared.'));
  }
  $nids = array();
  $limit = variable_get('linkintel_bulkupdate_requests_sync_limit', 100);
  $args = linkintel_get_enabled_content_types();
  $placeholders = implode(',', array_fill(0, count($args), "'%s'"));
  $args[] = $limit;
  $sql = "
    SELECT DISTINCT n.nid
    FROM {node} n
    LEFT JOIN {linkintel_page} p ON p.nid = n.nid
    WHERE p.last_sync IS NULL
    AND n.type IN ($placeholders)
    LIMIT %d
  ";
  $result = db_query($sql, $args);
//dsm(vsprintf($sql, $args));
  while($row = db_fetch_object($result)) {
    $node = node_load($row->nid);
    $requests = linkintel_sync_requests($node);
    if (!empty($requests)) {
      $nids[$row->nid] = $requests;
    }
  }
  return $nids;
}

function linkintel_get_enabled_content_types() {  
  static $types;
  if($types) {
    return $types;
  }
  $all = node_get_types();
//dsm($all);
//return array('blog');
  foreach($all AS $type) {
    if(variable_get('linkintel_type_' . $type->type . '_enable', variable_get('linkintel_enable', LINKINTEL_ENABLE_DEFAULT))) {
      $types[] = $type->type;
    }
  }
  return $types;
}

function linkintel_bulkupdate_clear_links_page($pid = NULL) {
  linkintel_clear_page_autogen_update($pid);
  drupal_set_message(t('Links cache has been cleared.'));
  drupal_goto('admin/settings/linkintel');
  return '';
}

function linkintel_bulkupdate_links_page($count = NULL) {
  if(!$count) {
    $count = variable_get('linkintel_bulkupdate_autogen_limit', 1);
  }
  $nids = linkintel_bulkupdate_links($count);
  //dsm($nids);
  return theme_linkintel_buildupdate_links_updated($nids);
}

function theme_linkintel_buildupdate_links_updated($nids) {
  $node_items = array();
  if(!is_array($nids) || empty($nids)) {
    $node_items[] = t('No nodes were updated.');
  }
  else {
    foreach($nids AS $nid => $links) {
      $node_items[] .= t('!link was updated.',
        array(
          '!link' => linkintel_l_report('node/' . $nid),
        )
      );
      $link_items = array();
      if(is_array($links) && count($links)) {
        foreach($links AS $link) {
          if($link->autogen == 1) {
            $link_items[] = t('%link_mode link !link was auto generated. (via requests match on %keyword)',
              array(
                '%link_mode' => ($link->mode == 1) ? t('Content') : t('Sidebar'),
                '!link' => linkintel_l($link, ($link->mode == 1)),
                '%keyword' => $link->keyword,
              )
            );
          }
          else {
            $link_items[] = t('Coded link !link was found.',
              array(
                '!link' => linkintel_l($link),
              )
            );            
          }    
        } 
      }
      else {
        $link_items[] = t('No links were auto generated.');
      }
      $node_items[] = theme_item_list($link_items);   
    }
    
  }
  $output = theme_item_list($node_items);
  return $output;  
}

function linkintel_bulkupdate_links($limit_override = 0, $clear = FALSE) {
  if(!$limit_override && !variable_get('linkintel_bulkupdate_enable', 1)) {
    return FALSE;
  }
  if($clear) {
    linkintel_clear_page_autogen_update($pid);
    drupal_set_message(t('Links cache has been cleared.'));
  }
  $nids = array();
  $args = linkintel_get_enabled_content_types();
  $placeholders = implode(',', array_fill(0, count($args), "'%s'")); 
  // update expire 
  $args[] = time() - variable_get('linkintel_bulkupdate_autogen_expire', 90) * 60 * 60 * 24;
  $args[] = variable_get('linkintel_bulkupdate_autogen_limit', 10);

  $sql = "
    SELECT DISTINCT n.nid
    FROM {node} n
    LEFT JOIN {linkintel_page} p ON p.nid = n.nid
    WHERE n.type IN ($placeholders)
    AND (p.last_autogen IS NULL OR p.last_autogen < %d)
    ORDER BY p.last_autogen
    LIMIT %d    
  ";
//dsm(vsprintf($sql, $args));
  $result = db_query($sql, $args);
  $count = 0;
  while($row = db_fetch_object($result)) {
    $node = node_load($row->nid);
    linkintel_save_coded_links($node);
    $links = linkintel_autogen_links_by_page($node);
    $nids[$row->nid] = $links;
    $count++;
  }
  if($count) {
    watchdog('linkintel', t('Links were auto generated for %count pages.', array('%count' => $count)));
  }
  return $nids;
}

function linkintel_bulkupdate_porterstemmer_page($limit = 100) {
  $keywords = linkintel_bulkupdate_porterstemmer($limit);
  $output = t('%count keywords were stemed.', array('%count' => count($keywords)));
  return $output;
}

function linkintel_bulkupdate_porterstemmer($limit = 100) {
  if(!module_exists('porterstemmer')) {
    return FALSE;
  }  
  $sql = "
    SELECT *
    FROM {linkintel_keyword}
    WHERE keyword_stem IS NULL OR keyword_stem = ''
    LIMIT %d
  ";
  $keywords = array();
  $result = db_query($sql, $limit);
  while($r = db_fetch_object($result)) {
    $keyword_stem = porterstemmer_search_preprocess($r->keyword);
    $sql = "
      UPDATE {linkintel_keyword}
      SET keyword_stem = '%s'
      WHERE kid = %d
    ";
    db_query($sql, $keyword_stem, $r->kid); 
    $keywords[$r->keyword] = $keyword_stem;
  }
  return $keywords;
}