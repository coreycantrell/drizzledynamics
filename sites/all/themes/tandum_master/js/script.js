﻿/* Cookie plugin */
(jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { // name and value given, set cookie
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
        // CAUTION: Needed to parenthesize options.path and options.domain
        // in the following expressions, otherwise they evaluate to undefined
        // in the packed version for some reason...
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
})(jQuery);

$(document).ready(function(){  
    
    var editMode = $.cookie('editMode');
    var displayMessages = $.cookie('displayMessages');
    
    if (editMode == 'false'){
    	$("div.tabs").hide();
    	$("#editModeOff").show();
    	$("#editModeOn").hide();
    } else {
        $("div.tabs").show();
    	$("#editModeOff").hide();
    	$("#editModeOn").show();
    }
    
    $("#editModeOn").click(function() {
        $.cookie("editMode", "false", "options");
        $("div.tabs").hide();
    	$("#editModeOff").show();
    	$(this).hide();
    });
    
    $("#editModeOff").click(function() {
        $.cookie("editMode", "true", "options");
        $("div.tabs").show();
    	$(this).hide();
    	$("#editModeOn").show();
    });
    
    if (displayMessages== 'false'){
    	$("div.messages").removeClass("collapse");
    	$("#messagesOff").show();
    	$("#messagesOn").hide();
    } else {
        $("div.messages").addClass("collapse");
    	$("#messagesOn").show();
    	$("#messagesOff").hide();
    }
    
    $("#messagesOn").click(function() {
        $.cookie("displayMessages", "false", "options");
        $(this).hide();
        $("#messagesOff").show();
        $("div.messages").removeClass("collapse");
    });
    
    $("#messagesOff").click(function() {
        $.cookie("displayMessages", "true", "options");
        $(this).hide();
        $("#messagesOn").show();
        $("div.messages").addClass("collapse");
    });
    
    $("div.messages.collapse").click(function() {
	    $(this).toggleClass("collapse");
    });

	$(".statusRow").click(function() {
		$(this).next(".logRow").toggle();
	});
	
	$('.views-field-nothing img').bt({
	  contentSelector: "$(this).next('span');",
	  fill:             "#FFFFFF",
	  strokeStyle:      "#9C9C9C",
	  spikeLength: 		5,
	  shrinkToFit:      true,
	  shadowOverlap:    true,
	  shadow: true,
      shadowColor: 'rgba(0,0,0,.5)',
      shadowBlur: 8,
      shadowOffsetX: -1,
      shadowOffsetY: -1,
	  hoverIntentOpts: {
		interval: 0,
		timeout: 0
	  }
	});
	
	// Add cool-ness to front page blocks.
	$('.front .region-content-top .block').draggable();
	$('.front .region-content-top #block-views-repository-block_2 .content').jScrollPane({contentWidth:400});
	$('.front .region-content-top #block-block-3 .content').jScrollPane({contentWidth:300});
	$('.front .region-content-top #block-views-activity-block_1 .content').jScrollPane();
    
	//$('.front .region-content-top #block-views-activity-block_1 .content').resizable({ 
	//    handles: "s",
	//    containment: "#page",
	//    minHeight: 190,
	//    minWidth: 181
	//});
	
	var months = ["January",
                  "February",
                  "March",
                  "April",
                  "May",
                  "June",
                  "July",
                  "August",
                  "September",
                  "October",
                  "November",
                  "December"]
                  
    var curMonth = months[new Date().getMonth()];
    var curDay = new Date().getDate();
    var curYear = new Date().getFullYear();
    var curDate = 'Sent ' + curMonth + ' ' + curDay +', ' + curYear;
	
	$('#email-invoice').click(function() {
	    $('#edit-field-invoice-date-email-0-value').val(curDate);
        Drupal.editablefields.onchange('#edit-field-invoice-date-email-0-value');
    });
    
    $('#email-notify').click(function() {
	    $('#edit-field-invoice-date-notify-0-value').val(curDate);
        Drupal.editablefields.onchange('#edit-field-invoice-date-notify-0-value');
    });
	
});