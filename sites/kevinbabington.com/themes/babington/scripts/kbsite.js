﻿$(document).ready(function(){

	$(".view-id-Videos.view-display-id-page_1 img, .view-id-show_horse img.imagecache").lazyload();	

    //Frontpage image slideshow
    $('#block-views-Slideshow-block_1 .view-content').cycle({ 
        fx:     'fade',
        speed:   300, 
        timeout: 3000, 
        next:   '#s3', 
        pause:   1 
    });
	
	$('#block-views-Videos-block_1 .item-list ul').cycle({ 
		fx:      'fade', 
		speed:    'fast', 
		timeout:  5000,
		pager:  '#portfolioNav',
		prev:    '#prev',
        next:    '#next',
		pause:   1 
	});
	
});