<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/jquery.cycle.min.js"></script>
  <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/jquery.bt.js"></script>
  <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/jquery.lazyload.js"></script>
  <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/dzdy.required.js"></script>
  <!--[if IE]><script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/excanvas.js"></script><![endif]-->
</head>
<body class="<?php print $body_classes; ?>">
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=179094622148866";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<div id="page-wrapper">
		<div id="header-top">
		Instruction in a strong, safe, and effective seat that aids in the advancement of both horse and rider.
		</div>
		<div id="page">
			<div id="header">
				<?php if ($header_links): ?>
					<?php print $header_links; ?>
				<?php endif; ?>
				<div id="address">
				186 W. Mill Road<br/>
				Long Valley, NJ 07853<br/>
				(908) 876-5867
				</div>
			</div>
			<div id="main">
				<div id="main-inner" class="clear-block with-navbar">
					<div id="content">
						<div id="content-inner">
							<?php print $messages; ?>
							<?php if ($content_top): ?>
							<div id="content-top" class="region region-content_top">
								<?php print $content_top; ?>
							</div>
							<?php endif; ?>
							<?php if ($breadcrumb || $title || $tabs || $help): ?>
							<div id="content-header">
								<?php print $breadcrumb; ?>
								<?php if ($title): ?>
									<h1 class="title"><?php print $title; ?></h1>
								<?php endif; ?>
								<?php if ($tabs): ?>
									<div class="tabs"><?php print $tabs; ?></div>
								<?php endif; ?>
								<?php print $help; ?>
							</div>
							<?php endif; ?>
							<div id="content-area">
							<?php print $content; ?>
							</div>
							<?php if ($content_bottom): ?>
							<div id="content-bottom" class="region region-content_bottom">
								<?php print $content_bottom; ?>
							</div>
							<?php endif; ?>
							<div class="fb-like-box" data-href="https://www.facebook.com/devinryanatriverrunstables" data-colorscheme="light" data-show-faces="false" data-header="true" data-stream="false" data-show-border="true"></div>
							<div id="youtubeLink">
							<a href="http://www.youtube.com/user/RiverRunStablesGP#g/u">View all of our videos on YouTube</a><img src="/sites/devinryan.com/themes/devinryan/images/youtube.png" alt="" />
							</div>
						</div>
					</div>
					<?php if ($search_box || $secondary_links || $navbar): ?>
					<div id="navbar">
						<div id="navbar-inner" class="clear-block region region-navbar">
							<a name="navigation" id="navigation"></a>
							<?php if ($search_box): ?>
							<div id="search-box">
								<?php print $search_box; ?>
							</div>
							<?php endif; ?>
						  <?php if ($secondary_links): ?>
							<div id="secondary" class="clear-block">
							  <?php print theme('links', $secondary_links); ?>
							</div>
						  <?php endif; ?>
						  <?php print $navbar; ?>
						</div>
					</div>
					<?php endif; ?>
					<?php if ($left): ?>
					<div id="sidebar-left"><div id="sidebar-left-inner" class="region region-left">
						<?php print $left; ?>
					</div></div>
					<?php endif; ?>
					<?php if ($right): ?>
					<div id="sidebar-right"><div id="sidebar-right-inner" class="region region-right">
						<?php print $right; ?>
					</div></div>
					<?php endif; ?>
				</div>
			</div>

			<?php if ($footer || $footer_message): ?>
				<div id="footer">
					<div id="footer-inner" class="region region-footer">
						<?php if ($footer_message): ?>
						<div id="footer-message"><?php print $footer_message; ?></div>
						<?php endif; ?>
						<?php print $footer; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
		<span class="clear"></span>
	</div>
	<?php if ($closure_region): ?>
    <div id="closure-blocks" class="region region-closure">
		<?php print $closure_region; ?>
		<div id="dzdy_badge" style="margin-bottom:10px;text-align:center;"></div>
	</div>
	<?php endif; ?>
	<?php print $closure; ?>
	<div id="dialog-containers">
		<div id="usef-gateway" style="display:none">
		<div class="close-usef-gateway">Close</div>
			<p>You're about to be redirected to the <a href="http://www.usef.org">USEF</a> Horse Results Search webpage. Use the information below in your search.<br/>
				<span>
					Horse's Name: <strong><?php print check_plain($node->title) ?></strong><br/>			
				</span>
				<a href="http://www.usef.org/_IFrames/Searches/horseResults.aspx" target="usef-search" class="gateway-link" z-index="0">Continue to search...</a>
			</p>
		</div>
	</div>
</body>
</html>
