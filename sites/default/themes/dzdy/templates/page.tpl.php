<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir;?>" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	<title><?php print $head_title; ?> - Design &amp; Technology Solutions | Web Development, Graphic Design, Social Branding</title>
	<?php print $head; ?>
	<meta name="viewport" content="width=960, initial-scale=1"/>
	<?php print $styles; ?>
	<link href="http://fonts.googleapis.com/css?family=Karla|Volkhov" rel="stylesheet" type="text/css">
	<link href="https://s3.amazonaws.com/scripts.drizzledynamics.com/formalize/formalize.css" rel="stylesheet" type="text/css">
	<?php print $scripts; ?>	
	<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/jquery.bt.js"></script>
	<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/jquery.cycle.all.js"></script>
	<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/dzdy.required.js"></script>
	<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/formalize/jquery.formalize.js"></script>
	<!--[if IE]><script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/excanvas.js"></script><![endif]-->
	<script src="http://platform.twitter.com/anywhere.js?id=iNtW3ITrFe7unu4CBwZhbw&amp;v=1"></script>
</head>
<body class="<?php print $classes; ?>">
	<a id="top"></a>
	<div id="header">
		<div class="page-region">
			<a href="<?php print $base_path ?>" title="Drizzle Dynamics Home">
				<img src="<?php print $base_path ?>sites/default/themes/dzdy/images/header-logo.png" width="444" height="54" alt="Drizzle Dynamics LLC"/>
			</a>
			<?php if ($primary_links): ?>
				<?php print theme(array('links__system_main_menu', 'links'), $primary_links,
					array(
					  'id' => 'nav',
					));
				?>
			<?php endif; ?>
		</div>
	</div>
	<div id="page">
		<div id="main-wrapper" class="page-region">
			<div id="main" class="clearfix">
				<div id="content" class="column">
					<div class="section">
						<?php print $breadcrumb; ?>
						<?php if ($title): ?>
							<h1 class="title"><?php print $title; ?></h1>
						<?php endif; ?>
						<?php print $messages; ?>
						<?php if ($tabs): ?>
							<div class="tabs"><?php print $tabs; ?></div>
						<?php endif; ?>
						<?php print $help; ?>
						<?php print $content_top; ?>
						<div id="content-area">
							<?php print $content; ?>
						</div>
						<?php print $content_bottom; ?>
						
					</div>
				</div>
				<?php print $sidebar_first; ?>
				<?php print $sidebar_second; ?>
			</div>
		</div>
	</div>
	<div id="footer">
		<div class="page-region">
			<ul id="footer-links">
				<li id="contact-tab">Interested in our servies?</li>
				<li id="connect-tab" class="active">Follow us online.</li>
				<li class="working-tab" style="display: none">Interested in working with us?</li>
			</ul>
			<div id="footer-boxs">
				<div id="contact-box" style="display:none">
					<div id="call-box">
					Give us a call at<br/>
					<strong>215-995-1208</strong><br/>
					</div>
					Or, send an email to<br/><strong style="font-size:1.6em"><a href="mailto:team@drizzledynamics.com">team@drizzledynamics.com</a></strong>
				</div>
				<div id="working-box" style="display:none">
					<?php print $contact_block; ?>
				</div>
				<div id="connect-box">
					
					<div style="float:left">
					<strong class="header">Be social! Connect with us.</strong>
                    <div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#appId=187406787976223&amp;xfbml=1"></script><fb:like href="http://www.facebook.com/pages/Drizzle-Dynamics-LLC/148853071824026" send="true" layout="button_count" width="450" height="20" show-faces="false" show_faces="false" font="arial"></fb:like>					
					<div id="follow-twitterapi"></div>
					</div>
					
					<div style="float:right;background-color: #F4F7F5;padding:10px;border-radius:5px;margin-top:8px;width:339px">
					<?php print $twitter_feed; ?>
					</div>
				</div>
			</div>
			<?php if ($primary_links): ?>
				<?php print theme(array('links__system_main_menu', 'links'), $primary_links,
					array(
					  'id' => 'footer-nav',
					));
				?>
			<?php endif; ?>
			<?php print $page_closure; ?>
			<div id="footer-social">
				<a href="http://twitter.com/drizzledynamics"><img src="<?php print $base_path ?>sites/default/themes/dzdy/images/twitter.png"/></a>
				<a href="http://www.facebook.com/drizzledynamics?sk=app_179094622148866"><img src="<?php print $base_path ?>sites/default/themes/dzdy/images/facebook.png"/></a>
			</div>
		</div>
	</div>
	<?php print $closure; ?>
    <div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({appId: '204093716269591', status: true, cookie: true,
                 xfbml: true});
      };
      (function() {
        var e = document.createElement('script');
        e.type = 'text/javascript';
        e.src = document.location.protocol +
          '//connect.facebook.net/en_US/all.js';
        e.async = true;
        document.getElementById('fb-root').appendChild(e);
      }());
    </script>

	</body>
</html>
