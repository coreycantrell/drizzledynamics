﻿$(document).ready(function(){  

	$('#submit').click(function(){
		var email = $("#email").val();
		window.location = "/contact-us?email=" + email;
	});

	//Carousels
	$('#front-carousel').cycle({ 
		fx:      'scrollLeft', 
		speed:    'fast', 
		timeout:  3000,
		pager:  '#fcsNav .pagerbar',
		prev:    '#fcsNav .prev',
        next:    '#fcsNav .next',
		pause:   1 
	});

	//Twitter hovercards
	twttr.anywhere(function(twitter) {
		twitter.hovercards();
	});
	
	//Twitter follow button
	twttr.anywhere(function (T) {
		T('#footer-boxs #follow-twitterapi').followButton("drizzledynamics");
	});

	//Hover states for footer webform
	$("#footer-links li").click(function() {
		$("#footer-links li").removeClass("active");
		$(this).addClass("active");
	});	
	
	$(".working-tab.link").click(function() {
		$("li#contact-tab").removeClass("active");
		$("li.working-tab").addClass("active");
	});
	
	$("#contact-tab").click();
	
	$(".working-tab").click(function() {
		$("#working-box").show();
		$("#contact-box").hide();
		$("#connect-box").hide();
	});
	
	$("#contact-tab").click(function() {
		$("#contact-box").show();
		$("#working-box").hide();
		$("#connect-box").hide();
	});
	
	$("#connect-tab").click(function() {
		$("#connect-box").show();
		$("#working-box").hide();
		$("#contact-box").hide();
	});
	
	//Masks phone number input
	$("input#edit-submitted-phone").mask("(999) 999-9999",{placeholder:" "});
		
	$(".view-dependents-sidebar img").bt({
        positions: ['right', 'top', 'left', 'bottom'],
        fill: '#727D74',
        padding: '15px 15px',
        strokeWidth: 1,
        strokeStyle: "#475F4C",
        cornerRadius: 5,
        spikeGirth: 33,
        spikeLength: 15,
        overlap: -1,
        cssStyles: {
			fontWeight: 'bold', 
			fontSize: '13pt', 
			textAlign: 'left',
			width: 'auto',
			color: '#FFFFFF',
			whiteSpace: 'noWrap'
		}
	});	
	
	$("#nav li.menu-306 a").bt({
        positions: ['bottom', 'top', 'left', 'right'],
        fill: '#FFFFFF',
        padding: '5px 15px',
        strokeWidth: 1,
        strokeStyle: "#475F4C",
        cornerRadius: 5,
        spikeGirth: 33,
        spikeLength: 15,
        overlap: -1,
        cssStyles: {
			fontWeight: 'bold', 
			fontSize: '11pt', 
			textAlign: 'center',
			width: 'auto',
			whiteSpace: 'noWrap'
		}
	});	
	
	$("#online-presence").bt({
        positions: ['bottom', 'top', 'left', 'right'],
        fill: '#DDDDDD',
        padding: '5px 15px',
        strokeWidth: 1,
        strokeStyle: "#475F4C",
        cornerRadius: 5,
        spikeGirth: 33,
        spikeLength: 15,
        overlap: -1,
        cssStyles: {
			fontWeight: 'normal', 
			fontSize: '11pt', 
			textAlign: 'center',
			width: '500px',
			color: '#5A6D63',
			lineHeight: '16pt'
		}
	});	
	
	$('img.inactive').bt({
	  showTip: function(box){
		$(box).fadeIn(200);
	  },
	  hideTip: function(box, callback){
		$(box).animate({opacity: 0}, 200, callback);
	  },
	  positions: ['bottom', 'top', 'left', 'right'],
	  fill:             "#FFFFFF",
	  strokeStyle:      "#b74e4e",
	  spikeLength: 		5,
	  width:            '80px',
	  shadowOverlap:   true,
	  cssStyles:        {
		color: '#b74e4e',
		fontWeight: 'bold',
		textAlign: 'center'
	  },
	  hoverIntentOpts: {
		interval: 0,
		timeout: 0
	  }
	});
	
	$('img.active').bt({
	  showTip: function(box){
		$(box).fadeIn(200);
	  },
	  hideTip: function(box, callback){
		$(box).animate({opacity: 0}, 200, callback);
	  },
	  positions: ['bottom', 'top', 'left', 'right'],
	  fill:             "#FFFFFF",
	  strokeStyle:      "#4b9a59",
	  spikeLength: 		5,
	  width:            '80px',
	  shadowOverlap:   true,
	  cssStyles:        {
		color: '#4b9a59',
		fontWeight: 'bold',
		textAlign: 'center'
	  },
	  hoverIntentOpts: {
		interval: 0,
		timeout: 0
	  }
	});
	
	$('img.dev').bt({
	  showTip: function(box){
		$(box).fadeIn(200);
	  },
	  hideTip: function(box, callback){
		$(box).animate({opacity: 0}, 200, callback);
	  },
	  positions: ['bottom', 'top', 'left', 'right'],
	  fill:             "#FFFFFF",
	  strokeStyle:      "#68726a",
	  spikeLength: 		5,
	  width:            '120px',
	  shadowOverlap:   true,
	  cssStyles:        {
		color: '#68726a',
		fontWeight: 'bold',
		textAlign: 'center'
	  },
	  hoverIntentOpts: {
		interval: 0,
		timeout: 0
	  }
	});

});