<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix">
	<?php if ($teaser): ?>
	<div class="park-marker" title="This is a park profile which contains a group of trails for a specific park.">&nbsp;</div>
	<div class="trail-name">
		<a href="<?php print '/' . drupal_lookup_path('alias',"node/".$node->nid); ?>">
		<?php print check_plain($title); ?>, <?php print $node->locations[0]['city'] ?>, <?php print $node->locations[0]['province'] ?>
		</a>
	</div>
	<div class="trail-image">
		<?php
		$image_safeurl = str_replace(' ', '%20', $title); 
		$image_json_string = file_get_contents("https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q="  . $image_safeurl);
		$image_parsed_json = json_decode($image_json_string, true);
		echo '<a href="../' . drupal_lookup_path("alias","node/".$node->nid) . '"><img class="park-image" src="' . $image_parsed_json["responseData"]["results"][0]["url"] . '"/></a>';
	?>
	</div>

	<?php else: ?>

	<?php
		$image_safeurl = str_replace(' ', '%20', $title); 
		$image_json_string = file_get_contents("https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q="  . $image_safeurl);
		$image_parsed_json = json_decode($image_json_string, true);
		echo '<img class="park-image" src="' . $image_parsed_json["responseData"]["results"][0]["url"] . '"/>';
	?>
	<div>
		<div class="park-city"><?php print $node->locations[0]['city'] ?>, <?php print $node->locations[0]['province'] ?></div>
		<p>
			<?php
				function limit_text($text, $limit) {
			      if (str_word_count($text, 0) > $limit) {
			          $words = str_word_count($text, 2);
			          $pos = array_keys($words);
			          $text = substr($text, 0, $pos[$limit]) . '...';
			      }
			      return $text;
			    }
				$text_safeurl = str_replace(' ', '%20', $title); 
				$text_json_string = file_get_contents("http://en.wikipedia.org/w/api.php?action=query&prop=extracts&format=json&exintro=&titles=" . $text_safeurl); 
				$text_parsed_json = json_decode($text_json_string); 
				foreach($text_parsed_json->query->pages as $k) {
					echo limit_text($k->extract, 120);
				}
			?>
			<div class="park-source">Park description from <a href="http://en.wikipedia.org/wiki/<?php echo str_replace(' ', '%20', $title) ?>" target="_blank">Wikipedia</a>. Image retrieved from Google Images.</div>
		</p>
	</div>
	<?php endif; ?>
</div><!-- /.node -->