<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://opengraphprotocol.org/schema/" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>">
  <head>
	<title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/jquery.cycle.min.js"></script>
    <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/jquery.lazyload.js"></script>
    <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/dzdy.required.js"></script>
    <!--[if lte IE 7]><?php print phptemplate_get_ie_styles(); ?><![endif]-->
  </head>
  <body <?php print phptemplate_body_class($left, $right); ?> >
	<!-- Facebook HTML5 Initailzier -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
<!-- Layout -->
    <div id="wrapper">
      <div id="header">
        <?php if ($search_box): ?><?php print $search_box ?><?php endif; ?>
        <div class="clear"></div>
      </div> <!-- /#header -->

      <div id="nav">
        <?php if ($nav): ?>
          <?php print $nav ?>
        <?php endif; ?>

        <?php if (!$nav): ?><!-- if block in $nav, removes default $primary and $secondary links -->

          <?php if (isset($primary_links)) : ?>
            <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
          <?php endif; ?>
          <?php if (isset($secondary_links)) : ?>
            <div id="secondary-links"><?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?></div>
          <?php endif; ?>

        <?php endif; ?>
      </div> <!-- /#nav -->

      <div id="container">

        <?php if ($left): ?>
          <div id="sidebar-left" class="sidebar">
            <?php print $left ?>
          </div> <!-- /#sidebar-left -->
        <?php endif; ?>

        <div id="center">
          <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
          <?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
          <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block"><ul class="tabs primary">'. $tabs .'</ul>'; endif; ?>
          <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
          <?php if ($tabs): print '<span class="clear"></span></div>'; endif; ?>
          <?php if ($show_messages && $messages): print $messages; endif; ?>
          <?php print $help; ?>
          <?php print $header; ?>
          <?php print $content ?>
        </div> <!-- /#center -->
  
        <?php if ($right): ?>
          <div id="sidebar-right" class="sidebar">
            <?php print $right ?>
          </div> <!-- /#sidebar-right -->
        <?php endif; ?>

        <div id="footer" class="clear">
          <?php print $footer_message . $footer ?>
          <?php print $feed_icons ?>
        </div> <!-- /#footer -->

      </div> <!-- /#container -->
      <table id="post-footer" style="width:100%">
	  <tr><td style="text-align:left;vertical-align:top">
      <span style="font-size: 80%; opacity: 0.4"><?php global $user; ?><?php if ($user->uid) : ?><?php print l("User logout","logout"); ?><?php else : ?><?php print l("Staff login","user/login"); ?><?php endif; ?></span>
      </td>
	  <td style="text-align:right">
	  <span id="dzdy_badge" style="margin-bottom:10px;text-align:right;"></span>
	  </td></tr>
      </table>
      <span class="clear"></span>
    </div> <!-- /#wrapper -->
<!-- /layout -->

  <?php print $closure ?>
  
  </body>
</html>
