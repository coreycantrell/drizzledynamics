<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
  <meta name="msvalidate.01" content="9C10FFCAC889E99F74FCA0B1725A85E4" />
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <script type="text/javascript" src="<?php print $base_path ?>sites/panel.drizzledynamics.com/themes/dzdy_panel/scripts/dzdy.js"></script>
  <script type="text/javascript" src="http://www.drizzledynamics.com/sites/all/script-library/jquery.bt.js"></script>
  <script type="text/javascript" src="http://www.drizzledynamics.com/sites/all/script-library/jquery.dialog.js"></script>
  <script type="text/javascript" src="http://www.drizzledynamics.com/sites/all/script-library/jquery.cookie.js"></script>
  <!--[if IE]><script type="text/javascript" src="http://drizzledynamics.com/sites/all/script-library/excanvas.js"></script><![endif]-->
</head>
<body class="<?php print $body_classes; ?>">	
    <div id="user-bar-band"></div>
  <div id="page"><div id="page-inner">
    <div id="user-bar-block">
	<?php print date('M j, Y')?><?php global $user;?><?php if ($user->uid) : ?>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php print $base_path ?>user"><?php print $user->name ?></a> <?php print '<a href="'?><?php print $base_path ?><?php print 'logout"><img src="'?><?php print $base_path ?><?php print 'sites/panel.drizzledynamics.com/themes/dzdy_panel/images/signout.png" class="user-logout" title="All done? Click to logout."/></a>'; ?><?php endif; ?><br/>
	<?php global $user;
    $sql1 = 'select * from lh where uid='.$user->uid.';';
	$args1[] = PROFILE_PUBLIC_LISTINGS;
	$hc = db_query($sql1, $args1);
    while ($hr = db_fetch_object($hc)){
		$output ="<span id='last-access-date'>";
		$output .="Last Access ";      
		$output .= $hr->time;
		$output .="</span>";
	}
	echo $output;  
	?>
	</div>
    <div id="header"><div id="header-inner" class="clear-block">

        <div id="logo-title">
            <a href="<?php print $base_path ?>" title="Client Panel Home"><img src="<?php print $base_path ?>sites/panel.drizzledynamics.com/themes/dzdy_panel/images/logo.png" height="50" width="389"/></a>
        </div> <!-- /#logo-title -->

      <?php if ($header): ?>
        <div id="header-blocks" class="region region-header">
          <?php print $header; ?>
        </div> <!-- /#header-blocks -->
      <?php endif; ?>

    </div></div> <!-- /#header-inner, /#header -->

    <div id="main"><div id="main-inner" class="clear-block">

      <div id="content"><div id="content-inner">

        <?php if ($content_top): ?>
          <div id="content-top" class="region region-content_top">
            <?php print $content_top; ?>
          </div> <!-- /#content-top -->
        <?php endif; ?>

        <?php if ($title || $tabs || $messages): ?>
          <div id="content-header">
            <h1 class="title"><?php print ($title); ?></h1>
            <?php print $messages; ?>
            <?php if ($tabs): ?>
              <div class="tabs"><?php print $tabs; ?></div>
            <?php endif; ?>
          </div> <!-- /#content-header -->
        <?php endif; ?>

        <div id="content-area">
          <?php print $content; ?>
        </div>

        <?php if ($feed_icons): ?>
          <div class="feed-icons"><?php print $feed_icons; ?></div>
        <?php endif; ?>

        <?php if ($content_bottom): ?>
          <div id="content-bottom" class="region region-content_bottom">
            <?php print $content_bottom; ?>
          </div> <!-- /#content-bottom -->
        <?php endif; ?>
		<br style="clear:both"/>
      </div></div> <!-- /#content-inner, /#content -->

        <?php if ($left): ?>
        <div id="sidebar-left"><div id="sidebar-left-inner" class="region region-left">
          <?php print $left; ?>
        </div></div> <!-- /#sidebar-left-inner, /#sidebar-left -->
      <?php endif; ?>

      <?php if ($right): ?>
        <div id="sidebar-right"><div id="sidebar-right-inner" class="region region-right">
          <?php print $right; ?>
        </div></div> <!-- /#sidebar-right-inner, /#sidebar-right -->
      <?php endif; ?>

    </div></div> <!-- /#main-inner, /#main -->

    <?php if ($footer || $footer_message): ?>
      <div id="footer"><div id="footer-inner" class="region region-footer">

        <?php if ($footer_message): ?>
          <div id="footer-message"><?php print $footer_message; ?></div>
        <?php endif; ?>

        <?php print $footer; ?>
		
      </div></div> <!-- /#footer-inner, /#footer -->
    <?php endif; ?>

  </div></div> <!-- /#page-inner, /#page -->

  <?php if ($closure_region): ?>
    <div id="closure-blocks" class="region region-closure"><?php print $closure_region; ?></div>
  <?php endif; ?>

  <?php print $closure; ?>

</body>
</html>
