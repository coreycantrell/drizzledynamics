<?php
/**
 * @file maintenance-page.tpl.php
 *
 * Theme implementation to display a single Drupal page while off-line.
 *
 * All the available variables are mirrored in page.tpl.php. Some may be left
 * blank but they are provided for consistency.
 *
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" id="maintenance" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
  <title>Trailstack.com</title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/dzdy.required.js"></script>
  <meta name="description" content="Trailstack is a community based hiking and trail guide.">
  <meta name="author" content="hiking trails, hiking tips, trail maps, trail profiles, camping trails, backpacking trails">
</head>
<body class="<?php print $classes; ?>">

  <div id="page-wrapper"><div id="page">

    <img src="/sites/trailstack.com/themes/denominator/images/soon.png" height="306" width="960" alt="Trailstack is a community based hiking and trail guide"/>

  </div></div><!-- /#page, /#page-wrapper -->
  
  <div style="text-align:center;text-align:center;margin-top:25px;"><?php print $content; ?></div>

  <?php print $page_closure; ?>

  <?php print $closure; ?>
  
  <div style="margin-top:15%;font-size:9pt;text-align:center;color:#9F9F9F">
  &copy; 2013. All Rights Reserved. TrailStack. 
  <div id="dzdy_badge" style="margin-top:5px"></div>
  </div>

</body>
</html>
