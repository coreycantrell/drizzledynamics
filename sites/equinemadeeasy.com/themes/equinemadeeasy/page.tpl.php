<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
	<head>
		<title><?php print $head_title; ?></title>
		<?php print $head; ?>
		<meta name="description" content="Equine Made Easy offers equine equipment and supplements that are designed by horsemen for horsemen and are manufactured entirely in the United States from the highest quality materials.">
		<meta name="keywords" content="equine, equipment, products, caddy, wagon, horse, first, rambo, walker, supplements" />
		<?php print $styles; ?>
		<?php print $scripts; ?>
	</head>
	<body class="<?php print $body_classes; ?>">
		<div id="utility-bar">
			<div id="utility-bar-inner">
				<span id="utility-bar-left">
				Affordable Prodcuts Designed BY and FOR Professional Horseman!
				</span>
				<span id="utility-bar-right">
				<ul id="utility-bar-links">
					<li class="last"><a href="/help" title="Help">Help</a></li>
					<li><a href="/cart" title="Shopping Cart">Shopping Cart</a></li>
					<li><a href="/user" title="My Account">My Account</a></li>
					<li class="first"><?php global $user; ?><?php if ($user->uid) : ?><?php print l("Logout","logout"); ?><?php else : ?><?php print l("Login","user/login", array('query' => drupal_get_destination()));?><?php endif; ?></li>
				</ul>
				</span>
			</div>
		</div>
		<div id="page">
			<div id="header">
				<div id="header-left" onclick="location.href='<?php print $base_path ?>';">
					&nbsp;
					<span id="logo-tagline" style="display:none"></span>
				</div>
				<div id="header-right">
					<span id="social-links"><a href="http://www.facebook.com/#!/pages/Blue-Bell-PA/Equine-Made-Easy/125860960785945?ref=ts&ajaxpipe=1&__a=9" alt="Find us on Facebook"><img src="/sites/equinemadeeasy.com/themes/equinemadeeasy/images/facebook.png" alt="Facebook Logo" height="16" width="16"/> <span>Find us on Facebook</span></a>&nbsp;&nbsp;<a href="http://twitter.com/equinemadeeasy" alt="Follow us on Twitter"><img src="/sites/equinemadeeasy.com/themes/equinemadeeasy/images/twitter.png" alt="Twitter Logo" height="16" width="16"/> <span>Follow us on Twitter</span></span></a>
					<div id="search-container">
						<span id="search-label">
							Find what your looking for faster.<br/>
							<span>Search our site!</span>
						</span>
						<?php print drupal_get_form('search_theme_form');?>
					</div>
				</div>
			<br style="clear:both"/>
			</div>
			<?php if ($navbar): ?>
			<div id="navbar">
				<div id="navbar-inner" class="clear-block region region-navbar">
					<?php print $navbar; ?>
				</div>
			</div> 
			<?php endif; ?>
			<div id="main">
				<div id="main-inner" class="clear-block<?php if ($primary_links || $secondary_links || $navbar) { print ' with-navbar'; } ?>">
					<div id="content">
						<div id="content-inner">
							<?php if ($content_top): ?>
							<div id="content-top" class="region region-content_top">
								<?php print $content_top; ?>
							</div>
							<?php endif; ?>
							<?php if ($title || $tabs || $help || $messages): ?>
							<div id="content-header">
								<?php print $messages; ?>
								<?php if ($tabs): ?>
								<div class="tabs"><?php print $tabs; ?></div>
								<?php endif; ?>
								<?php if ($title): ?>
								<h1 class="title"><?php print $title; ?></h1>
								<?php endif; ?>
								<?php print $help; ?>
							</div>
							<?php endif; ?>
							<div id="content-area">
								<?php print $content; ?>
							</div>
							<?php if ($content_bottom): ?>
							<div id="content-bottom" class="region region-content_bottom">
								<?php print $content_bottom; ?>
							</div>
							<?php endif; ?>
						</div>
					</div>
				<?php if ($right): ?>
				<div id="sidebar-right">
					<div id="sidebar-right-inner" class="region region-right">
						<?php print $right; ?>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php if ($footer || $footer_message): ?>
    <div id="footer">
		<div id="footer-inner" class="region region-footer">
			<?php if ($footer_message): ?>
			<div id="footer-message">
				<?php print $footer_message; ?>
			</div>
			<?php endif; ?>
        <?php print $footer; ?>
		</div>
	</div>
    <?php endif; ?>
  <?php if ($closure_region): ?>
    <div id="closure-blocks" class="region region-closure"><?php print $closure_region; ?></div>
  <?php endif; ?>
  <?php print $closure; ?>
</body>
</html>
