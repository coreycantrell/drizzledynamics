<?php
if ($title == 'Access denied') {
header( 'Location: /user/login' );
} ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
  <title>
    <?php if (arg(0) == 'user' && arg(1) == 'register') : ?>
        Create an Account | Tandum by Drizzle Dynamics
    <?php elseif (arg(0) == 'user' && arg(1) == 'password') : ?>
        Retrieve Lost Password | Tandum by Drizzle Dynamics
    <?php elseif (arg(0) == 'user' && arg(1) == 'login') : ?>
        User Login | Tandum by Drizzle Dynamics
    <?php elseif (arg(0) == 'user') : ?>
        User Account | Tandum by Drizzle Dynamics
    <?php elseif ($node->type == 'ticket') : ?>
        Ticket <?php print $node->nid ?> | Tandum by Drizzle Dynamics
    <?php else : ?>
        <?php print $head_title; ?>
    <?php endif ; ?>
    </title>
    <meta name="robots" content="noindex" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php print $head; ?>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link href='https://s3.amazonaws.com/scripts.drizzledynamics.com/formalize/formalize.css' rel='stylesheet' type='text/css'>
    <link href='https://s3.amazonaws.com/scripts.drizzledynamics.com/jscrollpane/jscrollpane.css' rel='stylesheet' type='text/css'>
    <?php print $styles; ?>
    <link href='/sites/all/themes/tandum_master/css/responsive.css' rel='stylesheet' type='text/css'>
    <!--[if !IE 7]>
    	<style type="text/css">
    		#wrap {display:table;height:100%}
    	</style>
    <![endif]-->
</head>
<body class="<?php print $classes; ?>">

  <?php if ($primary_links): ?>
    <div id="skip-link"><a href="#main-menu"><?php print t('Jump to Navigation'); ?></a></div>
  <?php endif; ?>

  <div id="page-wrapper"><div id="page">

    <div id="header"><div class="section clearfix">
		<div id="site-logo">
			<a href="<?php print $base_path; ?>">tandum</a>
		</div>
      <?php print $header; ?>
    </div></div><!-- /.section, /#header -->
	<div id="navHeader" class="section clearfix">
		<?php print $navigation; ?>
	</div>
	<div id="messageHeader" class="section clearfix">
	<?php print $messages; ?>
	</div>

    <div id="main-wrapper"><div id="main" class="clearfix<?php if ($primary_links || $navigation) { print ' with-navigation'; } ?>">

      <div id="content" class="column"><div class="section">

        <?php print $highlight; ?>

        <?php if ($title): ?>
          <h1 class="title">
            <?php if (arg(0) == 'user' && arg(1) == 'register') : ?>
                Create an Account
            <?php elseif (arg(0) == 'user' && arg(1) == 'password') : ?>
                Retrieve Lost Password
            <?php elseif (arg(0) == 'user' && arg(1) == 'login') : ?>
                User Login
            <?php elseif (arg(0) == 'user') : ?>
                User Account
            <?php else : ?>
                <?php print $title ?>
            <?php endif ; ?>
            </h1>
        <?php endif; ?>
        <?php if ($tabs): ?>
          <div class="tabs"><?php print $tabs; ?></div>
        <?php endif; ?>
        <?php print $help; ?>

        <?php print $content_top; ?>

        <div id="content-area">
          <?php print $content; ?>
        </div>

        <?php print $content_bottom; ?>

      </div></div><!-- /.section, /#content -->

    </div></div><!-- /#main, /#main-wrapper -->
    
    </div></div><!-- /#page, /#page-wrapper -->
    
    <?php print $page_closure; ?>

     <?php print $closure; ?>

    <?php if ($footer || $footer_message): ?>
      <div id="footer"><div class="section">

        <?php if ($footer_message): ?>
          <div id="footer-message"><?php print $footer_message; ?></div>
        <?php endif; ?>

        <?php print $footer; ?>

      </div></div><!-- /.section, /#footer -->
    <?php endif; ?>
  
  <?php print $scripts; ?>
  <script type="text/javascript" src="https://www.google.com/jsapi?key=ABQIAAAArpaBv6YC4nXarJvJ0-O9dRQ03-XrT8WdLhG4SgCM_yJqDUPsURSu87ftONjfMio_sLuixIjsS8Ayvw"></script>
  <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/dzdy.required.js"></script>
  <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/formalize/jquery.formalize.js"></script>
  <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/jscrollpane/jquery.jscrollpane.js"></script>
  <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/jscrollpane/jquery.mousewheel.js"></script>
  <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/jscrollpane/mwheelIntent.js"></script>
  <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/jquery.bt.js"></script>

</body>
</html>
