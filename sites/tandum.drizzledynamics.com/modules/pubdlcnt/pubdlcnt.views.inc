<?php
// $Id: pubdlcnt.views.inc,v 1 2010/01/08 01:11:33 cerup Exp $

/**
 *  @file
 *  This defines views hooks for the pubdlcnt module. It will be loaded automatically as needed by the Views module.
 */

/**
 * Implementation of hook_views_data()
 */
function pubdlcnt_views_data() {
  // ----------------------------------------------------------------
  // pubdlcnt table

  // Describe the pubdlcnt table.
  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data['pubdlcnt']['table']['group']  = t('pubdlcnt');

  $data['pubdlcnt']['table']['base'] = array(
    'field' => 'nid',
    'title' => t('Downloads'),
    'help' => t('Download counts'),
  );

  $data['pubdlcnt']['table']['join'] = array(
    'users' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    // This goes to the node so that we have consistent authorship.
    'node_revisions' => array(
      'left_table' => 'node',
      'left_field' => 'nid',
      'field' => 'nid',
    ), 
  );

  // Describe the count column of the pubdlcnt table.
  $data['pubdlcnt']['count'] = array(
    'title' => t('Count'),
    'help' => t("Counts"), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'numeric' => TRUE,
      'name field' => 'count', // display this field in the summary
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Describe the points column of the userpoints table.
  $data['pubdlcnt']['nid'] = array(
    'title' => t('nid'),
    'help' => t("nid"), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'numeric' => TRUE,
      'name field' => 'nid', // display this field in the summary
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Describe the points column of the userpoints table.
  $data['pubdlcnt']['name'] = array(
    'title' => t('Name'),
    'help' => t("Name"), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
      'numeric' => TRUE,
      'name field' => 'name', // display this field in the summary
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );



$data['pubdlcnt']['date'] = array(
    'title' => t('Updated date'), // The item it appears as on the UI,
    'help' => t('Download date.'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_field_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_field_date',
    ),
  );

  return $data;
}
