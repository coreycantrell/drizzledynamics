$Id: README.txt,v 1.3 2010/04/07 06:54:52 gestaltware Exp $


Description
---------------
Private Number defines a CCK field type for numbers which should be kept 
private, such as government-issued identification or account numbers. The 
module enables two of the industry standard best practices for handling 
confidential data: masking the number when viewed by users without view private 
number access permission and encrypting the number with a md5 block cipher in 
128 bit CFB mode when stored.

Private Number fields can optionally have a validation pattern, which 
will both validate input and provide a human-readable form of the pattern for 
field descriptions and error messages. The module exposes a hook for private 
number access to be integrated with contributed or custom modules.


Dependencies
---------------
CCK Content module


Related Modules
---------------
Secure Pages (http://drupal.org/project/securepages)
  private information should also be encrypted during transmission via SSL.

GnuPG (http://drupal.org/project/gnupg)
  OpenPGP API for encrypting emails.

encrypted_text (http://drupal.org/project/encrypted_text)
  CCK widget that encrypts fields with MySQL's AES_ENCRYPT() function, but 
  does not handle access for display.


Features
---------------
* Access permissions

  Private Number introduces two access permissions, view private number and 
  view own private number. Private numbers are displayed as 'Ending in 123' for 
  users without this permission. The number of digits displayed can be changed 
  on a per field basis with Display fields settings.

* md5 block cipher encryption

  Private Number data is stored in the database with md5 block cipher 
  encryption. This is recommended enabled to help thwart identity theft if 
  access to your database becomes compromised. A unique site key must be set in 
  either the $conf array of settings.php or in an external file outside of 
  docroot. See Key Management below.

* Validation patterns

  Validation patterns can optionally be added for any Private Number field. 
  In addition, validation patterns will (usually) output a human-readable form 
  for field descriptions and error messages. The human regex error messages can 
  be disabled at the field level. See Validation Patterns below.

* Third-party module integration

  Private Number can be integrated with contributed or custom modules by 
  implementing hook_private_number_access() to enable view or edit access. See
  Developers below.


Installation
---------------
1. install module: copy private_number directory and all its contents to your 
   modules directory
2. enable module: admin/build/modules
3. configure access permissions: admin/user/permissions#module-private_number
4. set a unique site key before using module: see Key Management below


Download
---------------
Download package and report bugs, feature requests, or submit a patch from the 
project page on the Drupal web site.
http://drupal.org/project/private_number


Key Management
---------------
Private Number implements md5 block cipher encryption which requires a unique 
cipher (or site) key. Note that changing the key will make previously stored 
Private Numbers unreadable! By default, a site key is set in the $conf array of 
settings.php with the $conf['private_number_key'] variable. 

For more security, use the site key path and create a text file named 
private_number.key one level above the Drupal installation (e.g. above www).
Place your key on the first line of that file. Change permissions on the 
file to 0444. The file can be moved to another location or name changed by 
altering the $conf['private_number_key_file'] variable.


Validation Patterns
-------------------
Private Number validation patterns are passed through preg_match with start and 
end of string delimiters automatically appended. It is beyond the scope of the
module to provide support for crafting your own regular expressions, but there
are numerous resources on the subject (a particularly good one is
http://www.regular-expressions.info/examples.html).

Examples of common validation patterns:

For a U.S. telephone number, the pattern would be:
$pattern='\(\d{3}\) \d{3}\-\d{4}';

which would output in human-readable form as:
(999) 999-9999

For a U.S. social security number, the pattern would be:
$pattern='\d{3}\-\d{2}\-\d{4}';

which would output in human-readable form as:
999-99-9999

We chose to use phrases for quantifiers because it provided more understandable 
output than the 9 notation, but can quickly get cumbersome when more than one
quantifier is used in an expression.

\d?     0 or 1 digit
\d+     1 or more digits
\d*     0 or more digits
\d{1,3} 1 to 3 digits

Be sure to escape meta characters in your validation pattern. For more 
information on preg_match, visit this page: 
http://us.php.net/manual/en/function.preg-match.php


Developers
---------------
Private Number can be integrated with contributed or custom modules by 
implementing hook_private_number_access() to enable view or edit access.

Examples:

// View all friend's private number fields (in pseudo code).
function example_private_number_access($op, $node, $field, $account) {
  if ($op == 'view') {
    if ($account->uid IS FRIEND OF $node->uid) return TRUE;
  }
}

// Enable users in role ID 3 to edit a private number field named field_secret.
function example_private_number_access($op, $node, $field, $account) {
  if ($op == 'edit' && $field['field_name'] == 'field_secret') {
    if (in_array(3, array_keys($account->roles))) return TRUE;
  }
}


Author
---------------
John Money
ossemble LLC.
http://ossemble.com
What can we ossemble for you? ossemble provides web application development and 
consulting for mission-critical Drupal projects.

Module development sponsored by LifeWire, a service of About.com, a part of 
The New York Times Company.
http://www.lifewire.com
