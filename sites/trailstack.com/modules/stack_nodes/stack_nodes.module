<?php

/**
 * @file
 * Stack node module
 *
 * This module allows users to add certain nodes to a stack list. Each node has an "add to stack" link at the bottom.
 * This stack list is visible to others viewing the site.
 */

define('STACK_NODES_NODE_TYPE',         'stack_nodes_node_type_');
define('STACK_NODES_PERM_ADD',          'create stack nodes');
define('STACK_NODES_PERM_VIEW',         'view stack nodes');
define('STACK_NODES_PERM_ADMINISTER',   'administer stack nodes');
define('STACK_NODES_BLOCK',             'stack_nodes_block_type_');
define('STACK_NODES_BLOCK_LIMIT',       'stack_nodes_block_limit');
define('STACK_NODES_BLOCK_TITLE',       'stack_nodes_block_title');
define('STACK_NODES_PAGE_LIMIT',        'stack_nodes_page_limit');
define('STACK_NODES_PROFILE_LIMIT',     'stack_nodes_profile_limit');
define('STACK_NODES_PAGE_TYPE',         'stack_nodes_page_type');
define('STACK_NODES_MENUS',             'stack_nodes_menu');
define('STACK_NODES_TEASER',            'stack_nodes_teaser');
define('STACK_NODES_LIST_EMPTY_TITLES', 'stack_nodes_list_empty_titles');
define('STACK_NODES_SHOWLINK',          'stack_nodes_showlink');

/**
 * Implementation of hook_help().
 */
function stack_nodes_help($path, $arg) {
  switch ($path) {
    case 'admin/help#stack_nodes':
    case 'admin/modules#description':
      return t('Allows users to manage a stack list of nodes.');
  }
}

function stack_nodes_perm() {
  return array(STACK_NODES_PERM_ADD, STACK_NODES_PERM_VIEW, STACK_NODES_PERM_ADMINISTER);
}

/**
 * Implementation of hook_menu().
 */
function stack_nodes_menu() {
  global $user;
  $items = array();

  $items['admin/settings/stack_nodes'] = array(
      'title'            => 'Stack',
      'description'      => 'Settings for stack nodes',
      'page callback'    => 'drupal_get_form',
      'page arguments'   => array('stack_nodes_settings'),
      'type'             => MENU_NORMAL_ITEM,
      'access arguments' => array(STACK_NODES_PERM_ADMINISTER),
  );

  $items['stack/add'] = array(
      'page callback'    => 'stack_nodes_add_page',
      'type'             => MENU_CALLBACK,
      'access arguments' => array(STACK_NODES_PERM_ADD),
  );

  $items['stack/delete'] = array(
      'page callback'    => 'stack_nodes_delete_page',
      'type'             => MENU_CALLBACK,
      'access arguments' => array(STACK_NODES_PERM_ADD),
  );

  $items['stack/view'] = array(
      'page callback'    => 'stack_nodes_view_page',
      'title'            => '',
      'type'             => MENU_CALLBACK,
      'access arguments' => array(STACK_NODES_PERM_VIEW),
  );

  if (variable_get(STACK_NODES_MENUS, 0)) {
    foreach (_node_types_natcasesort() as $type) {
      if (variable_get(STACK_NODES_NODE_TYPE . $type->type, 0)) {
        $items["stack/view/$type->type"] = array(
          'title'            => t('@type Stack', array('@type' => $type->name)),
          'page callback'    => 'stack_nodes_view_page',
          'type'             => MENU_NORMAL_ITEM,
          'access arguments' => array(STACK_NODES_PERM_VIEW),
        );
      }
    }
  }
  return $items;
}

/**
 * Implementation of hook_theme().
 */
function stack_nodes_theme() {
  return array(
    'stack_nodes_view_table' => array(
      'arguments' => array('list' => array(), 'type' => NULL, 'uid' => NULL),
    ),
    'stack_nodes_view_teasers' => array(
      'arguments' => array('list' => array(), 'type' => NULL, 'uid' => NULL),
    )
  );
}

/**
 * Implementation of hook_xmlrpc().
 */
function stack_nodes_xmlrpc() {
  $items = array();

  $items[] = array(
    'stack_nodes.add',
    'stack_nodes_add',
    array('boolean', 'int'),
    t('Add a stack node to the current user\'s list.')
  );
}

function _node_types_natcasesort() {
  static $node_types;

  if (!isset($node_types)) {
    $node_types = node_get_types();
    uasort($node_types, create_function('$a, $b', 'return strnatcasecmp($a->name, $b->name);'));
  }
  return $node_types;
}

/**
 * Implementation of hook_user().
 */
function stack_nodes_user($op, &$edit, &$account, $category = NULL) {
  switch ($op) {
    case 'view':
      $types = array();

      foreach (_node_types_natcasesort() as $type) {
        $types[$type->name] = $type->type;

        if (variable_get(STACK_NODES_NODE_TYPE . $type->type, 0)) {
          $stack[$type->name] = stack_nodes_get($account->uid, $type->type, variable_get(STACK_NODES_PROFILE_LIMIT, 5));
        }
      }

      if (!empty($stack)) {
        $account->content['stack_nodes_list'] = array(
          '#type'  => 'user_profile_category',
          '#title' => t('Stack'),
        );

        foreach($stack as $type => $stack) {
          $items = array();
          foreach($stack as $id => $object) {
            $items[] = l($object->title, "node/$object->nid");
          }

          if(empty($items)) {
            $items[] = 'No trails have been added to stack.';
          }

          $account->content['stack_nodes_list']['stack'][$type] = array(
            //'#type'  => 'user_profile_item',
            //'#title' => l($type, "stack/view/" . $types[$type] . "/" . $account->uid),
            '#value' => theme('item_list', $items),
          );

        }
      }
      break;

    case 'delete':
      stack_nodes_delete_stack($account->uid);
      break;
  }
}

/**
 * List of stack enabled content types with stack
 */
function theme_stack_nodes_user_page($account) {
  $fav_list = array();
  foreach (_node_types_natcasesort() as $type) {
    if (variable_get(STACK_NODES_NODE_TYPE . $type->type, 0)) {
      $uid = intval($account->uid);
      $stack = stack_nodes_get($uid, $type->type, variable_get(STACK_NODES_BLOCK_LIMIT, 5));
      $items = array();
      if (!empty($stack)) {
        foreach ($stack as $stack) {
          $items[] = l($stack->title, "node/$stack->nid");
        }
      }
      $fav_list[] = array(
        'title' => $items ? l($type->name, "stack/view/$account->uid/$type->type") : $type->name,
        'value' => theme('item_list', $items),
      );
    }
  }
  return array(t('stack') => $fav_list);
}

/**
 * Implementation of hook_nodeapi().
 */
function stack_nodes_nodeapi(&$node, $op, $teaser = NULL, $page = NULL) {
  switch ($op) {
    case 'delete':
      // Delete all stack entries of the node being deleted.
      db_query("DELETE FROM {stack_nodes} WHERE nid = %d", $node->nid);
      break;
  }
}

/**
 * Implementation of hook_block().
 */
function stack_nodes_block($op = 'list', $delta = 0, $edit = array()) {
  global $user;

  $node_types = _node_types_natcasesort();

  switch ($op) {
    case 'list':
      return array(array('info' => t('Stack nodes')));

    case 'view':
      if (user_access(STACK_NODES_PERM_VIEW)) {
        $block = array();
        $block['subject'] = variable_get(STACK_NODES_BLOCK_TITLE, t('stack'));
        $block['content'] = '';

        foreach ($node_types as $type) {
          if (variable_get(STACK_NODES_NODE_TYPE . $type->type, 0)) {
            $uid = intval($user->uid);
            $stack = stack_nodes_get($uid, $type->type, variable_get(STACK_NODES_BLOCK_LIMIT, 5));
            $items = array();
            if (!empty($stack)) {
              foreach ($stack as $stack) {
                $items[] = l($stack->title, "node/$stack->nid");
              }
              $block['content'] .= theme('item_list', $items, variable_get(STACK_NODES_BLOCK . $type->type, $type->name));
            }
            $sql = "SELECT COUNT(*) FROM {node} n INNER JOIN {stack_nodes} f USING(nid) WHERE n.type = '%s' AND f.uid = %d";
            $count = db_result(db_query($sql, $type->type, $user->uid));
            if ($count > variable_get(STACK_NODES_BLOCK_LIMIT, 5)) {
              $block['content'] .= "<div class=\"more\">\n";
              $block['content'] .= l(t('More Stack %types', array('%types' => variable_get(STACK_NODES_BLOCK . $type->type, $type->name))), "stack/view/$user->uid/$type->type");
              $block['content'] .= "</div>\n";
            }
          }
        }
      }
      return $block;

    case 'configure':
      $form = array();
      $form['titles'] = array(
        '#type' => 'fieldset',
        '#title' => t('Titles'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      $form['titles']['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Block title'),
        '#size' => 60,
        '#description' => t('This title will be displayed at the top of the block.'),
        '#default_value' => variable_get(STACK_NODES_BLOCK_TITLE, t('stack')),
      );
      $form['limit'] = array(
        '#type' => 'textfield',
        '#title' => t('Number of stack to display'),
        '#size' => 4,
        '#description' => t('Up to this many stack of each type of content will be displayed in the block. If there are no marked stack of a type, then that type won\'t show up.'),
        '#default_value' => variable_get(STACK_NODES_BLOCK_LIMIT, 5),
      );
      $form['titles']['subtitles'] = array(
        '#type' => 'fieldset',
        '#title' => t('Type subtitles'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      foreach ($node_types as $type) {
        if (variable_get(STACK_NODES_NODE_TYPE . $type->type, 0)) {
          $form['titles']['subtitles'][STACK_NODES_BLOCK . $type->type] = array(
            '#type' => 'textfield',
            '#title' => t('Subtitle for the %name content type', array('%name' => $type->name)),
            '#size' => 60,
            '#description' => t('Within the block, any links to content of the %name type will be categorized under this subtitle.', array('%name' => $name)),
            '#default_value' => variable_get(STACK_NODES_BLOCK . $type->type, $type->name),
          );
        }
      }
      return $form;

    case 'save':
      variable_set(STACK_NODES_BLOCK_TITLE, $edit['title']);
      variable_set(STACK_NODES_BLOCK_LIMIT, $edit['limit']);
      foreach ($node_types as $type) {
        if (variable_get(STACK_NODES_NODE_TYPE . $type->type, 0)) {
          variable_set(STACK_NODES_BLOCK . $type->type, $edit[STACK_NODES_BLOCK . $type->type]);
        }
      }
      break;

    default:
      break;
  }
}

/**
 * Implementation of hook_link().
 */
function stack_nodes_link($type, $node = NULL, $teaser = FALSE) {
  global $user;
  $links = array();

  if ($type == 'node') {
    if (variable_get(STACK_NODES_NODE_TYPE . $node->type, 0)) {
      if (user_access(STACK_NODES_PERM_ADD)) {
        if (!_stack_nodes_check($node->nid)) {
          if (!$teaser) {
            $links['add_to_stack'] = array(
              'title' => t('Add to Stack'), 
              'href' => 'stack/add/'. $node->nid
            );
          }
          elseif ($teaser && variable_get('stack_nodes_teaser', 0) == 1) {
            $links['add_to_stack'] = array(
              'title' => t('Add to Stack'), 
              'href' => 'stack/add/'. $node->nid
            );
          }
        }
        else {
          if (user_access(STACK_NODES_PERM_VIEW)) {
            if (!$teaser) {
              $links['in_stack'] = array(
                'title' => 'In Stack'
              );
              $links['remove_from_stack'] = array(
                'title' => t('Remove'), 
                'href' => 'stack/delete/'. $node->nid
              );
            }
            elseif ($teaser && variable_get('stack_nodes_teaser', 0) == 1) {
              $links['in_stack'] = array(
                'title' => 'In Stack'
              );
              $links['remove_from_stack'] = array(
                'title' => t('Remove'), 
                'href' => 'stack/delete/'. $node->nid
              );
            }
          }
        }
      }
    }
  }

  return $links;
}

/**
 * Settings page for this module.
 */
function stack_nodes_settings() {
  $set = 'page';
  $form[$set] = array(
    '#type' => 'fieldset',
    '#title' => t('stack Page Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form[$set][STACK_NODES_PAGE_LIMIT] = array(
    '#type' => 'textfield',
    '#title' => t('Stack Nodes Page Limit'),
    '#default_value' => variable_get(STACK_NODES_PAGE_LIMIT, 10),
    '#description' => t('How many items to display on a single page of marked stack.'),
  );
  $form[$set][STACK_NODES_PROFILE_LIMIT] = array(
    '#type' => 'textfield',
    '#title' => t('Stack Nodes Profile Limit'),
    '#default_value' => variable_get(STACK_NODES_PROFILE_LIMIT, 3),
    '#description' => t('How many items per type to display on the profile page.'),
  );
  $form[$set][STACK_NODES_PAGE_TYPE] = array(
    '#type' => 'select',
    '#title' => t('Type of Page Display for Stack Nodes'),
    '#options' => array(
      'table' => 'Table',
      'teasers' => 'Teasers',
    ),
    '#default_value' => variable_get(STACK_NODES_PAGE_TYPE, 'table'),
    '#description' => t('How should stack be displayed on the stack nodes page?'),
  );
  $form[$set][STACK_NODES_MENUS] = array(
    '#type' => 'checkbox',
    '#title' => t('Navigation menu items'),
    '#return_value' => 1,
    '#default_value' => variable_get(STACK_NODES_MENUS, 0),
    '#description' => t('Whether to show a menu item in the navigation block for each node type?'),
  );
  $form[$set][STACK_NODES_TEASER] = array(
    '#type' => 'checkbox',
    '#title' => t('Show in teaser'),
    '#return_value' => 1,
    '#default_value' => variable_get(STACK_NODES_TEASER, 0),
    '#description' => t('Whether to show in teaser or not?'),
  );
  $form[$set][STACK_NODES_LIST_EMPTY_TITLES] = array(
    '#type' => 'checkbox',
    '#title' => t('Display empty tables in user stack listing ?'),
    '#default_value' => variable_get(STACK_NODES_LIST_EMPTY_TITLES, 0),
    '#description' => t('By default, all node types are listed on the user stack nodes table view. In many cases, you may want to limit the lists to those that have stack nodes actually bookmarked. Selecting this option will allow all the lists to appear regardless of whether they contain nodes or not. Unselecting this option will restirct the list to only those actually containing nodes.'),
  );

  $form[$set][STACK_NODES_SHOWLINK] = array(
    '#type' => 'checkbox',
    '#title' => t('Display Stack Node options as links'),
    '#return_value' => 1,
    '#default_value' => variable_get(STACK_NODES_SHOWLINK, 1),
    '#description' => t('Whether to show link items in the node view?'),
  );

  $set = 'types';
  $form[$set] = array(
    '#type' => 'fieldset',
    '#title' => t('Enable stack for these node types'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  foreach (_node_types_natcasesort() as $type) {
    $form[$set][STACK_NODES_NODE_TYPE . $type->type] = array(
      '#type' => 'checkbox',
      '#title' => $type->name,
      '#return_value' => 1,
      '#default_value' => variable_get(STACK_NODES_NODE_TYPE . $type->type, 0),
    );
  }
  
  return system_settings_form($form);
}

/**
 * Add a stack node.
 */
function stack_nodes_add($nid) {
  global $user;

  $node = node_load($nid);
  if (!$node->nid) {
    return FALSE;
  }

  db_query("DELETE FROM {stack_nodes} WHERE nid = %d AND uid = %d", $nid, $user->uid);
  db_query("INSERT INTO {stack_nodes} (nid, uid, last) VALUES (%d, %d, %d)", $nid, $user->uid, time());

  /**
   * Invoke hook_stack_nodes(), which has the following parameters:
   * @param op
   * The operation being performed. Can be either 'add' or 'delete'.
   * @param node
   * The node object being added or deleted.
   */
  module_invoke_all('stack_nodes', 'add', $node);
  return TRUE;
}

/**
 * Delete a stack node.
 */
function stack_nodes_delete($nid) {
  global $user;

  $node = node_load($nid);
  if (!$node->nid) {
    return FALSE;
  }

  db_query("DELETE FROM {stack_nodes} WHERE nid = %d AND uid = %d", $nid, $user->uid);

  /**
   * Invoke hook_stack_nodes(), which has the following parameters:
   * @param op
   * The operation being performed. Can be either 'add' or 'delete'.
   * @param node
   * The node object being added or deleted.
   */
  module_invoke_all('stack_nodes', 'delete', $node);
  return TRUE;
}

/**
 * Select all the stack nodes a user has.
 * TODO why don't return all node if tpye are nul. So we can display all stack node on path stack/view/"uid"
 */
function stack_nodes_get($uid, $type = NULL, $limit = NULL) {
  if (is_null($limit)) {
    $limit = variable_get(STACK_NODES_PAGE_LIMIT, 10);
  }
  $row = array();
  if ($limit != 0 && $type && variable_get(STACK_NODES_NODE_TYPE . $type, 0)) {
    $sql = "SELECT n.nid, n.title, f.uid, f.last FROM {node} n INNER JOIN {stack_nodes} f ON n.nid = f.nid WHERE n.type = '%s' AND f.uid = %d ORDER by f.last DESC";
    $sql_count = "SELECT COUNT(*) FROM {node} n INNER JOIN {stack_nodes} f ON n.nid = f.nid WHERE n.type = '%s' AND f.uid = %d";

    $result = pager_query($sql, $limit, 0, $sql_count, $type, $uid);
    if ($result) {
      while ($data = db_fetch_object($result)) {
        $row[$data->nid] = $data;
      }
    }
  }
  return $row;
}

/**
 * Page to add a stack node.
 */
function stack_nodes_add_page() {
  $nid = arg(2);
  if (is_numeric($nid)) {
    $result = stack_nodes_add($nid);
    if ($result) {
      $node = node_load($nid);
      $type = node_get_types('name', $node);
      drupal_set_message(t('The !type was added to your stack.', array("!type" => $type)));
    }
    else {

    }
    drupal_goto('node/'. $nid);
  }

  drupal_not_found();
}

/**
 * Page to delete a stack node.
 */
function stack_nodes_delete_page() {
  global $user;
  $nid = arg(2);

  stack_nodes_delete($nid);
  $node = node_load($nid);
  $type = node_get_types('name', $node);
  drupal_set_message(t('The !type was removed from your stack.', array("!type" => $type)));
  drupal_goto("user/$user->uid");
}

/**
 * Page to display a user's stack list.
 */
function stack_nodes_view_page() {
  global $user;

  $a2 = arg(2);
  $a3 = arg(3);

  if (isset($a2)) {
    // argument 2 is specified
    if (is_numeric($a2)) {
      // It is a number, so it is a user's uid
      $uid = $a2;
    }
    else {
      // It is not a number, so a node type
      $type = $a2;
      if (isset($a3)) {
        // We have argument 2, it must be a uid
        $uid = $a3;
      }
      else {
        $uid = $user->uid;
      }
    }
  }
  else {
    // No arguments specified
    if ($user->uid) {
      // Use the uid for the currently logged in user
      $uid = $user->uid;
    }
    else {
      return t('No stack');
    }
  }

  return theme('stack_nodes_view_'. variable_get(STACK_NODES_PAGE_TYPE, 'table'), stack_nodes_get($uid, $type), $uid, $type);
}

/**
 * Delete stack nodes a user has.
 */
function stack_nodes_delete_stack($uid) {
  db_query("DELETE FROM {stack_nodes} WHERE uid = %d", $uid);
}

/**
 * Get all the stack nodes inner joining the users.
 * TODO: currently not being used.. do we need this?
 */
function _stack_nodes_get_users($nid) {
  $sql = "SELECT u.*, f.last FROM {users} u INNER JOIN {stack_nodes} f USING(uid) WHERE f.nid = %d ORDER by f.last DESC";
  $result = db_query($sql, $nid);
  $row = array();
  while ($data = db_fetch_object($result)) {
    $row[$data->uid] = $data;
  }

  return $row;
}

/**
 * Check if a user already has a node in their stack list.
 */
function _stack_nodes_check($nid) {
  global $user;
  $sql = "SELECT COUNT(*) FROM {stack_nodes} WHERE uid = %d AND nid = %d";
  return db_result(db_query($sql, $user->uid, $nid));
}

/**
 * TODO: This is not being used.. do we need it?
 */
function theme_stack_nodes_view_teasers($list = array(), $uid = NULL, $type = NULL) {
  $type_desc = variable_get(STACK_NODES_BLOCK . $type, $type);

  $user = user_load(array('uid' => $uid));

  $output .= '<div id="stack_page">';

  if (!$type) {
    $output .= t('Please select stack type.');
  }
  else {
    if (!empty($list)) {
      $output .= '<h2>'. t('Trail Stack  for !user', array('!type' => $type_desc, '!user' => theme('username', $user))) ."</h2>\n";
      $iterator = 0;
      foreach ($list as $nid => $data) {
        $iterator++; 
        $node = node_load($nid);
        $output .= '<div class="stack_row stack_row_' . $iterator . '">' . node_view($node, TRUE). '</div>';
      }
    }
    else {
      $output .= t('You do not have any !types in your Stack yet.', array('!type' => $type_desc));
    }
  }
  $output .= theme('pager');

  $output .= '</div>';

  return $output;
}

/**
 * Table which displays stack node lists.
 */
function theme_stack_nodes_view_table($list = array(), $uid = NULL, $type = NULL) {
  global $user;
  $account = user_load(array('uid' => $uid));
  $header = array(t('Title'), t('Added'), t('Operations'));

  $rows = array();
  if (isset($list)) {
    foreach ($list as $nid => $data) {
      $title = array(l($data->title, "node/$nid"), format_date($data->last, 'custom', 'Y-m-d H:i'));
      if ($user->uid == $data->uid || $user->uid == 1) {
        $delete = array(l(t('delete'), "stack_nodes/delete/$nid"));
        $result = array_merge($title, $delete);
      }
      else {
        $result = $title;
      }
      $rows[] = array('data' => $result);
    }
  }

  if (variable_get(STACK_NODES_LIST_EMPTY_TITLES, 0) || !empty($rows)) {
    $output .= '<h2>'. t('Trail Stack for !user', array(
      '!type' => variable_get(STACK_NODES_BLOCK . $type, node_get_types('name', $type)),
      '!user' => theme('username', $account))) ."</h2>\n";
    $output .= theme('table', $header, $rows);
    $output .= theme('pager');
  }

  return $output;
}

/**
 * Implementation of hook_views_api().
 */
function stack_nodes_views_api() {
  return array(
    'api' => 2,
  );
}
