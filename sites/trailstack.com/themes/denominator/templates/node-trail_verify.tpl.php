<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix">
	<div><strong>Trail:</strong> <?php print $node->field_trail_verify_trail[0]['view'] ?></div>
	<div><strong>Marker Color:</strong> <?php print $node->field_trail_marker_color[0]['view'] ?></div>
	<div><strong>Length:</strong> <?php print $node->field_trail_length[0]['view'] ?></div>
	<div><strong>Elevation Gain:</strong> <?php print $node->field_trail_elevation[0]['view'] ?></div>
	<div><strong>Difficulty:</strong> <?php print $node->field_trail_difficulty[0]['view'] ?></div>
	<div><strong>Seclusion:</strong> <?php print $node->field_trail_seclusion[0]['view'] ?></div>
	<div><strong>Park:</strong> <?php $terms = taxonomy_node_get_terms_by_vocabulary($node, 3); if ($terms) { foreach ($terms as $term) { print $term->name;}}?></div>
	<div><strong>Activities:</strong> <?php $activities = taxonomy_node_get_terms_by_vocabulary($node,1); if ($activities) { foreach ($activities as $activity) {print $activity->name . ', ';}} ?></div>
	<div><strong>Location:</strong> <?php print $node->locations[0]['street'] ?>, <?php print $node->locations[0]['city'] ?>, <?php print $node->locations[0]['province'] ?></div>
	<div class="seperator"></div>
	<div><strong>Reviewd by Staff:</strong> <?php print $node->field_trail_verify_reviewed[0]['view'] ?></div>
	<div><strong>Original Correct:</strong> 
	<?php 
	if ($node->field_trail_verify_correct[0]['safe'] == 1) {
		print 'Yes';
	} else {
		print 'No';
	}
	?></div>
</div><!-- /.node -->
