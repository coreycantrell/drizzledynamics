<?php
// Grabs the firsts image path and sets $imagePath.
//print('<pre>'.print_r($node,1).'</pre>');
$imagePath = $node->field_image_cache['0']['filepath'];
global $base_path;
?>
<div id="node">
	<div class="productnode">
		<div class="product-left-col">
			<div class="imagesholder">
				<div class="mainimage">
					<a href="<?php print $base_path . $imagePath; ?>" class="thickbox">
						<?php print theme('imagecache', 'product', $imagePath, '', ''); ?>
					</a>
				</div>
			</div>
			<div id="product-other-images">
				<div class="product-other-images-text">
					<a href="<?php print $base_path . $imagePath; ?>" class="thicbox"><?php print t("View Larger Image"); ?></a>
				</div>
			</div>
		</div>
		<div class="product-right-col">
			<div class="productdesc">
				<?php print $node->content['body']['#value'];  ?>
			</div>
			<div class="orderOptions">
				<div class="price">
					Price: <?php print uc_currency_format($node->sell_price); ?>
				</div>
				<div id="cartButtons">
					<?php print $node->content['add_to_cart']["#value"]; ?>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>