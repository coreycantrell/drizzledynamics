// $Id: README.txt,v 1.1.2.1 2008/04/07 01:25:13 mfb Exp $

One-time login links is a small utility module which generates a CSV of 
one-time login links for each user.

Once enabled, this module places a tab at Administer >> User management 
>> Users or can be accessed directly at
admin/user/user/one_time_login.  This will prompt you to download the 
CSV file which will have the following columns in this order:

[ user id | user name | user email address | one-time login url | 
  expiration date (if any) of the one-time login url ]

One-time login links have an expiration date only if the user has 
previously logged in to the account.
