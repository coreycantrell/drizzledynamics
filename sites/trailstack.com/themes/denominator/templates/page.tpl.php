<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" xmlns:fb="http://ogp.me/ns/fb#" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
	<script src="//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
	<script>
	  WebFont.load({
	    google: {
	      	families: ['Sanchez']
	    },
	    custom: {
		    families: ['icomoon'],
		    urls: ['/sites/trailstack.com/themes/denominator/css/icomoon.css']
		 }
	  });
	</script>
	<title>
	<?php
	if ($node->type == 'trail_verify') {
		print 'Verification for ' . $node->field_trail_verify_trail[0]['safe']['title'] . ' | ' . $site_name;
	} elseif ($node->type == 'trail') {
		print check_plain($title) . ' | Trail | ' . $site_name;
	} elseif ($node->type == 'park') {
		print check_plain($title) . ' | Park | ' . $site_name;
	} elseif ($node->type == 'resource') {
		print check_plain($title) . ' | Resource | ' . $site_name;
	} elseif ($is_front) {
		print 'Welcome to TrailStack.com' . ' | ' . $site_name;
	} else {
		print $head_title;
	}
	?>
	</title>
	<?php print $head; ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="apple-touch-icon" href="/sites/trailstack.com/themes/denominator/mark-icons/72.png" />
	<link rel="apple-touch-icon" sizes="57x57" href="/sites/trailstack.com/themes/denominator/mark-icons/57.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="/sites/trailstack.com/themes/denominator/mark-icons/72.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="/sites/trailstack.com/themes/denominator/mark-icons/114.png" />
	<?php print $styles; ?>
	<link type="text/css" rel="stylesheet" media="all" href="/sites/trailstack.com/themes/denominator/css/retina.css">
	<link href="https://s3.amazonaws.com/scripts.drizzledynamics.com/formalize/formalize.css" rel="stylesheet" type="text/css" />
</head>
<body class="<?php print $classes; ?>">
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=187406787976223";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<script src="//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
    <script>
        WebFont.load({
            google: {
                families: ['Lato', 'Muli']
            }
        });
    </script>
	<div id="header-wrapper">
		<div id="header">
    		<div class="section clearfix">
				<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"></a>
				<?php if (!user_is_logged_in()): ?>
					<?php if ($header_hybridauth): ?>
					<div id="hybridauth">
						<div class="registerLink">
							<div class="first">Sign Up</div>
							<div class="last">or <a href="/user">Log in</a></div>
						</div>
						<?php print $header_hybridauth; ?>
					</div>
					<?php endif; ?>
				<?php else: ?>
					<div id="countContainer">
					<?php global $user; $uid = $user->uid;
						$count = db_result(db_query("SELECT COUNT(*) FROM {stack_nodes} WHERE uid = %d", $user->uid));
						echo "<a href='/stack/view/trail/$uid' id='stackCount'>$count</a>";
						if ($count == "1") {
							echo " trail ";
						} else {
							echo " trails ";
						};
						echo "in your stack <span aria-hidden='true' class='icon-stack'></span>";
					?>
					</div>
				<?php endif; ?>
				<span id="tagline">Your Trails. Your Stack.</span>
    			<?php print $header; ?>
			</div>
		</div><!-- /.section, /#header -->
	</div>	
	<?php if ($nav): ?>
	<div id="nav-wrapper">
		<div id="nav">
			<div class="section clearfix">
				<div id="userUtility">
					<?php global $user; if ($user->uid): ?>
					<?php print theme('imagecache', 'user_image_small', $user->picture, $user->name, $user->name) ?> <span aria-hidden="true" class="icon-utilitymenu"/></span>
					<div id="userUtilityMenu" style="display:none">
						<div>
							<strong><?php global $user; print $user->name ?></strong><br/>
							<a href="/user">Profile</a><br/>
							<a href="/user/<?php print $user->uid ?>/edit">Account</a><br/>
							<a href="/stack/view/trail">My Stack</a><br/>
							<a href="/friends">Friends</a><br/>
							<?php if (in_array('manager', array_values($user->roles))) : ?>
							<span aria-hidden="true" class="icon-shield gold"/></span> <a href="/apanel">aPanel</a><br/>
							<?php endif; ?>
							<?php if (in_array('contributor', array_values($user->roles)) || in_array('manager', array_values($user->roles))) : ?>
							<span aria-hidden="true" class="icon-shield<?php if (in_array('manager', array_values($user->roles))) : ?> gold<?php endif; ?>"/></span> <a href="/staff-help">Staff Help</a><br/>
							<?php endif; ?>
							<?php print l("Logout","logout"); ?>
						</div>
					</div>
					<?php endif; ?>
				</div>
				<?php print $nav; ?>
			</div>
		</div>
	</div>
	<?php endif; ?>
	
	<div id="page-wrapper"><div id="page">

		<?php if ($is_front): ?>
		<?php print $messages; ?>
		<?php endif; ?>
	
		<?php if ($pre_content): ?>
		<div id="pre-content-wrapper">
			<div id="pre-content"><div class="section clearfix">
				<?php print $pre_content; ?>
			</div></div><!-- /.section, /#pre-content" -->
		</div>
		<?php endif; ?>
	
		<div id="main-wrapper"><div id="main" class="clearfix">

			<div id="content" class="column"><div class="section">

				<?php print $highlight; ?>

				<?php print $breadcrumb; ?>
				
				<?php if ($title): ?>
				  <h1 class="title"><?php print $title; ?></h1>
				<?php endif; ?>
				
				<?php if (!$is_front): ?>
				<?php print $messages; ?>
				<?php endif; ?>
				
				<?php if ($tabs): ?>
				  <div class="tabs"><?php print $tabs; ?></div>
				<?php endif; ?>
				
				<?php print $help; ?>

				<?php print $content_top; ?>

				<div id="content-area">
				  <?php print $content; ?>
				</div>

				<?php print $content_bottom; ?>

			</div></div><!-- /.section, /#content -->

		  <?php print $sidebar_first; ?>

		  <?php print $sidebar_second; ?>

		</div></div><!-- /#main, /#main-wrapper -->

		<?php if ($footer || $footer_message): ?>
			<div id="footer"><div class="section">
				<?php if ($footer_message): ?>
					<div id="footer-message"><?php print $footer_message; ?></div>
				<?php endif; ?>
			<?php print $footer; ?>
			</div></div><!-- /.section, /#footer -->
		<?php endif; ?>
	
	</div></div><!-- /#page, /#page-wrapper -->
	
	<?php print $page_closure; ?>
	
	<?php global $user; if ($user->uid): ?>
	<div id="feedbackDialog" style="display:none";><?php print $feedback_dialog; ?></div>
	<div id="inviteDialog" style="display:none";><?php print $invite_dialog; ?></div>
	<?php endif; ?>

	<?php print $scripts; ?>
	<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/jquery.bt.js"></script>
	<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/jquery.cycle.min.js"></script>
	<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/formalize/jquery.formalize.js"></script>
    <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/dzdy.required.js"></script>
	<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/usmap/lib/raphael.js"></script>
	<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/usmap/jquery.usmap.js"></script>
	<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/jquery.cookie.js"></script>
	<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/jquery-ui.accordion.js"></script>

	<?php print $closure; ?>

</body>
</html>