<?php

/**
 * @file
 * Admin include file.
 */

/**
 * Displays the form for the standard settings tab.
 *
 * @return
 *   array A structured array for use with Forms API.
 */
function linkintel_admin_settings() {
 
  $settings = linkintel_load_page_settings();
  $options = linkintel_get_priority_options();
  $form['priority'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page priority options'),
    '#collapsible' => TRUE,
    //'#description' => t('Sets the default options for generating synced request. These settings can be changed per content type and per node.'),
  ); 
  $form['priority']['linkintel_priority'] = array(
    '#type' => 'select',
    '#title' => t('Page priority default'), 
    '#options' => $options,
    '#default_value' => variable_get('linkintel_priority', LINKINTEL_PRIORITY_DEFAULT),
    '#description' => t('Use to set the default page priority. The higher the priority relative to other pages, the more likely a page will be backlinked to. If unsure, just set to "Standard".'),
  ); 
  $options = array(
    'basic' => t('Basic'),
    'expanded' => t('Expanded'),
  );
  $form['priority']['linkintel_priority_granularity'] = array(
    '#type' => 'select',
    '#title' => t('Priority gruanularity'), 
    '#options' => $options,
    '#default_value' => variable_get('linkintel_priority_granularity', 'basic'),
    '#description' => t('Enables additional granularity for priority settings. If unsure, just set to "Basic".'),
  );   
  $form['sync'] = array(
    '#type' => 'fieldset',
    '#title' => t('Request sync defaults'),
    '#collapsible' => TRUE,
    '#description' => t('Sets the default options for generating synced request. These settings can be changed per content type and per node.'),
  ); 
  $form['sync'] = array_merge($form['sync'], linkintel_get_request_sync_subform($settings));

  $form['autogen'] = array(
    '#type' => 'fieldset',
    '#title' => t('Link autogen defaults'),
    '#collapsible' => TRUE,
    //'#collapsed' => FALSE,
    '#description' => t('Sets the default options for auto generating (autogen) links. These settings can be changed per content type and per node.')
  );
  $form['autogen'] = array_merge($form['autogen'], linkintel_get_autogen_subform($settings));

  $form['autogen_params'] = array(
    '#type' => 'fieldset',
    '#title' => t('Autogen tuning'),
    '#collapsible' => TRUE,
    //'#collapsed' => FALSE,
    '#description' => t('These settings allow you to tune the auto generated (autogen) link scoring.')
  );
  if(module_exists('porterstemmer')) {
    $form['autogen_params']['linkintel_autogen_params_porterstemmer_enable'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable keyword stemming'),
      '#return_value' => 1,
      '#default_value' => variable_get('linkintel_autogen_params_porterstemmer_enable', 1),
      '#description' => t('If enabled, both exact match and keyword stems will be returned. This allows multiple varients of a keyword to be found. E.g. brake would match brakes and braking'),
      //'#field_suffix' => t('%'),
      '#size' => 3,
    );
    $form['autogen_params']['linkintel_autogen_params_porterstemmer_penalty'] = array(
      '#type' => 'textfield',
      '#title' => t('Keyword stem penalty'),
      '#default_value' => variable_get('linkintel_autogen_params_porterstemmer_penalty', 10),
      '#description' => t('Gives reduces the score of stemmed keyword matches. Set between 0 - 100. 0 will no reduction, 100 is maximum reduction.'),
      //'#field_suffix' => t('%'),
      '#size' => 3,
    );    
  }
  else {
    
  }
  $form['autogen_params']['linkintel_autogen_params_word_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Word count bonus'),
    '#default_value' => variable_get('linkintel_autogen_params_word_count', 0),
    '#description' => t('Gives keyword phrases with more words a boost. Set between 0 - 100. 0 will produce no boost, 100 is maximum boost.'),
    //'#field_suffix' => t('%'),
    '#size' => 3,
  );  
  $form['autogen_params']['linkintel_autogen_params_word_count_sort'] = array(
    '#type' => 'checkbox',
    '#title' => t('Word count sort'),
    '#return_value' => 1,
    '#default_value' => variable_get('linkintel_autogen_params_word_count_sort', 1),
    '#description' => t('This will sort keyword phrases of equivalent scores by descending word count.'),
    //'#field_suffix' => t('%'),
    '#size' => 3,
  );  
  $form['autogen_params']['linkintel_autogen_params_random'] = array(
    '#type' => 'textfield',
    '#title' => t('Randomness'),
    '#default_value' => variable_get('linkintel_autogen_params_random', 0),
    '#description' => t('Enables randomness in link selection. Set between 0 - 100. 0 will produce no randomness, 100 is completely random.'),
    //'#field_suffix' => t('%'),
    '#size' => 3,
  );  
  
  $form['seeding'] = array(
    '#type' => 'fieldset',
    '#title' => t('Link suggestions keyword seeding'),
    '#collapsible' => TRUE,
    '#description' => t('These settings enable you to seed keywords for a node on the link suggestions form.')
  );
  if(module_exists('taxonomy')) {
    $vocabs = taxonomy_get_vocabularies();
  }
  $options = array();
  if(!empty($vocabs)) {
    foreach ($vocabs AS $vid => $v) {
      //if ($v->tags) {
        $options[$vid] = $v->name;
      //}
    }
  }
  $form['seeding']['linkintel_suggestions_seed_vocabs'] = array(    
    '#type' => 'checkboxes',    
    '#title' => t('Seed vocabularies'),   
    '#options' => $options,
    '#default_value' => variable_get('linkintel_suggestions_seed_vocabs', array()),     
    '#description' => t('Select vocabularies to gather terms for seeding the link suggestions tool.')     
  );
  
  if(module_exists('alchemy')) {
    
    $form['seeding']['alchemy'] = array(
      '#type' => 'fieldset',
      '#title' => t('Alchemy'),
      '#description' => t('Settings for using Alchemy keyword extraction to seed suggestions.')
    );
    
    $form['seeding']['alchemy']['linkintel_suggestions_seed_alchemy_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Alchemy as a seed source'), 
      '#return_value' => 1,
      '#default_value' => variable_get('linkintel_suggestions_seed_alchemy_enabled', 1),
    );
    
    $options = alchemy_get_types();
    $form['seeding']['alchemy']['linkintel_suggestions_seed_alchemy_types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Default data types'), 
      '#options' => $options,
      '#default_value' => variable_get('linkintel_suggestions_seed_alchemy_types', alchemy_get_types(TRUE)),
      '#description' => t('Alchemy provides an array of data it can extract from content. Use the above checkboxes to select which ones you want the content analyzer to perform by default.'),
    );
    
    $form['seeding']['alchemy']['linkintel_suggestions_seed_alchemy_threshold'] = array(
      '#type' => 'textfield',
      '#title' => t('Relevancy threshold'),
      '#default_value' => variable_get('linkintel_suggestions_seed_alchemy_threshold', 50),
      '#description' => t('Use this to specify the minimum relevance a phrase must have in order to be included. Set between 0 - 100.'),
      '#field_suffix' => t('%'),
      '#size' => 3,
    );
    
    $form['seeding']['alchemy']['linkintel_suggestions_seed_alchemy_usecache'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use cached data if available.'), 
      '#default_value' => variable_get('linkintel_suggestions_seed_alchemy_usecache', ''),
      '#description' => t('The responses from Alchemy are cached. If you want to enable fetching data from cache rather than fetching from the Alchemy API, check the above box.'),
      '#prefix' => '<div id="alchemy_contentanalysis_cache">',
      '#suffix' => '</div>',  
    );
  }   
  
 
  
  $form['bulkupdate'] = array(
    '#type' => 'fieldset',
    '#title' => t('Bulk update'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('Request sync and link autogen functions are executed on node save. You can also set requests and links to automatically bulk update on cron runs using the below settings.')
  ); 
  $form['bulkupdate']['linkintel_bulkupdate_enable'] = array(    
    '#type' => 'checkbox',
    '#title' => t('Enable bulk update'), 
    '#return_value' => 1,
    '#default_value' => variable_get('linkintel_bulkupdate_enable', 1),
    '#description' => t('Check if you want to bulk update request sync and/or link autogen.'),   
  ); 
  $form['bulkupdate']['linkintel_bulkupdate_requests_sync_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Request sync node limit'),
    '#default_value' => variable_get('linkintel_bulkupdate_requests_sync_limit', 100),
    '#description' => t('The maximum number of nodes that will have requests auto synced per cron run. Leave blank to disable request auto update.'),
    '#field_suffix' => t(' nodes per cron run'),
    '#size' => 3,
  );
  $form['bulkupdate']['linkintel_bulkupdate_all_requests_before_links'] = array(    
    '#type' => 'checkbox',
    '#title' => t('Do not auto update links until all requests have been updated.'), 
    '#return_value' => 1,
    '#default_value' => variable_get('linkintel_bulkupdate_all_requests_before_links', 1),
    '#description' => t('Check if assure all nodes have been request synced before generating any links.'),   
  );
  $form['bulkupdate']['linkintel_bulkupdate_autogen_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Link autogen node limit'),
    '#default_value' => variable_get('linkintel_bulkupdate_autogen_limit', 10),
    '#description' => t('The maximum number of nodes that will have links auto generated per cron run. Leave blank to disable link bulk auto updating.'),
    '#field_suffix' => t(' nodes per cron run'),
    '#size' => 3,
  );
  $form['bulkupdate']['linkintel_bulkupdate_autogen_expire'] = array(
    '#type' => 'textfield',
    '#title' => t('Refresh auto generated links after'),
    '#default_value' => variable_get('linkintel_bulkupdate_autogen_expire', 90),
    '#description' => t('Use this setting if you want to re-auto generated links after a specified number of days.'),
    '#field_suffix' => t(' days'),
    '#size' => 3,
  ); 
  $form['bulkupdate']['linkintel_bulkupdate_run'] = array(
    '#item' => 'textfield',
    //'#title' => t('Run bulk update'),
    '#value' => '<div>' 
      . l(t('Manually run bulk update'), 'linkintel/bulkupdate', array('attributes' => array('target' => 'linkintel_bulkupdate')))  
      . ' (' . l(t('Clear and bulk update'), 'linkintel/bulkupdate', array('query' => 'clear=1','attributes' => array('target' => 'linkintel_bulkupdate'))) . ')'
      . '<br /><span style="font-size: .8em;"> (' . l(t('set requests to refresh'), 'linkintel/bulkupdate/clear_requests', array('attributes' => array('target' => '_self')))  
      . ' | ' . l(t('set links to refresh'), 'linkintel/bulkupdate/clear_links', array('attributes' => array('target' => '_self')))  
      . ")</span></div>\n",
  ); 
  if(module_exists('porterstemmer')) {
    $form['bulkupdate']['linkintel_bulkupdate_porterstemmer_run'] = array(
      '#item' => 'textfield',
      //'#title' => t('Run bulk update'),
      '#value' => '<div>' . l(t('Manually run stemmer bulk update'), 'linkintel/bulkupdate/porterstemmer', array('attributes' => array('target' => 'linkintel_bulkupdate'))) . "</div>\n", 
    );     
  }
  
  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['advanced']['linkintel_nolinkintel_tags'] = array(
    '#type' => 'textfield',
    '#title' => t('Nolinkintel tags'),
    '#default_value' => variable_get('linkintel_nolinkintel_tags', LINKINTEL_NOLINKINTEL_TAGS),
    '#description' => t('Specify any HTML tags you want Link Intelligence to ignore when auto generating links. Use commas to seperate tags. Note, the <em>a</em> tag is automatically filtered.'),
  ); 

//dsm($form);
  return system_settings_form($form);   
}