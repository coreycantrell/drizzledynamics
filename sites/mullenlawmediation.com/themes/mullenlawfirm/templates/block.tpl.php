
<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>">
  <?php if ($title): ?>
    <h2 class="title"><?php print $title; ?></h2>
  <?php endif; ?>

<div class="content"><?php print $block->content; ?>
  <?php if ($block->module == "block"):?>
      <?php if (user_access('administer blocks')) :?>
      <div class="block-edit-link"><a href='/admin/build/block/configure/block/<?php print $block->delta;?>'>Edit</a></div>
      <?php endif; ?>
  <?php endif; ?>
  </div>

  <?php print $edit_links; ?>
</div><!-- /.block -->
