<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Babington Mills | Coming Soon</title>
	<link rel="shortcut icon" href="/sites/babingtonmills.com/themes/babingtonmills/favicon.ico" type="image/x-icon" />
	<link type="text/css" rel="stylesheet" href="/sites/babingtonmills.com/themes/babingtonmills/css/maintenance-style.css" />
	</head>
<body>
	<div id="container">
		<div id="logo-container">
			<img alt="Coming Soon" src="/sites/babingtonmills.com/themes/babingtonmills/images/comingsoon.png"/><br/>
			<h1> We're currently updating our website software. Please check back later.</h1><br/>
		</div>
		<div id="social-container">
		Copyright &copy;2012. <a href="http://www.kevinbabington.com">Kevin Babington LLC</a>. All Rights Reserved.
		</div>
	</div> 
</body>
</html>