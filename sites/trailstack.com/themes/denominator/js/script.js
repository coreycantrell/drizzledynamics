﻿function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

$(document).ready(function(){ 

	//First time user cookie to show dialog
	//var firstVisit = $.cookie('firstVisit');
	//if (firstVisit == null){
    //	$('#openWelcomeDialog').click();
	//	$.cookie("firstVisit", "true");
    //}

    // Handles showing user utility menu
    function showUserMenu() {
        $("#userUtilityMenu").show();
        $("#userUtility").addClass('menuOpen');
    }

    function hideUserMenu() {
        $("#userUtilityMenu").hide();
        $("#userUtility").removeClass('menuOpen');
    }

    $("#userUtility").hoverIntent({
        over: showUserMenu,
        out: hideUserMenu,
        interval: 20,
        timeout: 100
    });
	
    // Handles showing corrdinates
    function showCoordsMenu() {
        $("#coordsMenu").show();
        $(".icon-globe").addClass('menuOpen');
    }

    function hideCoordsMenu() {
        $("#coordsMenu").hide();
        $(".icon-globe").removeClass('menuOpen');
    }

    $("#coordsContainer").hoverIntent({
        over: showCoordsMenu,
        out: hideCoordsMenu,
        interval: 20,
        timeout: 100
    });
	
    // Handles showing user utility menu
    function showmoreMenu() {
        $("#moreMenu").show();
        $(".menuTarget").addClass('menuOpen');
    }

    function hidemoreMenu() {
        $("#moreMenu").hide();
        $(".menuTarget").removeClass('menuOpen');
    }

    $("#moreContainer").hoverIntent({
        over: showmoreMenu,
        out: hidemoreMenu,
        interval: 20,
        timeout: 100
    });

    // Handles register button click on home page
    // Adds entered email as URL parameter for next page
	if($('#user-register').length) {
		var email = getUrlVars()["email"];
		$('#edit-mail').val(email);
	}
	$('#beginRegister').click(function(){
		var email = $("#email").val();
		window.location = "user/register?email=" + email;
	});

	// Initialize front page slideshow
	if($('body').hasClass('front')) {
		$('#block-views-Trails-block_1 .item-list ul').cycle({ 
			fx:      'fade', 
			speed:    'fast', 
			timeout:  5000,
			pager:  '#trailNav',
			prev:    '#prev',
			next:    '#next',
			pause:   1,
			activePagerClass: 'selected',
			pagerAnchorBuilder: function(idx, slide) { 
				return '<a href="#" aria-hidden="true" class="icon-bullet"></a>'; 
			} 
		});
	}

	// Initialize front page background rotation
	// Set image names
	//var images = ['1.png', '2.png', '3.png'];
	// Path for background image folder & do the math
	//$('#pre-content').css({'background-image': 'url(/sites/trailstack.com/themes/denominator/images/precontent-bg/' + images[Math.floor(Math.random() * images.length)] + ')'});
	
	// Redirect for front page state select
	$('#front-state-select').change(function() {
		// set the window's location property to the value of the option the user has selected
		var state = $(this).val();
		window.location = '/trails/state/' + state;
	});
	
	// Insert icons into trail profile links
	$('.in_stack').prepend('<span aria-hidden="true" class="icon-stack"></span>');
	$('.add_to_stack a').prepend('<span aria-hidden="true" class="icon-stack"></span>&nbsp;&nbsp;');
	$('.comment_add a').prepend('<span aria-hidden="true" class="icon-comments"></span>&nbsp;&nbsp;');
	$('.comment_comments').prepend('<span aria-hidden="true" class="icon-comments"></span>&nbsp;&nbsp;');

	// Insert icons into nav tabs
	$('<span aria-hidden="true" class="icon-arrow-down"></span>').insertAfter('li.menuparent .nolink').eq(0);
	
	// BeautyTips for icons
	var btOptions = {
		positions: ['bottom', 'top', 'right', 'left'],
		fill: '#26537A',
		padding: '5px 10px',
		strokeWidth: 0,
		strokeStyle: "#B9B5AE",
		cornerRadius: 5,
		spikeGirth: 10,
		spikeLength: 5,
		overlap: -1,
		cssStyles: {
			fontSize: '11pt',
			textAlign: 'left',
			width: 'auto',
			color: '#FFFFFF',
			whiteSpace: 'noWrap'
		}
	}
	
	$("h1 .icon-checkmark").bt($.extend(btOptions));
	$(".trail-badges .icon-stack").bt($.extend(btOptions));
	$(".trail-badges .icon-comments").bt($.extend(btOptions));
	$(".trail-badges .icon-notification").bt($.extend(btOptions));
	$(".park-marker").bt($.extend(btOptions));
	
	// Trail verification form functionality
	$('#edit-field-trail-verify-correct-value-1').click(function() {
		if($(this).is(':checked')) {
			$('input[type=text]').attr('readonly', 'readonly');
			$('input[type=number]').attr('readonly', 'readonly');
			$('select').attr('disabled', 'disabled');
		} else {
			$('input[type=text]').attr('readonly', null);
			$('input[type=number]').attr('readonly', null);
			$('select').attr('disabled', null);
		}
	});	
	$('#edit-field-trail-verify-correct-value-1').attr('checked', false);
	$('#edit-field-trail-verify-trail-0-nid-nid').attr('readonly', 'readonly').removeClass('form-autocomplete');
	$('.page-node-add-trail-verify').find('label').eq(0).hide();

	// Insert text before social fields
	$('<span>http://www.facebook.com/ </span>').insertBefore('#edit-profile-facebook');
	$('<span>http://www.twitter.com/ </span>').insertBefore('#edit-profile-twitter');

	$("textarea").keydown(function(e) {
	    if(e.keyCode === 9) { // tab was pressed
	        // get caret position/selection
	        var start = this.selectionStart;
	        var end = this.selectionEnd;

	        var $this = $(this);
	        var value = $this.val();

	        // set textarea value to: text before caret + tab + text after caret
	        $this.val(value.substring(0, start)
	                    + "\t"
	                    + value.substring(end));

	        // put caret at right position again (add one for the tab)
	        this.selectionStart = this.selectionEnd = start + 1;

	        // prevent the focus lose
	        e.preventDefault();
	    }
	});

	// Initialize accordion on changelog
	$( "#accordion" ).accordion({ 
		active: 0,
		collapsible: true,
		autoHeight: false,
		icons: { 
			"header": "icon-plus", 
			"headerSelected": "icon-minus" 
		}
	});
	
});
