<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>">
  <?php if ($title): ?>
    <h2 class="title"><?php print $title; ?></h2>
  <?php endif; ?>

<div class="content">
	<?php print $block->content; ?>
</div>

</div><!-- /.block -->