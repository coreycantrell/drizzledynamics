<?php global $user; ?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix">
<?php if ($teaser): ?>
	<div class="trail-name">
		<a href="<?php print '/' . drupal_lookup_path('alias',"node/".$node->nid); ?>">
		<?php print check_plain($title); ?>, <?php print $node->field_trail_park[0]['safe']['title'] ?>
		</a>
	</div>
	<div class="trail-meta">
		<?php print $node->location['city'] ?>, <?php print $node->location['province'] ?> <?php print $node->field_trail_length[0]['view'] ?>
	</div>
	<div class="trail-image">
		<a href="../<?php print drupal_lookup_path('alias',"node/".$node->nid); ?>"><?php print theme('imagecache', 'trail-teaser', $node->field_trail_primary_photo[0]['filepath']); ?></a>
	</div>
	<div class="trail-badges">
		<div class="trail-meta-stack">
			<?php if ($node->links['add_to_stack']['title'] != "Add to Stack"): ?>
			<span aria-hidden="true" class="icon-stack" title="This trail is in your stack"></span>
			<?php else: ?>
			<span aria-hidden="true" class="icon-stack not" title="This trail is not in your stack"></span> 
			<?php endif; ?>
		</div>
		<?php if ($node->comment): ?>
		<div class="trail-meta-comment">
			<?php if ($node->comment_count > 1 && $node->comment): ?>
			<span aria-hidden="true" class="icon-comments" title="This trail has <?php print $node->comment_count ?> comments"/></span> <?php print $node->comment_count ?>
			<?php else: ?>
			<span aria-hidden="true" class="icon-comments none" title="This trail does not have any comments"/></span>
			<?php endif; ?>
		</div>
		<?php endif ?>
		<div class="trail-meta-verify">
			<?php if ($node->field_trail_status[0]['value'] == "1"): ?>
				<span aria-hidden="true" class="icon-notification" title="This trail has not been verified by our members"></span>
				<?php else: ?>
				<span aria-hidden="true" class="icon-checkmark" title="This trail has been verified by our members"></span>
			<?php endif; ?>
		</div>
		<div class="clearfix"></div>
	</div>
	
	
<?php else: ?>


	<h1 class="node-data name">
		<?php print check_plain($title); ?>, <?php print $node->field_trail_park[0]['safe']['title'] ?>
		<?php if ($node->field_trail_status[0]['value'] == "2"): ?>
			<span aria-hidden="true" class="icon-checkmark" title="This trail has been VERIFIED by our members"></span>
		<?php endif; ?>
	</h1>
	<div class="content">
		<div class="node-photos">
			<?php print $node->field_trail_primary_photo[0]['view'] ?>
			<?php if (($user->uid) && ($node->field_trail_ts[0]['value'] == 'no')): ?>
			<div class="gallery">
				<div>Click to enlarge</div>
				<?php print $node->field_trail_gallery1_photo[0]['view'] ?>
				<?php print $node->field_trail_gallery2_photo[0]['view'] ?>
				<?php print $node->field_trail_gallery3_photo[0]['view'] ?>
				<?php print $node->field_trail_gallery4_photo[0]['view'] ?>
			</div>
			<?php endif; ?>
		</div>
		<div class="node-data-container">
			<div class="node-data city"><strong>Nearest City</strong><?php print $node->locations[0]['city'] ?></div>
			<div class="node-data diff"><strong>Difficulty</strong><?php print $node->field_trail_difficulty[0]['view'] ?></div>
			<div class="node-data sec"><strong>Seclusion</strong><?php print $node->field_trail_seclusion[0]['view'] ?></div>
			<?php if ($user->uid): ?><div class="node-data length"><strong>Length</strong><?php print $node->field_trail_length[0]['view'] ?></div><?php endif; ?>
			<?php if ($user->uid): ?><div class="node-data gain"><strong>Elevation Gain</strong><?php print $node->field_trail_elevation[0]['view'] ?></div><?php endif; ?>
			<div class="node-data marker-color"><strong>Marker Color</strong><?php print $node->field_trail_marker_color[0]['safe']?></div>
			<?php if ($user->uid): ?><div class="node-data sec"><strong>Dogs</strong><?php print $node->field_trail_dogs[0]['view'] ?></div><?php endif; ?>
			<?php if ($user->uid): ?><div class="node-data map"><strong>Trail Map</strong><?php if (!empty($node->field_trail_map[0]['filepath'])): ?><a href="/<?php print $node->field_trail_map[0]['filepath'] ?>" target="_blank"><?php print $node->field_trail_map[0]['filename'] ?></a><?php else: ?>No map added<?php endif; ?></div><?php endif; ?>
			<?php if ($user->uid): ?><div class="node-data weather"><strong>Current Weather</strong><?php $json_string = file_get_contents("http://api.wunderground.com/api/87e2660654417ebf/geolookup/conditions/q/" . $node->locations[0]['latitude'] . "," . $node->locations[0]['longitude'] . ".json"); $parsed_json = json_decode($json_string); $location = $parsed_json->{'location'}->{'city'}; $temp_f = $parsed_json->{'current_observation'}->{'temp_f'}; $weather = $parsed_json->{'current_observation'}->{'weather'}; echo "<a href='http://www.wunderground.com/cgi-bin/findweather/getForecast?query=" . $node->locations[0]['city'] . "%2C+" . $node->locations[0]['province'] . "'' target='_blank'>${weather}</a> &nbsp; ${temp_f}&deg;F";?></div><?php endif; ?>
		</div>
		<?php if ($user->uid): ?>
		<?php 
			if ( arg(0) == 'node' && is_numeric(arg(1)) ) {
				$nodeSelf = node_load(arg(1));
				$vidSelf = 1;
				$terms = taxonomy_node_get_terms_by_vocabulary($nodeSelf, $vidSelf);
				if ($terms) {
					print '<div class="clearfix"></div><div class="terms"><span class="header">Activities</span> &nbsp; ';
					foreach ($terms as $term) {
						print '<div class="activity">'.l($term->name, 'taxonomy/term/'.$term->tid).'</div>';
					}
					print '</div>';
				}
			}
		?>
		<?php endif; ?>
		<div class="clearfix"></div>
	</div>
	<div class="location-items">
		<?php if ($user->uid): ?>
			<div id="trail-buttons">
				<?php print $links; ?>
				<div id="moreContainer">
					<span class="menuTarget"><span aria-hidden="true" class="icon-cog"></span> More</span>
					<div id="moreMenu" <?php if ($node->field_trail_ts[0]['value'] == 'yes'): ?>class="padFix" <?php endif; ?>style="display:none">
						<span class="menuTarget"><span aria-hidden="true" class="icon-cog"></span> More</span>
						<ul>
							<li><a href="https://www.google.com/maps?q=<?php print $node->locations[0]['street'] ?>+<?php print $node->locations[0]['city'] ?>+<?php print $node->locations[0]['province'] ?>" target="_blank">Get Directions</a></li>
							<li><a href="#" onclick="window.print(); return false;">Print Trail</a></li>
							<?php if ($node->field_trail_status[0]['value'] == "1"): ?>
							<li><a href="/node/add/trail-verify?edit[field_trail_verify_trail][0][nid][nid]=<?php print $title ?><?php $parks = taxonomy_node_get_terms_by_vocabulary($node,3);if ($parks) {foreach ($parks as $park) {print '&edit[taxonomy][tags][3]=' . $park->name;}} else {print '&edit[taxonomy][tags][3]=None';}?>&edit[group_trail_data][field_trail_length][0][value]=<?php print $node->field_trail_length[0]['value'] ?>&edit[group_trail_data][field_trail_difficulty][value]=<?php print $node->field_trail_difficulty[0]['value'] ?>&edit[group_trail_data][field_trail_seclusion][value]=<?php print $node->field_trail_seclusion[0]['value'] ?>&edit[group_trail_data][field_trail_marker_color][value]=<?php print $node->field_trail_marker_color[0]['value']?>&edit[locations][0][street]=<?php print $node->locations[0]['street'] ?>&edit[locations][0][city]=<?php print $node->locations[0]['city'] ?>&edit[locations][0][province]=<?php print $node->locations[0]['province'] ?>&edit[group_trail_data][field_trail_elevation][value]=<?php print $node->field_trail_elevation[0]['value'] ?><?php $activities = taxonomy_node_get_terms_by_vocabulary($node,1);if ($activities) {print '&edit[taxonomy][tags][1]='; foreach ($activities as $activity) {print $activity->name . ', ';}} else {print '&edit[taxonomy][tags][1]=';}?>">Verify this trail</a></li>
							<?php endif; ?>
						</ul>
						<?php if (($display_submitted) && ($node->field_trail_ts[0]['value'] == 'no')): ?>
						<div class="submitted">
							<?php print $user_picture; ?> Submitted by <a href="/user/<?php print $node->uid; ?>"><?php print $node->name ?></a>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<?php print $trail_map; ?>
		<div class="address-container">
			<div id="coordsContainer">
				<?php if ($user->uid): ?>
				<span aria-hidden="true" class="icon-globe"></span>
				<div id="coordsMenu" style="display:none">
					<strong>Coordinates</strong><br/>
					<?php print $node->locations[0]['latitude'] ?>, <?php print $node->locations[0]['longitude'] ?>
				</div>
				<?php endif; ?>
			</div>
				<strong><?php print check_plain($title); ?></strong><br/>
				<?php if (!empty($node->locations[0]['street'])) { print $node->locations[0]['street'];}?><?php if (!empty($node->locations[0]['additional'])) { print ', ' . $node->locations[0]['additional'];}?><br/>
				<?php print $node->locations[0]['city'] ?>, <?php print $node->locations[0]['province'] ?><br/>
				<?php if ($user->uid): ?>
				<div class="trail-distance">
					<?php 
					$smart_ip_session = smart_ip_session_get('smart_ip');
					$json_distance_string = file_get_contents("http://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $smart_ip_session['location']['latitude'] . "," . $smart_ip_session['location']['longitude'] . "&destinations=" . $node->locations[0]['latitude'] . "," . $node->locations[0]['longitude'] . "&mode=driving&sensor=false&units=imperial"); 
					$parsed_distance_json = json_decode($json_distance_string); 
					$distance = $parsed_distance_json->rows[0]->elements[0]->distance->text; 
					$time = $parsed_distance_json->rows[0]->elements[0]->duration->text; 
					$user_allowed = smart_ip_session_get('device_geolocation');
					if ($user_allowed != null ) {
						echo "This trail is " . $distance . " from you (" . $time . ").";
					} else {
						echo "This trail is about " . $distance . " from you (" . $time . ").";
					}
					?>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php if ($node->field_trail_ts[0]['value'] == 'yes'): ?>
		<div class="trail-is-ts trailNoticeBand">
			<span aria-hidden="true" class="icon-notification"></span>&nbsp; This is a TrailStack added trail. Some information may be missing or incomplete. <a href="/node/add/trail-verify?edit[field_trail_verify_trail][0][nid][nid]=<?php print $title ?><?php $parks = taxonomy_node_get_terms_by_vocabulary($node,3);if ($parks) {foreach ($parks as $park) {print '&edit[taxonomy][tags][3]=' . $park->name;}} else {print '&edit[taxonomy][tags][3]=None';}?>&edit[group_trail_data][field_trail_length][0][value]=<?php print $node->field_trail_length[0]['value'] ?>&edit[group_trail_data][field_trail_difficulty][value]=<?php print $node->field_trail_difficulty[0]['value'] ?>&edit[group_trail_data][field_trail_seclusion][value]=<?php print $node->field_trail_seclusion[0]['value'] ?>&edit[group_trail_data][field_trail_marker_color][value]=<?php print $node->field_trail_marker_color[0]['value']?>&edit[locations][0][street]=<?php print $node->locations[0]['street'] ?>&edit[locations][0][city]=<?php print $node->locations[0]['city'] ?>&edit[locations][0][province]=<?php print $node->locations[0]['province'] ?>&edit[group_trail_data][field_trail_elevation][value]=<?php print $node->field_trail_elevation[0]['value'] ?><?php $activities = taxonomy_node_get_terms_by_vocabulary($node,1);if ($activities) {print '&edit[taxonomy][tags][1]='; foreach ($activities as $activity) {print $activity->name . ', ';}} else {print '&edit[taxonomy][tags][1]=';}?>">Help verify this trail now</a>.
		</div>
	<?php elseif (($node->field_trail_status[0]['value'] == "1") && ($user->uid)): ?>
		<div class="trail-verify trailNoticeBand">
			<span aria-hidden="true" class="icon-notification"></span>&nbsp; The trail information is UNVERIFIED. Our members have not verified the accuracy of this trail's profile.
			<a href="/node/add/trail-verify?edit[field_trail_verify_trail][0][nid][nid]=<?php print $title ?>&edit[field_trail_verify_park][0][nid][nid]=<?php print $node->field_trail_park[0]['safe']['title'] ?>&edit[group_trail_data][field_trail_length][0][value]=<?php print $node->field_trail_length[0]['value'] ?>&edit[group_trail_data][field_trail_difficulty][value]=<?php print $node->field_trail_difficulty[0]['value'] ?>&edit[group_trail_data][field_trail_seclusion][value]=<?php print $node->field_trail_seclusion[0]['value'] ?>&edit[group_trail_data][field_trail_marker_color][value]=<?php print $node->field_trail_marker_color[0]['value']?>&edit[locations][0][street]=<?php print $node->locations[0]['street'] ?>&edit[locations][0][city]=<?php print $node->locations[0]['city'] ?>&edit[locations][0][province]=<?php print $node->locations[0]['province'] ?>&edit[group_trail_data][field_trail_elevation][value]=<?php print $node->field_trail_elevation[0]['value'] ?><?php $activities = taxonomy_node_get_terms_by_vocabulary($node,1);if ($activities) {print '&edit[taxonomy][tags][1]='; foreach ($activities as $activity) {print $activity->name . ', ';}} else {print '&edit[taxonomy][tags][1]=';}?>">Help verify this trail now</a>.
		</div>
	<?php endif; ?>
	<div class="clearfix"></div>
	<?php if (!$user->uid): ?>
	<div class="memberDrive-lg">
		<a href="/user/register">Sign up</a> to see more information for <?php print check_plain($title); ?> in <?php print $node->field_trail_park[0]['safe']['title'] ?>.
		<div>
		<strong>Our Members enjoy these extra features:</strong>
		<ul>
			<li>More trail photos</li>
			<li>Real-time trail weather</li>
			<li>Trail distance and specific coords</li>
			<li>Easy trail printing</li>
			<li>Add trail to your own Stack</li>
		</ul>
		</div>
	</div>
	<?php endif; ?>
	<?php endif; ?>
</div><!-- /.node -->