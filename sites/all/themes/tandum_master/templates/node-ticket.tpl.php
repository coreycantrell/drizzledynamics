<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix">
    
    <div class="field-item" style="margin-bottom:5px">
        <div class="field-value">
            <h2 class="title"><?php print $node->field_ticket_summary[0]['view'] ?></h2>
        </div>
    </div>    
    
    <div class="field-section-header">Information</div>
    
    <div class="ticket-left">
        <div class="field-item">
            <div class="field-label">Ticket ID</div> 
            <div class="field-value">
                <img src="/sites/all/themes/tandum_master/images/note.png"> <?php print $node->nid ?>
            </div>
        </div>
        
        <div class="field-item">
            <div class="field-label">Client</div> 
            <div class="field-value">
                <img src="/sites/all/themes/tandum_master/images/user.png"> <?php print $node->field_ticket_ref_client[0]['view'] ?>
            </div>
        </div>        
        
        <div class="field-item">
            <div class="field-label">Type</div> 
            <div class="field-value">
                <?php $d=($node->field_ticket_type[0]['view']); if ($d=='Chore') print '<img src="/sites/all/themes/tandum_master/images/cog.png"> Chore'; elseif ($d=='Feature') print '<img src="/sites/all/themes/tandum_master/images/star.png"> Feature'; elseif ($d=='Bug') print  '<img src="/sites/all/themes/tandum_master/images/bug.png"> Bug'; else echo "Error"; ?>
            </div>
        </div>
    </div>
    
    <div class="ticket-right">
        <div class="field-item">
            <div class="field-label">Status</div> 
            <div class="field-value">
                <?php $d=($node->field_ticket_status[0]['safe']); if ($d=='0') print '<img src="/sites/all/themes/tandum_master/images/flag_blue.png">'; elseif ($d=='1') print '<img src="/sites/all/themes/tandum_master/images/flag_green.png">'; elseif ($d=='2') print  '<img src="/sites/all/themes/tandum_master/images/flag_check.png">'; elseif ($d=='3') print  '<img src="/sites/all/themes/tandum_master/images/flag_red.png">'; elseif ($d=='4') print '<img src="/sites/all/themes/tandum_master/images/flag_green.png">'; ?> <?php print $node->field_ticket_status[0]['view'] ?>
                <?php $d=($node->field_ticket_status[0]['safe']); 
                    if ($d=='2') {
                        if (!empty($node->field_ticket_accepted[0]['value'])) {
                            print ' &nbsp;-&nbsp; ' . $node->field_ticket_accepted[0]['view'];
                        } else { 
                            print ' &nbsp-&nbsp; Not Accepted'; 
                        }
                    }
                ?>
            </div>
        </div>

        <div class="field-item">
            <div class="field-label">Priority</div> 
            <div class="field-value">
                <?php $d=($node->field_ticket_priority[0]['view']); if ($d=='Low') print '<img src="/sites/all/themes/tandum_master/images/asterisk_green.png"> Low'; elseif ($d=='Normal') print '<img src="/sites/all/themes/tandum_master/images/asterisk_yellow.png"> Normal'; elseif ($d=='High') print  '<img src="/sites/all/themes/tandum_master/images/asterisk_red.png"> High'; else echo "Error"; ?>
            </div>
        </div>
        
        <div class="field-item">
            <div class="field-label">Due Date</div> 
            <div class="field-value">
                <img src="/sites/all/themes/tandum_master/images/clock.png"> <?php $d=($node->field_ticket_due[0]['value']); if ($d==null) { print "Not Set"; } else { print $node->field_ticket_due[0]['view']; }?>
            </div>
        </div>
    </div>
    
    <div class="field-item field-details" <?php $d=($node->field_ticket_status[0]['safe']); if ($d!=='4') { print "style='display:none'"; } ?>>
        <div class="field-value">
            <div class="field-section-header">Reason to Reopen</div>
            <?php print $node->field_ticket_reason[0]['view'] ?>
        </div>
    </div>
    
    <div class="field-item field-details">
        <div class="field-value">
            <div class="field-section-header">Ticket Details</div>
            <?php print $node->content['body']['#value'] ?>
            <div class="field-item" <?php $d=($node->field_ticket_attach[0]['filepath']); if ($d==null) { print "style='display:none'"; } ?>>
                <div class="field-label">Attachment</div> 
                <div class="field-value">
                    <img src="/sites/all/themes/tandum_master/images/attach.png"> <a href="/<?php print $node->field_ticket_attach[0]['filepath'] ?>" target="_blank"><?php print $node->field_ticket_attach[0]['filename'] ?></a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="field-item field-tracking">
        Owned by <?php $user=user_load($node->field_ticket_owner[0]['uid']); $username=$user->name; print '<a href="/users/' . $username . '">' . $username . '</a>'?> and was requested by <a href="/users/<?php print $node->name ?>"><?php print $node->name ?></a>.<br/>
        Unique ID is <?php print check_plain($node->title) ?>.<br/>
        Created on <?php echo format_date($node->created, 'custom', "m/j/y - g:ia") ?>.<br/>
        Last updated on <?php echo format_date($node->changed, 'custom', "m/j/y - g:ia") ?>.
    </div>
    
</div>
