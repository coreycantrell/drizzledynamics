<div class="file-right">
    <iframe src="https://docs.google.com/viewer?url=https://tandum.drizzledynamics.com/<?php print $node->field_invoice_file[0]['filepath']?>&embedded=true" width="100%" height="780" style="border:none;"></iframe>
</div>
<div class="file-left">
    <div class="field-item">
        <div class="field-label">Client</div> <div class="field-value"><img src="/sites/all/themes/tandum_master/images/user.png"> <?php print $node->field_invoice_client[0]['view'] ?></div>
    </div>
    <div class="field-item">
        <div class="field-label">Created</div> <div class="field-value"><img src="/sites/all/themes/tandum_master/images/calendar_add.png"> <?php echo format_date($node->created, 'custom', "m/d/y - g:ia") ?></div>
    </div>
    <div class="field-item">
        <div class="field-label">File</div> <div class="field-value"><?php print $node->field_invoice_file[0]['view'] ?></div>
    </div>
    <div class="field-item">
        <div class="field-label">Trackable URL</div> 
        <div class="field-value">
            <input type="text" value="https://tandum.drizzledynamics.com/sites/tandum.drizzledynamics.com/modules/pubdlcnt/invoice.php?file=https://tandum.drizzledynamics.com/<?php print $node->field_invoice_file[0]['filepath']?>&nid=<?php print $node->nid ?>"/>
        </div>
    </div>
    <div class="field-item">
        <div class="field-label">Send Invoice</div> 
        <div class="field-value">
            <button id="email-invoice" onclick="window.open('/pet/invoice&nid=<?php print $node->nid ?>')">Email</button>
            <div class="date-container"><?php print $node->field_invoice_date_email[0]['view'] ?></div>
        </div>
    </div>
    <div class="field-item">
        <div class="field-label">Send Reminder</div> <div class="field-value">
            <button id="email-notify" onclick="window.open('/pet/invoice_reminder&nid=<?php print $node->nid ?>')">Email</button>
            <div class="date-container"><?php print $node->field_invoice_date_notify[0]['view'] ?></div>
        </div>
    </div>
</div>
