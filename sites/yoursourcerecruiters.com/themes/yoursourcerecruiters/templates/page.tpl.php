<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
<head>
  <meta name="msvalidate.01" content="9C10FFCAC889E99F74FCA0B1725A85E4" />
  <title><?php print $head_title; ?></title>
  <meta name="description" content="Executive recruitment specializing in pharmaceutical and biotechnology industry focused on the client and candidate's unique needs.">
  <meta name="keywords" content="job, job placement, recruiting, recruiters, executive recruiters, pharmaceutical, biotechnology" />
  <meta name="y_key" content="91108795aa7b677d" />
  <meta name="google-site-verification" content="Oh9IzszostrkSEIM-7TFYfXT-4C_UFuc7w85_AT_Ep8" />
  <meta name="msvalidate.01" content="9C10FFCAC889E99F74FCA0B1725A85E4" />
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/jquery.bt.js"></script>
  <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/jquery.cycle.min.js"></script>
  <script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/dzdy.required.js"></script>
  <!--[if IE]><script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/excanvas.js"></script><![endif]-->
</head>
<body class="<?php print $classes; ?>">
	<div id="page-wrapper">
		<div id="page">
				<?php if ($header_links): ?>
					<?php print $header_links; ?>
				<?php endif; ?>
			<div id="header">
				<div class="section clearfix">
					<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"></a>
					<?php print $header; ?>
					<div id="search-container">
						<span id="search-label">
							Search our job listings.<br/>
							<span>Enter job title, keywords, or location.</span>
						</span>
						<?php print drupal_get_form('search_theme_form');?>
					</div>
					<div id="contact-container">
					Email us at <a href="mailto:team@ysrecruiters.com">team@ysrecruiters.com</a>
					</div>
				</div>
			</div>
			<div id="main-wrapper">
				<div id="main" class="clearfix<?php if ($primary_links || $navigation) { print ' with-navigation'; } ?>">
					<div id="content" class="column">
						<div class="section">
							<?php print $highlight; ?>
							<?php if ($title): ?>
							<h1 class="title"><?php print $title; ?></h1>
							<?php endif; ?>
							<?php print $messages; ?>
							<?php if ($tabs): ?>
							<div class="tabs"><?php print $tabs; ?></div>
							<?php endif; ?>
							<?php print $help; ?>
							<?php print $content_top; ?>
							<div id="content-area">
								<?php print $content; ?>
							</div>
							<?php print $content_bottom; ?>
						</div>
					</div>
					<?php if ($primary_links || $navigation): ?>
					<div id="navigation">
						<div class="section clearfix">
						  <?php print theme(array('links__system_main_menu', 'links'), $primary_links,
							array(
							  'id' => 'main-menu',
							  'class' => 'links clearfix',
							),
							array(
							  'text' => t('Main menu'),
							  'level' => 'h2',
							  'class' => 'element-invisible',
							));
						  ?>

							<?php print $navigation; ?>
						</div>
					</div>
					<?php endif; ?>
					<?php print $sidebar_first; ?>
					<?php print $sidebar_second; ?>
				</div>
			</div>
			<?php if ($footer || $footer_message || $secondary_links): ?>
			<div id="footer">
				<div class="section">
					<?php print theme(array('links__system_secondary_menu', 'links'), $secondary_links,
					  array(
						'id' => 'secondary-menu',
						'class' => 'links clearfix',
					  ),
					  array(
						'text' => t('Secondary menu'),
						'level' => 'h2',
						'class' => 'element-invisible',
					  ));
					?>
					<?php if ($footer_message): ?>
					<div id="footer-message">
						<?php print $footer_message; ?>
					</div>
					<?php endif; ?>

					<?php print $footer; ?>
				</div>
			</div>
			<?php endif; ?>

		</div>
		
	</div>
	<div style="width:960px;margin:0 auto;text-align:right">
	<div id="dzdy_badge" style="margin:10px 0;text-align:right;"></div>
	</div>
  <?php print $page_closure; ?>
  <?php print $closure; ?>
</body>
</html>
