<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" itemscope itemtype="http://schema.org/Product" xmlns:fb="http://ogp.me/ns/fb#" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
	<title><?php print $head_title; ?></title>
	<?php print $head; ?>
	<?php print $styles; ?>
	<link href='http://fonts.googleapis.com/css?family=Arvo' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
    <link href="https://s3.amazonaws.com/scripts.drizzledynamics.com/formalize/formalize.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/dzdy.required.js"></script>
	<script type="text/javascript" src="https://s3.amazonaws.com/scripts.drizzledynamics.com/formalize/jquery.formalize.js"></script>
	<?php print $scripts; ?>
	<meta itemprop="name" content="BedEdge by Babington Mills">
	<meta itemprop="description" content="BedEdge is a dust extracted chopped straw horse bedding created from all natural sources and is environmentally friendly.">
	<meta itemprop="image" content="http://www.babingtonmills.com/sites/babingtonmills.com/themes/babingtonmills/images/logo.png">
</head>
<body class="<?php print $classes; ?>">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=187406787976223";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	<div id="page-wrapper"><div id="page">
		<div id="header"><div class="section clearfix">
			<div id="site-logo">
				<a href="<?php print $base_path; ?>"><img src="/sites/babingtonmills.com/themes/babingtonmills/images/logo.png"/></a>
			</div>
			<?php print $header; ?>
		</div></div><!-- /.section, /#header -->
		
		<?php if ($navigation): ?>
		    <?php print $navigation; ?>
		<?php endif; ?>
		
		<?php if ($adminUI): ?>
			<div id="adminUI-wrapper">
				<?php print $adminUI; ?>
			</div>
		<?php endif; ?>
		
		<div id="main-wrapper"><div id="main" class="clearfix">
			<div id="content" class="column"><div class="section">
				<?php print $highlight; ?>
					<?php print $messages; ?>
					<?php if ($tabs): ?>
					<div class="tabs"><?php print $tabs; ?></div>
					<?php endif; ?>
					<?php if ($title): ?>
					<h1 class="title"><?php print $title; ?></h1>
					<?php endif; ?>
					<?php print $help; ?>
					<?php print $content_top; ?>
					<div id="content-area">
						<?php print $content; ?>
					</div>
				<?php print $content_bottom; ?>
			</div></div><!-- /.section, /#content -->
		</div></div><!-- /#main, /#main-wrapper -->
		<?php if ($content_extend): ?>
		<div id="content-extend-wrapper">
			<?php print $content_extend; ?>
		</div>
		<?php endif; ?>
		<?php if ($pre_footer): ?>
		<div id="pre-footer-br"></div>
		<div id="pre-footer-wrapper">
			<?php print $pre_footer; ?>
		</div>
		<?php endif; ?>
	</div></div><!-- /#page, /#page-wrapper -->
	<div id="footer"><div class="section">
		<?php if ($footer_message): ?>
		<div id="footer-message"><?php print $footer_message; ?></div>
		<?php endif; ?>
		<?php print $footer; ?>
		<div id="dzdy_badge" style="text-align:center;position:relative;z-index:9"></div>
	</div></div><!-- /.section, /#footer -->
	<?php print $page_closure; ?>
	<?php print $closure; ?>
</body>
</html>
