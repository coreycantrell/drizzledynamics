; $Id: 
Eventbookings
Author: Andrew Hall(developer-x)

Description
Integrates the Bookings API's booking records with a data enabled Node - aka an "Event".  This module
provides an integrated UI into the Node edit screen.  This allows automatic evaluation of changes in the date field.  

Also, several views are included to provide resource schedules.

It is compatable with the Publicbookings module but isn't a requirement.

Prerequisites
- Bookings API
- Calendar

ALPHA RELEASE NOTES
===================
The module has been tested but is based off an alpha package of Bookings API - as such, it may 
change frequently.  This module won't be considered released until Bookings API has stablized.

