<?php


function linkintel_node_suggestions_page($node, $mode, $keywords = '') {
  require_once './' . drupal_get_path('module', 'linkintel') . "/includes/requests.inc";
  linkintel_set_admin_theme(); 
  drupal_set_title('Suggestions');  
  
  $options = array();
  $filter = linkintel_node_suggestions_build_query($node);

  $output .= drupal_get_form('linkintel_node_suggestions_form');

  if($_SESSION['linkintel_node_suggestions_form']) {
    $header = linkintel_get_requests_report_header('suggestions');
    
    $result = linkintel_load_filtered_requests_result($filter, $options, $header);
    
    $destination = 'node/' . $node->nid . '/linkintel/suggestions';
    $output .= theme_linkintel_requests_report($header, $result, $destination, 'suggestions', $node);
  }
  
  unset($_SESSION['linkintel_node_suggestions_form']);
  return $output;
}

function linkintel_node_suggestions_build_query($node) {
  $data = $_SESSION['linkintel_node_suggestions_filter'];
  if(is_array($_SESSION['linkintel_node_suggestions_form'])) {
    foreach($_SESSION['linkintel_node_suggestions_form'] AS $k => $v) {
      $data[$k] = $v;
    }
  }
  if (empty($data)) {
    return;
  }

  $porterstemmer_exists = module_exists('porterstemmer');
  $filters = linkintel_node_suggestions_filters();
  // Build query
  $where = $args = array();
  
  // don't suggest links to self and pages already linked to
  $exclude_nids = array($node->nid);
  if($links0 = linkintel_load_links_by_page($node->nid)) {
    foreach($links0 AS $l) {
      $exclude_nids[] = $l->nid;
    }
  }
  $placeholders = implode(',', array_fill(0, count($exclude_nids), '%d'));
  $where[] = "r.nid NOT IN($placeholders)";
  $args = $exclude_nids;
  foreach ($data as $key => $filter) {
    if(!$filters[$key]['#where']) {
      continue;
    }
    if($key == 'like_match') {
      continue;
    }
    $filter_where = array();
    if ($filters[$key]['#type'] == 'select') {
      foreach ($filter as $value) {
        $filter_where[] = $filters[$key]['#where'];
        $args[] = $value;
      }
    }
    elseif ($filter) {      
      if ($key == 'keywords') {        
        if($_SESSION['linkintel_requests_filter']['like_match']) {
          $filter_where[] = $filters['like_match']['#where'];
          $args[] = $filter;

        }
        else {
          $keywords = explode(',', $filter);
          $placeholders = implode(',', array_fill(0, count($keywords), '"%s"'));
          $filter_where[] = str_replace('"%s"', $placeholders, $filters[$key]['#where']);
          $filtered_keywords = array();
          foreach($keywords AS $keyword) {
            $keyword = trim(strtolower($keyword));
            $args[] = $keyword;
            $filter_keywords[] = $keyword;
          }
          if(TRUE && $porterstemmer_exists) {
            $placeholders = implode(',', array_fill(0, count($filter_keywords), '"%s"'));
            $wh = str_replace('keyword', 'keyword_stem', $filters[$key]['#where']);
            $filter_where[] = str_replace('"%s"', $placeholders, $wh); 
            foreach($filter_keywords AS $keyword) {
              $keyword_stem = porterstemmer_search_preprocess($keyword);
              $args[] = $keyword_stem;
            }           
          }
        }
      }
      else if($key == 'from') {
        // treat as nid input
        $a = explode('/', $filter);
        if(($a[0] == 'node' && is_numeric($a[1])) || ($filter > 0)) {
          $nid = (($a[0] == 'node' && is_numeric($a[1])))? $a[1] : $filter;
          $filter_where[] = 'r.nid = %d';
          $args[] = $nid;
        }
        else {
          $filter_where[] = $filters[$key]['#where'];
          $args[] = $filter;         
        }
      }
      else {
        $filter_where[] = $filters[$key]['#where'];
        $args[] = $filter;
      }      
    }
    if (!empty($filter_where)) {
      $where[] = '('. implode(' OR ', $filter_where) .')';
    }
  }
  $where = !empty($where) ? implode(' AND ', $where) : '';

  return array(
    'where' => $where,
    'args' => $args,
    'keywords' => $filter_keywords,
  );
}

function linkintel_node_suggestions_form() {
  $session_filter = &$_SESSION['linkintel_node_suggestions_filter'];
  $session_filter = is_array($session_filter) ? $session_filter : array();
  
  $session_form = $_SESSION['linkintel_node_suggestions_form'];
  $session_form = is_array($session_form) ? $session_form : array();

  $nid = arg(1);
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => arg(1),    
  );
  
  $keywords = $session_form['keywords'];
  if(!$keywords) {
    $keywords = linkintel_get_seed_keywords($nid);
    $keywords = implode(', ', $keywords);
  }
  $form['keywords'] = array(
    '#type' => 'textfield',
    '#title' => t('Keywords'),
    '#default_value' => ($analysis && $analysis['inputs']['keyword'])?$analysis['inputs']['keyword']:'',
    '#description' => t('Seperate keywords with commas, e.g. "keyword,keyphrase,key word"'),
    '#default_value' => $keywords,
    '#size' => 120,
    '#maxlength' => 1024,
    '#where' => 'k.keyword IN ("%s")',
  );

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Options'),
    //'#theme' => 'dblog_filters',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    //'#collapsed' => empty($session_filter),
  );
  
  $form['options']['like_match'] = array(
    '#type' => 'checkbox',  
    '#title' => t('Like match'),    
    '#return_value' => 1,
    '#default_value' => $session_filter['like_match'],
    '#where' => 'k.keyword LIKE "%%%s%%"',
    '#filter' => 1,
  );
  
  $options = linkintel_get_priority_options();
  $form['options']['request_priority'] = array(
    '#type' => 'select',  
    '#title' => t('Request priority'),    
    '#options' => $options,
    '#multiple' => TRUE,
    '#default_value' => $session_filter['request_priority'],
    '#where' => 'r.priority = %d',
    '#filter' => 1,
  );
  $form['options']['page_priority'] = array(
    '#type' => 'select',  
    '#title' => t('Page priority'),    
    '#options' => $options,
    '#multiple' => TRUE,
    '#default_value' => $session_filter['page_priority'],
    '#where' => 'p.priority = %d',
    '#filter' => 1,
  );
  $form['options']['keyword_priority'] = array(
    '#type' => 'select',  
    '#title' => t('Page priority'),    
    '#options' => $options,
    '#multiple' => TRUE,
    '#default_value' => $session_filter['keyword_priority'],
    '#where' => 'kwr.priority = %d',
    '#filter' => 1,
  );
 
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  if (!empty($session)) {
    $form['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset')
    );
  }

  return $form;
}

function linkintel_node_suggestions_filters() {
  $form = linkintel_node_suggestions_form();
  $filters = linkintel_flatten_form($form);
  return $filters;   
}

function linkintel_flatten_form($form) {
  $ret = array();
  // get field sets
  foreach($form AS $k => $v) {
    if($v['#type'] == 'fieldset') {
      foreach($v AS $k2 => $v2) {
        if(substr($k2, 0, 1) != '#') {
          $ret[$k2] = $v2;
        }
      }
    }
    else {
      $ret[$k] = $v;
    }
  }
  return $ret;
}

/**
 * Validate result from site keyword report filter form.
 */
function linkintel_node_suggestions_form_validate($form, &$form_state) {

}

/**
 * Process result from site keywords report filter form.
 */
function linkintel_node_suggestions_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  $filters = linkintel_node_suggestions_filters();
//dsm($filters);
  switch ($op) {
    case t('Submit'):
      foreach ($filters as $name => $filter) {
        if (isset($form_state['values'][$name])) {
          if($filters[$name]['#filter']) {
            $_SESSION['linkintel_node_suggestions_filter'][$name] = $form_state['values'][$name];            
          } 
          else {
            $_SESSION['linkintel_node_suggestions_form'][$name] = $form_state['values'][$name];
          }
        }
      }
      break;
    case t('Reset'):
      $_SESSION['linkintel_node_suggestions_filter'] = array();
      break;
  }
  return 'node/' . $form_state['values']['nid'] . '/linkintel/suggestions';
}

function linkintel_get_seed_keywords($nid, &$relevance = array()) {
  $node = node_load($nid);
  $seeds = array();
  $relevance = array();
  // seed the node title
  $term = trim(strtolower($node->title));
  $seeds[$term] = $term;
  $relevance[$term] += 100;  
  // seed using taxonomy
  $seed_vids = variable_get('linkintel_suggestions_seed_vocabs', array());
  if (!empty($seed_vids) && is_array($node->taxonomy)) {
    foreach($seed_vids AS $seed_vid) {   
      foreach($node->taxonomy AS $term) {
        if($term->vid == $seed_vid) {
          $term = trim(strtolower($term->name));
          $seeds[$term] = $term;
          $relevance[$term] += 100;       
        }
      }
    }
  }
  //seed using alchemy 
  if (module_exists('alchemy') && variable_get('linkintel_suggestions_seed_alchemy_enabled', 1)) {
    $types = variable_get('linkintel_suggestions_seed_alchemy_types', alchemy_get_types(TRUE));
    $thres = variable_get('linkintel_suggestions_seed_alchemy_threshold', 50) / 100;
    foreach($types AS $type) {
      $t = alchemy_get_elements($node->body, $type, 'normalized', $nid, ALCHEMY_DEBUG);
      if(is_array($t)) {
        foreach($t as $k => $v) {
          if($v['relevance'] >= $thresh) {
            $seeds[$k] = $k;
            $relevance[$k] += 100 * $v['relevance'];
          }
        }      
      }
    }
  }
  
  drupal_alter('linkintel_get_seed_keywords', $seeds, $relevance);
  
  return $seeds; 
}