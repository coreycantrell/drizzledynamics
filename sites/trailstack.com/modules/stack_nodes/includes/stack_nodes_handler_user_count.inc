<?php
class stack_nodes_handler_user_count extends views_handler_field {
  function render($values) {
    return db_result(db_query("SELECT COUNT(*) FROM {stack_nodes} WHERE nid = %d", $values->nid));
  }
}