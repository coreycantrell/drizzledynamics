<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix">
<?php if ($teaser): ?>
	<?php if (!$page && $title): ?>
		<h2 class="title"><a href="<?php print $node_url; ?>"><?php print $title; ?><?php if ($unpublished): ?> <?php print t('(Unpublished)'); ?></div><?php endif; ?></a></h2>
	<?php endif; ?>
	<div class="content">
		<?php print $node->field_blog_photo1[0]['view'] ?>
		<?php print $node->content['body']['#value']; ?>
	</div>
	<div class="comment-links">
		<?php if ($node->comment): ?>
		<div class="blog-comment">
			<span aria-hidden="true" class="icon-comments" title="This post has <?php print $node->comment_count ?> comments"/></span> <?php print $node->comment_count; ?>
		</div>
		<?php endif; ?>
		<?php if ($node->field_blog_trail[0]['safe']['title']): ?>
		<div class="blog-related-trail">
			Related Trail: <?php print $node->content['field_blog_trail']['field']['items'][0]['#children']; ?>
		</div>
		<?php endif ?>
		<?php if ($node->readmore ): ?>
		<div class="blog-read-more">
			<a href="<?php print $node->path; ?>">Read More</a>
		</div>
		<span class="blog-submitted">
			<?php print $user_picture; ?><?php print $submitted; ?>
		</span>
		<?php endif; ?>
		<div class="clearfix"></div>
	</div>
	
<?php else: ?>

  <div class="content">
	<?php print $node->field_blog_photo1[0]['view'] ?>
	<?php print $node->field_blog_photo2[0]['view'] ?>
	<?php print $node->field_blog_photo3[0]['view'] ?>
	<?php print $node->content['body']['#value']; ?>
  </div>

  <div class="comment-links">
	<?php print $links; ?>
	<?php if ($display_submitted): ?>
		<div class="meta">
			<?php if ($display_submitted): ?>
			<span class="submitted">
				<?php print $user_picture; ?><?php print $submitted; ?>
			</span>
			<?php endif; ?>
		</div>
	<?php endif; ?>
  </div>
<?php endif; ?>
</div>
